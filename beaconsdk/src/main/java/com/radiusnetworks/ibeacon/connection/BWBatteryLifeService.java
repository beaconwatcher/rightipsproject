/**
 * BeaconWatcher, Inc.
 * http://www.beaconwatcher.com
 *
 * @author Shahbaz Ali
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.radiusnetworks.ibeacon.connection;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
/**
 * An class used to keep BluetoothGattCharacteristic
 * of Battery Level of beacons (if available).
 */
public class BWBatteryLifeService implements BluetoothService
{
	private final HashMap<UUID, BluetoothGattCharacteristic> characteristics = new HashMap();

	public void processGattServices(List<BluetoothGattService> services)
	{
		for (BluetoothGattService service : services)
			if (BWUuid.BW_BEACON_BATTERY_LIFE_SERVICE.equals(service.getUuid())) {
				this.characteristics.put(BWUuid.BW_BEACON_BATTERY_LIFE_CHAR, service.getCharacteristic(BWUuid.BW_BEACON_BATTERY_LIFE_CHAR));
			}
	}
	
	public Integer getBatteryPercent() {
		return this.characteristics.containsKey(BWUuid.BW_BEACON_BATTERY_LIFE_CHAR) ? Integer.valueOf(getUnsignedByte(((BluetoothGattCharacteristic)this.characteristics.get(BWUuid.BW_BEACON_BATTERY_LIFE_CHAR)).getValue())) : null;
	}

	public void update(BluetoothGattCharacteristic characteristic)
	{
		this.characteristics.put(characteristic.getUuid(), characteristic);
	}

	public Collection<BluetoothGattCharacteristic> getAvailableCharacteristics() {
		List chars = new ArrayList(this.characteristics.values());
		chars.removeAll(Collections.singleton(null));
		return chars;
	}
	
	private static int getUnsignedByte(byte[] bytes) {
		return unsignedByteToInt(bytes[0]);
	}
	private static int unsignedByteToInt(byte value)
	{
		return value & 0xFF;
	}
	
	private static int getUnsignedInt16(byte[] bytes) {
		return unsignedByteToInt(bytes[0]) + (unsignedByteToInt(bytes[1]) << 8);
	}

	private static String getStringValue(byte[] bytes) {
		int indexOfFirstZeroByte = 0;
		while (bytes[indexOfFirstZeroByte] != 0) {
			indexOfFirstZeroByte++;
		}

		byte[] strBytes = new byte[indexOfFirstZeroByte];
		for (int i = 0; i != indexOfFirstZeroByte; i++) {
			strBytes[i] = bytes[i];
		}
		
		return new String(strBytes);
	}
}
