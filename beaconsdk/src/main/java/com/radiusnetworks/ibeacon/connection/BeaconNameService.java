/**
 * BeaconWatcher, Inc.
 * http://www.beaconwatcher.com
 *
 * @author Shahbaz Ali
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.radiusnetworks.ibeacon.connection;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
 

/**
 * An class used to keep BluetoothGattCharacteristic
 * of Beacon Name Service(if available).
 */

public class BeaconNameService implements BluetoothService{
	private final HashMap<UUID, BluetoothGattCharacteristic> characteristics = new HashMap<UUID, BluetoothGattCharacteristic>();
	private final HashMap<UUID, ConnectionManager.WriteCallback> writeCallbacks = new HashMap();

	public void processGattServices(List<BluetoothGattService> services){
		 for (BluetoothGattService service : services){
			 if (BWUuid.BW_BEACON_NAME_SERVICE.equals(service.getUuid())) {
				 this.characteristics.put(BWUuid.BW_BEACON_NAME_CHAR, service.getCharacteristic(BWUuid.BW_BEACON_NAME_CHAR));
			 }
		 }
	 }
 
	 public void update(BluetoothGattCharacteristic characteristic){
		 this.characteristics.put(characteristic.getUuid(), characteristic);
	 }
	 
	 public Collection<BluetoothGattCharacteristic> getAvailableCharacteristics() {
		 List chars = new ArrayList(this.characteristics.values());
		 chars.removeAll(Collections.singleton(null));
		 return chars;
	 } 
	 
	 
	public BluetoothGattCharacteristic beforeCharacteristicWrite(UUID uuid, ConnectionManager.WriteCallback callback) {
		this.writeCallbacks.put(uuid, callback);
		return (BluetoothGattCharacteristic)this.characteristics.get(uuid);
	}
 
	public void onCharacteristicWrite(BluetoothGattCharacteristic characteristic, int status) {
		ConnectionManager.WriteCallback writeCallback = (ConnectionManager.WriteCallback)this.writeCallbacks.remove(characteristic.getUuid());
		if (status == 0)
			writeCallback.onSuccess();
		else
			writeCallback.onError();
	}

	 
		public boolean hasCharacteristic(UUID uuid){
			return this.characteristics.containsKey(uuid);
		}

	 public String getBeaconName() {
		 String val=null;
		 if(this.characteristics.containsKey(BWUuid.BW_BEACON_NAME_CHAR)){
			 BluetoothGattCharacteristic chara=(BluetoothGattCharacteristic) this.characteristics.get(BWUuid.BW_BEACON_NAME_CHAR);
			 byte[]bytes=chara.getValue();
			 val=getStringValue(bytes);
		 }
		 
		 return val;
	 }
	 private static String getStringValue(byte[] bytes) {
		 int indexOfFirstZeroByte = 0;
		 for(int i=0; i<bytes.length; i++){
			 if(bytes[i] ==0){
				 indexOfFirstZeroByte=i;
			 }
		 }
		 
		 byte[] strBytes;
		 
		 if(indexOfFirstZeroByte > 0){
			 strBytes = new byte[indexOfFirstZeroByte];
		 }
		 else{
			 strBytes = new byte[bytes.length];
		 }
		 for (int i = 0; i<strBytes.length; i++) {
			 strBytes[i] = bytes[i];
		 }
		
		 String str=new String(strBytes);
		 //String str=new String(bytes);
		 
		 return str;
	 }			 
 
//	 public boolean isAuthSeedCharacteristic(BluetoothGattCharacteristic characteristic) {
//		 return characteristic.getUuid().equals(JaaleeUuid.AUTH_SEED_CHAR);
//	 }
// 
//	 public boolean isAuthVectorCharacteristic(BluetoothGattCharacteristic characteristic) {
//		 return characteristic.getUuid().equals(JaaleeUuid.AUTH_VECTOR_CHAR);
//	 }
// 
//	 public BluetoothGattCharacteristic getAuthSeedCharacteristic() {
//		 return (BluetoothGattCharacteristic)this.characteristics.get(JaaleeUuid.AUTH_SEED_CHAR);
//	 }
// 
//	 public BluetoothGattCharacteristic getAuthVectorCharacteristic() {
//		 return (BluetoothGattCharacteristic)this.characteristics.get(JaaleeUuid.AUTH_VECTOR_CHAR);
//	 }
 }
