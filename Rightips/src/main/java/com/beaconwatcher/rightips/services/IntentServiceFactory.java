package com.beaconwatcher.rightips.services;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.services.concrete.LikeListItemService;
import com.beaconwatcher.rightips.services.concrete.LoginService;
import com.beaconwatcher.rightips.services.concrete.MyFriendsService;
import com.beaconwatcher.rightips.services.concrete.RemoveListItemService;
import com.beaconwatcher.rightips.services.concrete.ReportListItemService;
import com.beaconwatcher.rightips.services.concrete.SaveProfilePictureService;
import com.beaconwatcher.rightips.services.concrete.SearchService;
import com.beaconwatcher.rightips.services.concrete.SiteReviewsService;
import com.beaconwatcher.rightips.services.concrete.UserLikesService;
import com.beaconwatcher.rightips.services.concrete.UserPhotosService;
import com.beaconwatcher.rightips.services.concrete.UserProfileService;
import com.beaconwatcher.rightips.services.concrete.UserReviewsService;
import com.beaconwatcher.rightips.services.concrete.UserWallsService;
import com.beaconwatcher.rightips.services.concrete.VerifyEmailService;
import com.google.gson.Gson;

public class IntentServiceFactory {
	private static IntentServiceFactory instance = null;
	private Gson gson = new Gson();

	private IntentServiceFactory() {
	}

	public static synchronized IntentServiceFactory getInstance() {
		if (instance == null) {
			instance = new IntentServiceFactory();
		}
		return instance;
	}

	public Class getIntentServiceClass(String action) {
		if (action.equals(AppConstants.ACTION_GET_USER_PROFILE)) {
			return UserProfileService.class;
		} else if (action.equals(AppConstants.ACTION_GET_USER_LIKES)) {
			return UserLikesService.class;
		} else if (action.equals(AppConstants.ACTION_GET_USER_PHOTOS)) {
			return UserPhotosService.class;
		} else if (action.equals(AppConstants.ACTION_SAVE_USER_PROFILE_IMAGE)
				|| action.equals(AppConstants.ACTION_SAVE_USER_COVER_IMAGE)) {
			return SaveProfilePictureService.class;
		} else if (action.equals(AppConstants.ACTION_LOGIN)) {
			return LoginService.class;
		} else if (action.equals(AppConstants.ACTION_GET_MY_WALLS)) {
			return UserWallsService.class;
		} else if (action.equals(AppConstants.ACTION_GET_SITE_REVIEWS)) {
			return SiteReviewsService.class;
		} else if (action.equals(AppConstants.ACTION_GET_USER_REVIEWS)) {
			return UserReviewsService.class;
		} else if (action.equals(AppConstants.REPORT_LIST_ITEM)) {
			return ReportListItemService.class;
		} else if (action.equals(AppConstants.REMOVE_LIST_ITEM)) {
			return RemoveListItemService.class;
		} else if (action.equals(AppConstants.LIKE_LIST_ITEM)) {
			return LikeListItemService.class;
		} else if (action.equals(AppConstants.ACTION_GET_MY_FRIENDS)) {
			return MyFriendsService.class;
		} else if (action.equals(AppConstants.START_SEARCH_WITH_KEYWORD)) {
			return SearchService.class;
		}
		else if (action.equals(AppConstants.ACTION_VERIFY_EMAIL)) {
			return VerifyEmailService.class;
		}
		
		
		return null;
	}

}
