package com.beaconwatcher.rightips.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.activities.MainActivity;
import com.beaconwatcher.rightips.db.RTDBManager;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.piwik.PiwikConstants;
import com.beaconwatcher.rightips.entities.piwik.PiwikUtilities;
import com.bw.libraryproject.R;
import com.bw.libraryproject.entities.BeaconWithNotification;
import com.bw.libraryproject.entities.NotificationData;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.radiusnetworks.ibeacon.IBeacon;
import com.radiusnetworks.ibeacon.IBeaconConsumer;
import com.radiusnetworks.ibeacon.IBeaconManager;
import com.radiusnetworks.ibeacon.MonitorNotifier;
import com.radiusnetworks.ibeacon.RangeNotifier;
import com.radiusnetworks.ibeacon.Region;
import com.radiusnetworks.ibeacon.service.IBeaconData;

public class RighTipsMonitorService extends Service implements IBeaconConsumer {
	private static final String TAG = "RighTipsMonitorService";
	private RighTipsApplication mApp;

	private IBeaconManager iBeaconManager = IBeaconManager
			.getInstanceForApplication(this);
	private Intent callerIntent;
	private Boolean notificationSent = false;
	private Boolean allNotificationsLoaded = false;
	private Region mRegion;
	private String mUUID;
	private String mNotificationsData;

	private Boolean isBackground = true;
	private RTDBManager dbManager;

	// Current beacon that needs to be treated for notification.
	private IBeaconData beaconToTrack;
	// Current beacon notification.
	private NotificationData beaconNotification;
	// String representation of the bacon as UUID_Major_Minor
	private String beaconInfo;

	// lastTracked beacon's UUID_Major_Minor
	private String lastTrackedBeaconUMM = "";
	private int lastTrackedZone;

	private String logText = "";

	private ArrayList<IBeacon> scannedBeacons = new ArrayList<IBeacon>();
	private HashMap<String, BeaconWithNotification> allBeaconsNotifications = new HashMap<String, BeaconWithNotification>();

	// We keep track of all beacons in a queue so that it does not encounter
	// same beacon again and again.
	private ArrayList<String> notificationsPosted = new ArrayList<String>();

	String[] arr = { "com.beaconwatcher.rightips.activities.MainActivity" };
	ArrayList<String> validActivities = new ArrayList<String>(
			Arrays.asList(arr));

	// For stopping this service for a predefined time.
	// If we don't stop, it will continue monitoring
	// in the background and drain the battery.
	private static int SELF_STOP_TIME = (1000 * 60) * (60);

	public static String FOREGROUND_NOTIFICATION = "RighTips_Foreground_Notification";
	public static String NOTIFICATION_DATA = "RighTips_Notification_Data";
	public static String BEACON_UUID_MAJOR_MINOR = "Beacon_UUID_MAJOR_MINOR";
	public static String NOTIFICATION_ID = "Beacon_Notification_ID";

	@Override
	public IBinder onBind(Intent intent) {
		Log.d(TAG, "Service is bound to: " + intent.getPackage());
		return null;
	}

	@Override
	public void onDestroy() {
		Log.d(TAG, "Service onDestroy called");
		if (iBeaconManager.isBound(this)) {
			iBeaconManager.unBind(this);
		}
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (intent != null && callerIntent == null) {
			callerIntent = intent;
		}

		notificationsPosted.clear();
		iBeaconManager.bind(this);
		mApp = (RighTipsApplication) getApplicationContext();

		// UNCOMMENT this delete database and start adding records from scratch.
		// deleteDatabase(RTDBManager.DATABASE_NAME);
		Log.d(TAG, "Service onCreate called");
		loadAllNotifications();

		if (intent != null && intent.hasExtra("isBackground")) {
			Boolean b = intent.getBooleanExtra("isBackground", true);
			if (b != null) {
				if (b == true) {
					isBackground = true;
				} else if (b == false) {
					isBackground = false;
				}
			}
		}
		/*
		 * if(intent.hasExtra("reloadNotifications")){ Boolean
		 * loadData=intent.getBooleanExtra("reloadNotifications", false);
		 * if(loadData==true){ loadAllNotifications(); } }
		 */

		Log.d(TAG, "Service On StartCommand called");

		mUUID = mApp.getPreferences("RightTipsAppUUID", "");
		mRegion = new Region("RightTipsRegion", mUUID, null, null);

		if (dbManager == null) {
			dbManager = new RTDBManager(getApplicationContext());
			dbManager.open();
		}

		// We must stop this background service
		// because if it is not stopped, it will forever run
		// and will drain the battery.
		final Handler handler = new Handler();
		final Runnable runnable = new Runnable() {
			@Override
			public void run() {
				Log.d(TAG, "Service stopSelf called after: " + SELF_STOP_TIME);
				iBeaconManager.unBind(RighTipsMonitorService.this);
				stopSelf();
			}
		};
		handler.postDelayed(runnable, SELF_STOP_TIME);
		return START_STICKY;
	}

	@Override
	public void onIBeaconServiceConnect() {
		Log.d(TAG, "Service onIBeaconServiceConnect called");
		iBeaconManager.setMonitorNotifier(new MonitorNotifier() {
			@Override
			public void didEnterRegion(Region region) {
				Log.d(TAG, "didEnterRegion called");
				// if(!allNotificationsLoaded){
				//loadAllNotifications();
				lastTrackedBeaconUMM = "";
				lastTrackedZone = -1;
				// }

				// Add analytic event.
				/*
				 * AnalyticsEvent
				 * evt=mApp.getAnalyticsEvent(RighTipsMonitorService.this,
				 * "Enter Beacon Region"); evt.addAttribute("Beacon UUID",
				 * region.getProximityUuid()); evt.addAttribute("Beacon Major",
				 * Integer.toString(region.getBeacon().getMajor()));
				 * evt.addAttribute("Beacon Minor",
				 * Integer.toString(region.getBeacon().getMinor()));
				 * mApp.logAnalyticsEvent(evt);
				 */

				startRanging();

				// Log event to piwik
				String val = PiwikUtilities.getPiwikSiteEnterJsonString(region
						.getProximityUuid(), region.getMajor().toString(),
						region.getMinor().toString());
				mApp.getTracker().trackEvent(PiwikConstants.CATEGORY_VENUE,
						PiwikConstants.ACTION_ENTER, val);

			}

			@Override
			public void didExitRegion(Region region) {
				Log.e(TAG, "didExitRegion");
				stopRanging();

				lastTrackedBeaconUMM = "";
				lastTrackedZone = -1;

				// Log event to piwik
				String val = PiwikUtilities.getPiwikSiteEnterJsonString(region
						.getProximityUuid(), region.getMajor().toString(),
						region.getMinor().toString());
				mApp.getTracker().trackEvent(PiwikConstants.CATEGORY_VENUE,
						PiwikConstants.ACTION_EXIT, val);
			}

			@Override
			public void didDetermineStateForRegion(int state, Region region) {
				Log.e(TAG, "didDetermineStateForRegion:" + state);
			}

		});

		try {
			iBeaconManager.startMonitoringBeaconsInRegion(mRegion);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		iBeaconManager.setRangeNotifier(new RangeNotifier() {
			@Override
			public void didRangeBeaconsInRegion(
					final Collection<IBeacon> iBeacons, Region region) {
				rangedBeacons(iBeacons);
			}
		});
	}

	private void startRanging() {
		try {
			Log.e(TAG, "ranging started");
			iBeaconManager.setForegroundBetweenScanPeriod(10000);
			iBeaconManager.updateScanPeriods();
			iBeaconManager.startRangingBeaconsInRegion(mRegion);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	private void stopRanging() {
		try {
			iBeaconManager.stopRangingBeaconsInRegion(mRegion);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	public void loadAllNotifications() {
		Log.d(TAG, "Loading all notifications...");
		RequestParams params = new RequestParams();
		// params.put("userID",
		// mApp.getPreferences(LoginActivity.PREF_LOGIN_USER_ID, ""));

		// this will fetch comma separated ids for user selected categories.
		String cats = mApp.getPreferences(AppConstants.SELECTED_CATEGORIES, "");
		if (!cats.equals("")) {
			params.put("cat_id", cats);
		}

		mApp.APICall(
				"http://api.beaconwatcher.com/index.php?action=getAllNotifications",
				params, responseHandler);
	}

	AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
		@Override
		public void onStart() {
			Log.v(TAG, "onStart");
		}

		@Override
		public void onSuccess(String response) {
			Log.v(TAG, "onSuccess");

			// {"status":1,"message":"Beacon details","data":[{"msb_id":"41","msb_name":"Testing Beacons","msb_location":"","msb_uuid":"5144A35F387A7AC0B254DFB1381C9DFF","msb_minor":"1","msb_major":"1","msb_txpower":"0","msb_mac_address":"test","site_id":"20","site_title":"ESOL OFFICE","company":"Right Tips","notifications":[{"nt_id":"21","nt_name":"Pizza Hut Special Deal","nt_title":"Fun 4 All","nt_details":"Big Time Treat Inside Box, 2 hot spicy pizzaz, 8 Garlic breads, 500 ml Cold Drink.","zone":"1","template":"http:\/\/beaconwatcher.com\/api\/template.php?nt_id=21"}]}]}
			// Log.d(TAG, "All Notifications Loaded: "+response);
			JSONObject jo = null;

			try {
				jo = new JSONObject(response);
				int status = jo.getInt("status");
				String msg = jo.getString("message");

				if (status == 0) {
					onNoNotification();
				} else {
					// mApp.savePreferences(ALL_NOTIFICATION_DATA, response);
					onNotificationFound(response);
				}
			} catch (JSONException e) {
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
		}

		@Override
		public void onFailure(Throwable error, String content) {
			Log.e(TAG, "onFailure error : " + error.toString() + "content : "
					+ content);
			onNoNotification();
		}

		@Override
		public void onFinish() {
			Log.v(TAG, "onFinish");
		}
	};

	private void onNoNotification() {
		// mNoteBox.setVisibility(View.VISIBLE);
	}

	private void onNotificationFound(String response) {
		// {"message":"Beacon details","data":[{"msb_id":"41","msb_name":"Testing Beacons","msb_location":"","msb_uuid":"5144A35F387A7AC0B254DFB1381C9DFF","msb_minor":"1","msb_major":"1","msb_txpower":"0","msb_mac_address":"test","site_id":"20","site_title":"ESOL OFFICE","company":"Right Tips","notifications":[{"nt_id":"21","nt_name":"Pizza Hut Special Deal","nt_title":"Fun 4 All","nt_details":"Big Time Treat Inside Box, 2 hot spicy pizzaz, 8 Garlic breads, 500 ml Cold Drink.","zone":"1","template":"http:\/\/beaconwatcher.com\/api\/template.php?nt_id=21"}]}]}
		mNotificationsData = response;
		if (!mNotificationsData.equals("")) {
			allBeaconsNotifications = BeaconWithNotification
					.convertNotifications(mNotificationsData);
		}
	}

	private void rangedBeacons(final Collection<IBeacon> beacons) {
		scannedBeacons.clear();
		scannedBeacons.addAll(beacons);

		Log.e(TAG, "ranged beacons: " + beacons.size());

		// Apply some sorting on accuracy/distance, so that
		// nearest beacons will be at start in the list.
		Collections.sort(scannedBeacons, comparator);/*
													 * new
													 * Comparator<IBeacon>(){
													 * public int
													 * compare(IBeacon beacon1,
													 * IBeacon beacon2) { //
													 * TODO Auto-generated
													 * method stub return
													 * Double.
													 * compare(beacon1.getAccuracy
													 * (),
													 * beacon2.getAccuracy()); }
													 * });
													 */

		beaconToTrack = null;
		beaconNotification = null;
		beaconInfo = "";

		Iterator<IBeacon> iterator = scannedBeacons.iterator();

		while (iterator.hasNext() && beaconToTrack == null) {
			IBeaconData beacon = (IBeaconData) iterator.next();
			beaconInfo = beacon.getProximityUuid() + "_" + beacon.getMajor()
					+ "_" + beacon.getMinor();
			beaconNotification = getNotificationByBeacon(beacon);
			if (beaconNotification != null) {
				beaconToTrack = beacon;
			}
		}

		if (beaconToTrack != null) {
			Log.e(TAG, "beaconToTrack: " + beaconInfo);
			// If beacon is DIFFERENT than last tracked beacon.
			if (beaconNotification != null
					&& !beaconInfo.equals(lastTrackedBeaconUMM)) {
				lastTrackedBeaconUMM = beaconInfo;
				lastTrackedZone = beaconToTrack.getProximity();
				generateNotification(beaconNotification, beaconInfo,
						Integer.parseInt(beaconNotification.getNoteID()),
						beaconToTrack);
			}
			// If beacon is SAME, then check if it is different zone.
			else if (beaconNotification != null
					&& beaconInfo.equals(lastTrackedBeaconUMM)
					&& lastTrackedZone != beaconToTrack.getProximity()) {
				lastTrackedBeaconUMM = beaconInfo;
				lastTrackedZone = beaconToTrack.getProximity();
				generateNotification(beaconNotification, beaconInfo,
						Integer.parseInt(beaconNotification.getNoteID()),
						beaconToTrack);
			}
		}
	}

	// Sort beacons by proximity
	Comparator<IBeacon> comparator = new Comparator<IBeacon>() {
		public int compare(IBeacon beacon1, IBeacon beacon2) {
			return Double.compare(beacon1.getAccuracy(), beacon2.getAccuracy());
		}
	};

	private NotificationData getNotificationByBeacon(IBeacon beacon) {
		if (beacon == null)
			return null;

		String key = beacon.getProximityUuid();
		/*
		 * key=key.toUpperCase(); key=key.replaceAll("-", "");
		 */
		key = key + "_" + beacon.getMajor() + "_" + beacon.getMinor();

		// key="00000000-0000-0000-0000-000000000099_16_2";
		if (!allBeaconsNotifications.containsKey(key)) {
			return null;
		}

		BeaconWithNotification bn = allBeaconsNotifications.get(key);
		for (int n = 0; n < bn.notifications.size(); n++) {
			NotificationData nd = bn.notifications.get(n);
			String beaconZone = Integer.toString(beacon.getProximity());
			String notifZone = nd.getZone();

			if (beaconZone.equals(notifZone)) {
				return nd;
			}
		}
		return null;
	}

	private Boolean isSameBeacon(IBeaconData b1, IBeaconData b2) {
		if (b1 == null || b2 == null) {
			return false;
		}
		if (b1.getProximityUuid().equals(b2.getProximityUuid())
				&& b1.getMajor() == b2.getMajor()
				&& b1.getMinor() == b2.getMinor()) {
			return true;
		}
		return false;
	}

	/**
	 * Issues a notification to inform the user that you entered into a region.
	 */
	private void generateNotification(NotificationData nd, String bi,
			Integer id, IBeaconData beacon) {
		nd.setSource("beacon");
		nd.setStatus("posted");
		dbManager.insertNotification(nd);

		Log.e(TAG, "Generate Notification: " + bi);

		// Add analytic event.
		/*
		 * AnalyticsEvent
		 * evt=mApp.getAnalyticsEvent(RighTipsMonitorService.this,
		 * "Notification Received"); evt.addAttribute("Beacon UUID",
		 * beacon.getProximityUuid()); evt.addAttribute("Beacon Major",
		 * Integer.toString(beacon.getMajor()));
		 * evt.addAttribute("Beacon Minor",
		 * Integer.toString(beacon.getMinor())); evt.addAttribute("Beacon Zone",
		 * nd.zone); evt.addAttribute("Notification ID", nd.id);
		 * evt.addAttribute("Notification Title", nd.title);
		 * evt.addAttribute("Notification Content", nd.template);
		 */

		/*
		 * If App is running in foreground and Activity specified for handling
		 * notification is also on Top Stack then send broadcase to update that
		 * Activity and insert notification into db.
		 */
		if (isAppRunningOnForeground(getApplicationContext()) == true
				&& isTopActivityValid(getApplicationContext()) == true) {
			// Tell other activities that new notifiction is posted
			// so that those activities add this notificiton into db and update
			// view.
			Intent intent = new Intent(
					RighTipsMonitorService.FOREGROUND_NOTIFICATION);
			intent.putExtra(RighTipsMonitorService.NOTIFICATION_ID, id);
			intent.putExtra(RighTipsMonitorService.NOTIFICATION_DATA, nd);
			intent.putExtra(RighTipsMonitorService.BEACON_UUID_MAJOR_MINOR, bi);
			sendBroadcast(intent);

			// Log event to piwik
			String val = PiwikUtilities.getPiwikNotificationString(
					nd.getNoteID(), nd.getTitle(), nd.getSiteID(),
					nd.getSiteName(), nd.getZone());
			mApp.getTracker().trackEvent(PiwikConstants.CATEGORY_NOTIFICATION,
					PiwikConstants.ACTION_OPENED, val);

		}

		/*
		 * If App is running in background then generate top bar notification
		 * and add notification data into intent so that when user click top bar
		 * notification and it open up desired activity, that activity will
		 * render the notification.
		 */

		else {
			// evt.addAttribute("Notification Type", "Status Bar Notification");
			// evt.addAttribute("Application Mode", "Background");
			// mApp.logAnalyticsEvent(evt);

			// Intent notifyIntent = new Intent();//, MonitoringActivity.class);
			postNotification(nd, bi, id);
		}
	}

	public void postNotification(NotificationData nd, String bi, Integer id) {
		if (callerIntent == null) {
			callerIntent = new Intent();
			callerIntent.putExtra("targetActivity",
					MainActivity.class.getName());
		}

		String targetName = callerIntent.getStringExtra("targetActivity");
		callerIntent.putExtra(RighTipsMonitorService.NOTIFICATION_DATA, nd);
		callerIntent.putExtra("BeaconInfo", bi);

		// Log event to piwik
		String val = PiwikUtilities.getPiwikNotificationString(nd.getNoteID(),
				nd.getTitle(), nd.getSiteID(), nd.getSiteName(), nd.getZone());
		mApp.getTracker().trackEvent(PiwikConstants.CATEGORY_NOTIFICATION,
				PiwikConstants.ACTION_POSTED, val);

		if (targetName != null) {
			try {
				Class<?> clazz = Class.forName(targetName);
				callerIntent.setClass(RighTipsMonitorService.this, clazz);
			} catch (ClassNotFoundException e) {
				// Log.e("BeaconDetactorService",
				// "No target activity defined for notification");
			}
		}

		callerIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);// FLAG_ACTIVITY_CLEAR_TASK);//
																// .FLAG_ACTIVITY_NEW_TASK);

		PendingIntent pendingIntent = PendingIntent.getActivities(
				RighTipsMonitorService.this, 0, new Intent[] { callerIntent },
				PendingIntent.FLAG_UPDATE_CURRENT);

		/*
		 * 
		 * Notification notification = new
		 * Notification.Builder(RighTipsMonitorService.this)
		 * .setSmallIcon(R.drawable.ic_launcher) .setContentTitle(nd.getTitle())
		 * .setContentText(nd.getDetail()) .setAutoCancel(true)
		 * .setContentIntent(pendingIntent) .build();
		 * 
		 * if(lastNotificationID!=id){ lastNotificationID=id;
		 * notification.defaults |= Notification.DEFAULT_SOUND; }
		 * notification.defaults |= Notification.DEFAULT_LIGHTS;
		 * ((NotificationManager)
		 * RighTipsMonitorService.this.getSystemService(Context
		 * .NOTIFICATION_SERVICE)).notify(id, notification);
		 */

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(nd.getTitle()).setContentText(nd.getDetail())
				.setAutoCancel(true);

		if (notificationsPosted.contains(id.toString())) {
			// mBuilder.setDefaults(NotificationCompat.DEFAULT_VIBRATE |
			// NotificationCompat.DEFAULT_LIGHTS);
			mBuilder.setDefaults(NotificationCompat.DEFAULT_LIGHTS);
		} else {
			notificationsPosted.add(id.toString());
			mBuilder.setDefaults(NotificationCompat.DEFAULT_SOUND
					| NotificationCompat.DEFAULT_LIGHTS);
		}

		// Creates an explicit intent for an Activity in your app

		Intent resultIntent = new Intent();

		Class<?> clazz;

		if (targetName != null) {
			try {
				clazz = Class.forName(targetName);
				resultIntent.setClass(RighTipsMonitorService.this, clazz);
				resultIntent.putExtra(RighTipsMonitorService.NOTIFICATION_DATA,
						nd);
				// callerIntent.putExtra(NOTIFICATION_DATA, nd);
				resultIntent.putExtra("BeaconInfo", bi);

				// callerIntent.setClass(RighTipsMonitorService.this, clazz);
			} catch (ClassNotFoundException e) {
				// Log.e("BeaconDetactorService",
				// "No target activity defined for notification");
			}
		}

		// The stack builder object will contain an artificial back stack for
		// the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(MainActivity.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.
		mNotificationManager.notify(id, mBuilder.build());

		notificationSent = true;
	}

	// This function checks if RighTips app is running on foreground.
	private Boolean isAppRunningOnForeground(final Context context) {
		try {
			ActivityManager am = (ActivityManager) context
					.getSystemService(Context.ACTIVITY_SERVICE);
			// The first in the list of RunningTasks is always the foreground
			// task.
			RunningTaskInfo foregroundTaskInfo = am.getRunningTasks(1).get(0);
			String foregroundTaskPackageName = foregroundTaskInfo.topActivity
					.getPackageName();// get the top fore ground activity
			PackageManager pm = context.getPackageManager();
			PackageInfo foregroundAppPackageInfo = pm.getPackageInfo(
					foregroundTaskPackageName, 0);

			String foregroundTaskAppName = foregroundAppPackageInfo.applicationInfo
					.loadLabel(pm).toString();

			// Log.e("", foregroundTaskAppName +"----------"+
			// foregroundTaskPackageName);
			if (foregroundTaskAppName.equals("RighTips")) {
				return true;
			}
		} catch (Exception e) {
			Log.e("isAppSentToBackground", "" + e);
		}
		return false;
	}

	/*
	 * This function checks if activity current on top stack matches any of the
	 * activity specified to handle foreground notification.
	 */
	private Boolean isTopActivityValid(Context context) {
		try {
			ActivityManager am = (ActivityManager) context
					.getSystemService(Context.ACTIVITY_SERVICE);
			// The first in the list of RunningTasks is always the foreground
			// task.
			RunningTaskInfo foregroundTaskInfo = am.getRunningTasks(1).get(0);

			// Class Name of top activity);
			String topActivityClassName = foregroundTaskInfo.topActivity
					.getClassName();
			if (validActivities.contains(topActivityClassName)) {
				return true;
			}
		} catch (Exception e) {
			Log.e("isAppSentToBackground", "" + e);
		}
		return false;
	}
}