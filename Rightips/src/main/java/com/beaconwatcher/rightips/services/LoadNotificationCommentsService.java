/*
 * Copyright (C) 2015 RighTips Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.beaconwatcher.rightips.services;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.CommentItem;
import com.bw.libraryproject.utils.BitmapUtilities;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * This service calls API to get details about a specific notification.
 * 
 */
public class LoadNotificationCommentsService extends IntentService {
	private static final String TAG = "LoadNotificationCommentsService";
	private static final int THUMBNAIL_SIZE = 500;

	private Context mContext;
	private String action;
	private String commentID;

	public LoadNotificationCommentsService() {
		// TODO Auto-generated constructor stub
		super("LoadNotificationCommentsService");
	}

	@Override
	protected void onHandleIntent(Intent workIntent) {
		// Gets data from the incoming Intent
		Log.d(TAG, "Sending API call to get notification comments");
		mContext = this;

		action = workIntent
				.getStringExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE);

		RequestParams params = new RequestParams();

		String url = "";
		if (action.equals(AppConstants.ACTION_WALL_GET_COMMENTS)) {
			String uid = workIntent.getStringExtra("uid");
			String msg_id = workIntent.getStringExtra("msg_id");
			String page = workIntent.getStringExtra("page");
			url = "http://www.rightips.com/api/index.php?action=comments_limit&uid="
					+ uid + "&msg_id=" + msg_id + "&p=" + page;
		}

		else if (action.equals(AppConstants.ACTION_WALL_INSERT_COMMENT)) {
			String uid = workIntent.getStringExtra("uid");
			String msg_id = workIntent.getStringExtra("msg_id");
			String comment = workIntent.getStringExtra("comment");
			params.put("comment", comment);
			url = "http://www.rightips.com/api/index.php?action=insertComment&uid="
					+ uid + "&msg_id=" + msg_id;// +"&comment="+comment;

			Uri uri = workIntent.getParcelableExtra("uri");
			if (uri != null) {
				ByteArrayInputStream stream = BitmapUtilities
						.getByteArrayStream(uri, 500, mContext, null);
				if (stream != null) {
					params.put("file", stream, "myimage.jpg");
				}
			}
			url = "http://www.rightips.com/api/index.php?action=insertComment&uid="
					+ uid + "&msg_id=" + msg_id;
		}

		else if (action.equals(AppConstants.ACTION_WALL_LIKE_COMMENT)) {
			String uid = workIntent.getStringExtra("uid");
			commentID = workIntent.getStringExtra("com_id");
			url = "http://www.rightips.com/api/index.php?action=likeComment&uid="
					+ uid + "&com_id=" + commentID + "&rel=Like";
		}

		else if (action.equals(AppConstants.ACTION_WALL_UNLIKE_COMMENT)) {
			String uid = workIntent.getStringExtra("uid");
			commentID = workIntent.getStringExtra("com_id");
			url = "http://www.rightips.com/api/index.php?action=likeComment&uid="
					+ uid + "&com_id=" + commentID + "&rel=Unlike";
		}

		/*
		 * else if(action.equals(AppConstants.ACTION_WALL_GET_MY_WALLS)){ String
		 * uid=workIntent.getStringExtra("uid");
		 * url="http://www.rightips.com/api/index.php?action=my_wall&uid="+uid;
		 * }
		 */

		url = url + "&rand=" + System.currentTimeMillis();
		Log.d(TAG, url);

		AsyncHttpClient httpClient = new AsyncHttpClient();
		httpClient.setTimeout(36000);
		httpClient.setMaxConnections(100);

		try {
			httpClient.post(url, params, responseHandler);// .post(url, params,
															// responseHandler);
		}

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {

		Intent intent = new Intent();

		@Override
		public void onStart() {
			Log.v(TAG, "onStart");
		}

		@Override
		public void onSuccess(String response) {
			Log.v(TAG, "onSuccess response: " + response);

			// {"status":1,"message":"Beacon details","data":[{"msb_id":"41","msb_name":"Testing Beacons","msb_location":"","msb_uuid":"5144A35F387A7AC0B254DFB1381C9DFF","msb_minor":"1","msb_major":"1","msb_txpower":"0","msb_mac_address":"test","site_id":"20","site_title":"ESOL OFFICE","company":"Right Tips","notifications":[{"nt_id":"21","nt_name":"Pizza Hut Special Deal","nt_title":"Fun 4 All","nt_details":"Big Time Treat Inside Box, 2 hot spicy pizzaz, 8 Garlic breads, 500 ml Cold Drink.","zone":"1","template":"http:\/\/beaconwatcher.com\/api\/template.php?nt_id=21"}]}]}
			// Log.d(TAG, "All Notifications Loaded: "+response);
			JSONObject jo = null;
			try {
				jo = new JSONObject(response);
				if (jo.has("status")) {
					int status = jo.getInt("status");
					if (status == 1) {
						parseResults(jo);
					} else if (status == 0) {
						if (jo.has("message")) {
							broadcastMessage(jo.getString("message"));
						} else {
							broadcastError(AppConstants.RESULT_FAILURE);
						}
					}
				}
			} catch (JSONException e) {
				broadcastError("JSON Exception in Comments Webservice");
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
		}

		@Override
		public void onFailure(Throwable error, String content) {
			broadcastError("Failed to load data from Comments Webservice");
			Log.e(TAG, "onFailure error : " + error.toString() + "content : "
					+ content);
		}

		@Override
		public void onFinish() {
			Log.v(TAG, "onFinish");
		}
	};

	private void parseResults(JSONObject jo) {
		if (action.equals(AppConstants.ACTION_WALL_GET_COMMENTS)) {
			if (jo.has("data")) {
				try {
					JSONArray ja = jo.getJSONArray("data");
					ArrayList<CommentItem> arr = new ArrayList<CommentItem>();
					JSONObject jitem;
					CommentItem item;

					for (int i = 0; i < ja.length(); i++) {
						jitem = ja.getJSONObject(i);
						Log.d(TAG, jitem.toString());
						item = parseCommentItem(jitem);
						if (item != null) {
							arr.add(item);
						}
					}
					Log.d(TAG, arr.toString());
					commentsLoaded(arr);
				}

				catch (JSONException e) {
					broadcastError("JSON Exception in Comments Webservice");
					Log.e("log_tag", "Error parsing data " + e.toString());
				}
			}
		}

		else if (action.equals(AppConstants.ACTION_WALL_INSERT_COMMENT)) {
			try {
				JSONObject jitem = jo.getJSONObject("data");
				CommentItem item = parseCommentItem(jitem);
				if (item != null) {
					commentInserted(item);
				}
			}

			catch (JSONException e) {
				broadcastError("JSON Exception in Comments Webservice");
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
		}

		else if (action.equals(AppConstants.ACTION_WALL_REMOVE_COMMENT)) {
			commentRemoved(commentID);
		}

		else if (action.equals(AppConstants.ACTION_WALL_LIKE_COMMENT)) {
			commentLiked(commentID);
		}

		else if (action.equals(AppConstants.ACTION_WALL_UNLIKE_COMMENT)) {
			commentUnLiked(commentID);
		}
	}

	private CommentItem parseCommentItem(JSONObject jitem) {
		CommentItem item = new CommentItem();
		try {
			if (jitem.has("com_id"))
				item.setComID(jitem.getString("com_id"));
			if (jitem.has("uid_fk"))
				item.setUID(jitem.getString("uid_fk"));
			if (jitem.has("comment"))
				item.setComment(jitem.getString("comment"));
			if (jitem.has("created"))
				item.setCreated(jitem.getString("created"));
			if (jitem.has("like_count"))
				item.setLikeCount(jitem.getString("like_count"));
			if (jitem.has("com_img"))
				item.setComImage(jitem.getString("com_img"));
			if (jitem.has("username"))
				item.setUserName(jitem.getString("username"));
			if (jitem.has("name"))
				item.setName(jitem.getString("name"));
			if (jitem.has("profile_pic"))
				item.setProfilePic(jitem.getString("profile_pic"));
			if (jitem.has("is_liked"))
				item.setIsLiked(jitem.getString("is_liked"));
			if (jitem.has("message"))
				item.setMessage(jitem.getString("message"));
			if (jitem.has("msg_id_fk"))
				item.setMessageID(jitem.getString("msg_id_fk"));

		}

		catch (JSONException e) {
			return null;
		}
		return item;
	}

	private void commentsLoaded(ArrayList<CommentItem> arr) {
		Intent intent = new Intent(AppConstants.WALL_COMMENT_SERVICE_COMPLETED);
		intent.putExtra(AppConstants.INTENT_SERVICE_DATA, arr);

		if (action.equals(AppConstants.ACTION_WALL_GET_COMMENTS)) {
			intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
					AppConstants.ACTION_WALL_COMMENTS_LOADED);
		}

		broadcastIntent(intent);
	}

	private void commentInserted(CommentItem item) {
		Intent intent = new Intent(AppConstants.WALL_COMMENT_SERVICE_COMPLETED);
		intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.ACTION_WALL_COMMENT_INSERTED);
		intent.putExtra(AppConstants.INTENT_SERVICE_DATA, item);
		broadcastIntent(intent);
	}

	private void commentRemoved(String removedID) {
		Intent intent = new Intent(AppConstants.WALL_COMMENT_SERVICE_COMPLETED);
		intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.ACTION_WALL_COMMENT_REMOVED);
		intent.putExtra(AppConstants.INTENT_SERVICE_DATA, removedID);
		broadcastIntent(intent);
	}

	private void commentLiked(String id) {
		Intent intent = new Intent(AppConstants.WALL_COMMENT_SERVICE_COMPLETED);
		intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.ACTION_WALL_COMMENT_LIKED);
		intent.putExtra(AppConstants.INTENT_SERVICE_DATA, id);
		broadcastIntent(intent);
	}

	private void commentUnLiked(String id) {
		Intent intent = new Intent(AppConstants.WALL_COMMENT_SERVICE_COMPLETED);
		intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.ACTION_WALL_COMMENT_UNLIKED);
		intent.putExtra(AppConstants.INTENT_SERVICE_DATA, id);
		broadcastIntent(intent);
	}

	private void broadcastError(String msg) {
		Intent intent = new Intent(AppConstants.WALL_COMMENT_SERVICE_COMPLETED);
		intent.putExtra(AppConstants.INTENT_SERVICE_ERROR_MESSAGE, msg);
		broadcastIntent(intent);
	}

	private void broadcastMessage(String msg) {
		Intent intent = new Intent(AppConstants.WALL_COMMENT_SERVICE_COMPLETED);
		intent.putExtra(AppConstants.INTENT_SERVICE_MESSAGE, msg);
		broadcastIntent(intent);
	}

	private void broadcastIntent(Intent intent) {
		LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

	}

}