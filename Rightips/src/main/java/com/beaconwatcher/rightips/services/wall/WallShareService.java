/*
 * Copyright (C) 2015 RighTips Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.beaconwatcher.rightips.services.wall;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

/**
 * This service class RighTips Wall Script when user hit like button on a
 * notification. A page created on wall script is notified to consider a like
 * for that page attached to notification. As it runs, it broadcasts its status
 * using LocalBroadcastManager; any component that wants to see the status
 * should implement a subclass of BroadcastReceiver and register to receive
 * broadcast Intents with category = CATEGORY_DEFAULT and action
 * Constants.BROADCAST_ACTION.
 * 
 */
public class WallShareService extends IntentService {
	private static final String TAG = "WallShareService";
	// Defines a custom Intent action
	public static final String WALL_SHARE_ACTION = "com.beaconwatcher.rightips.services.WallShareService";
	private Context mContext;

	public WallShareService() {
		// TODO Auto-generated constructor stub
		super("WallShareService");
	}

	@Override
	protected void onHandleIntent(Intent workIntent) {
		// Gets data from the incoming Intent
		Log.d(TAG, "Sending Share request to wall script");
		mContext = this;

		String uid = workIntent.getStringExtra("user_id");
		String msg_id = workIntent.getStringExtra("msg_id");

		String url = "https://api.beaconwatcher.com/index.php?action=sharePost&uid="
				+ uid + "&msg_id=" + msg_id + "&rel='Share'";

		AsyncHttpClient httpClient = new AsyncHttpClient();
		try {
			httpClient.get(url, responseHandler);
		}

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
		@Override
		public void onStart() {
			Log.v(TAG, "onStart");
		}

		@Override
		public void onSuccess(String response) {
			Log.v(TAG, "onSuccess");

			// {"status":1,"message":"Beacon details","data":[{"msb_id":"41","msb_name":"Testing Beacons","msb_location":"","msb_uuid":"5144A35F387A7AC0B254DFB1381C9DFF","msb_minor":"1","msb_major":"1","msb_txpower":"0","msb_mac_address":"test","site_id":"20","site_title":"ESOL OFFICE","company":"Right Tips","notifications":[{"nt_id":"21","nt_name":"Pizza Hut Special Deal","nt_title":"Fun 4 All","nt_details":"Big Time Treat Inside Box, 2 hot spicy pizzaz, 8 Garlic breads, 500 ml Cold Drink.","zone":"1","template":"http:\/\/beaconwatcher.com\/api\/template.php?nt_id=21"}]}]}
			// Log.d(TAG, "All Notifications Loaded: "+response);
			JSONObject jo = null;
			try {
				jo = new JSONObject(response);
				if (jo.has("status")) {
					broadcastMessage();
				}
			} catch (JSONException e) {
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
		}

		@Override
		public void onFailure(Throwable error, String content) {
			Log.e(TAG, "onFailure error : " + error.toString() + "content : "
					+ content);
		}

		@Override
		public void onFinish() {
			Log.v(TAG, "onFinish");
		}
	};

	private void broadcastMessage() {
		/*
		 * Creates a new Intent containing a Uri object BROADCAST_ACTION is a
		 * custom Intent action
		 */
		Intent localIntent = new Intent(WALL_SHARE_ACTION);
		// Broadcasts the Intent to receivers in this app.
		LocalBroadcastManager.getInstance(mContext).sendBroadcast(localIntent);
	}
}