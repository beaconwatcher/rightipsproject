package com.beaconwatcher.rightips.services;

import java.lang.reflect.Type;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.events.EventBusEvent;
import com.bw.libraryproject.events.intentservice.IntentServiceEvent;
import com.bw.libraryproject.events.intentservice.NoRecordFoundEvent;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import de.greenrobot.event.EventBus;

public class AbstractIntentService extends IntentService {
	public Type mResultType;

	public String mTag;
	public String mAction;
	public Intent mIntent;

	/*
	 * this will be used to keep reference of last item especially used for
	 * 'remove' api to keep reference of item so that it will return back this
	 * id to physical remove item.
	 */
	public String mItemID = "0";

	public String mBaseUrl;
	public RequestParams mParams;

	public RighTipsApplication mRootApp;
	public Context mContext;

	public AbstractIntentService(String t) {
		super(t);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		mIntent = intent;
		mContext = this;
		mRootApp = (RighTipsApplication) getApplicationContext();

		mTag = mIntent.getStringExtra(AppConstants.INTENT_SERVICE_TAG);
		mAction = mIntent
				.getStringExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE);

		// call this abstract method included in sub classes
		// to generate appropriate url and parameters to be called
		createUrlParameters();

		// After url is created and parameters are set,
		// make webservice call.
		makeWebserviceCall();
	}

	// Abstract Methods to be implemented in sub classes.
	public void createUrlParameters() {
	};

	// public void parseResults(JSONObject jo){};

	public void makeWebserviceCall() {
		if (mBaseUrl != null && mParams != null) {
			mBaseUrl = mBaseUrl + "&rand=" + System.currentTimeMillis();
			mRootApp.APICall(mBaseUrl, mParams, mResponseHandler);
		}
	}

	AsyncHttpResponseHandler mResponseHandler = new AsyncHttpResponseHandler() {
		@Override
		public void onStart() {
			Log.v(mTag, "onStart");
		}

		@Override
		public void onSuccess(String response) {
			Log.v(mTag, "onSuccess response: " + response);
			JSONObject jo = null;
			try {
				jo = new JSONObject(response);
				if (jo.has("status")) {
					Object statusObj=jo.get("status");
					//For Email Verification Webservice, status is String
					//We need to check if it is staring?
					if(statusObj instanceof String){
						if(!jo.getString("status").equals("")){
							Object result=new Gson().fromJson(response, mResultType);
							EventBusEvent evt = new EventBusEvent(
									mIntent.getStringExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE));
							evt.setData(result);
							EventBus.getDefault().post(evt);
						}
					}
					else{
						int status = jo.getInt("status");
						if (status == 0) {
							if (jo.has("message")) {
								EventBus.getDefault()
										.post(new IntentServiceEvent(
												mTag,
												AppConstants.INTENT_SERVICE_MESSAGE,
												jo.getString("message"), null,
												mItemID));
							} else {
								EventBus.getDefault()
										.post(new IntentServiceEvent(
												mTag,
												AppConstants.INTENT_SERVICE_ERROR_MESSAGE,
												jo.getString("message"), null,
												mItemID));
							}
						}

						else if (status == 1) {
							// get preferences called
							if (jo.has("data")) {
								parseResults(jo);
							}
							// Most probably a call made without any response
							// required
							else if (jo.has("message")) {
								EventBus.getDefault()
										.post(new IntentServiceEvent(
												mTag,
												AppConstants.INTENT_SERVICE_MESSAGE,
												jo.getString("message"), mAction,
												mItemID));
							}

						}

						else if (status == 2) {
							EventBus.getDefault()
									.post(new NoRecordFoundEvent(
											mTag,
											ErrorMessageFactory
													.getInstance()
													.getNoRecordMessage(
															mIntent.getStringExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE),
															mContext)));
						}
					}
				}
			} catch (JSONException e) {
				EventBus.getDefault().post(
						new IntentServiceEvent(mTag,
								AppConstants.INTENT_SERVICE_ERROR_MESSAGE,
								"JSON Exception in User Profile Webservice",
								null, mItemID));
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
		}

		@Override
		public void onFailure(Throwable error, String content) {
			EventBus.getDefault().post(
					new IntentServiceEvent(mTag,
							AppConstants.INTENT_SERVICE_ERROR_MESSAGE,
							"Failed to load data from User Profile Webservice",
							null, mItemID));
			Log.e(mTag, "onFailure error : " + error.toString() + "content : "
					+ content);
		}

		@Override
		public void onFinish() {
			Log.v(mTag, "onFinish");
		}
	};

	private void parseResults(JSONObject jo) {
		if (jo.has("data")) {
			try {
				String str = jo.getString("data");
				ArrayList<Object> arr = (ArrayList<Object>) new Gson()
						.fromJson(str, mResultType);
				EventBusEvent evt = new EventBusEvent(
						mIntent.getStringExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE));
				evt.setData(arr);
				EventBus.getDefault().post(evt);
			} catch (JSONException e) {
			}
		}

	}

}
