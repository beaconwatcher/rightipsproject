package com.beaconwatcher.rightips.services;

import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

public class NLService extends NotificationListenerService {

	@Override
	public void onNotificationPosted(StatusBarNotification sbn) {
		if (sbn.getPackageName().equals("com.beaconwatcher.righttips")) {
			// String info=sbn.getNotification().tickerText;
			Log.i("Notification Service", "Beacon info");// ="+info);

		}

		/*
		 * 
		 * //---show current notification---
		 * Log.i("","---Current Notification---"); Log.i("","ID :" + sbn.getId()
		 * + "\t" + sbn.getNotification().tickerText + "\t" +
		 * sbn.getPackageName()); Log.i("","--------------------------");
		 * 
		 * //---show all active notifications---
		 * Log.i("","===All Notifications==="); for (StatusBarNotification notif
		 * : this.getActiveNotifications()) { Log.i("","ID :" + notif.getId() +
		 * "\t" + notif.getNotification().tickerText + "\t" +
		 * notif.getPackageName()); } Log.i("","=======================");
		 */
	}

	@Override
	public void onNotificationRemoved(StatusBarNotification sbn) {
		if (sbn.getPackageName().equals("com.beaconwatcher.righttips")) {
			Log.i("Notification Service", "Beacon info");
		}

		/*
		 * 
		 * Log.i("","---Notification Removed---"); Log.i("","ID :" + sbn.getId()
		 * + "\t" + sbn.getNotification().tickerText + "\t" +
		 * sbn.getPackageName()); Log.i("","--------------------------");
		 */

	}
}
