/*
 * Copyright (C) 2015 RighTips Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.beaconwatcher.rightips.services;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.bw.libraryproject.entities.CategoryGroupData;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

/**
 * This service class get all groups of categories used on Google map to render
 * different markers and filter them. As it runs, it loads all groups and
 * broadcasts its result using LocalBroadcastManager; any component that wants
 * to see the results should implement a subclass of BroadcastReceiver and
 * register to receive broadcast Intents with category =
 * AppConstants.CATEGORY_GROUPS_ACTION
 * 
 */
public class LoadCategoryGroupsService extends IntentService {
	private static final String TAG = "LoadCategoryGroupService";
	// Defines a custom Intent action

	private Context mContext;
	// private int radius=5;
	private String appKey = "MTRfX1JpZ2h0VGlwcw==";

	public LoadCategoryGroupsService() {
		// TODO Auto-generated constructor stub
		super("LoadCategoryGroupService");
	}

	@Override
	protected void onHandleIntent(Intent workIntent) {
		// Gets data from the incoming Intent
		Log.d(TAG, "Sending request to load all groups of categories");
		mContext = this;
		String url = "http://api.beaconwatcher.com/index.php?action=getCatGroups&key="
				+ appKey;
		// http://www.rightips.com/api/index.php?action=likePost&msg_id=80&uid=13&rel=Like
		AsyncHttpClient httpClient = new AsyncHttpClient();
		try {
			httpClient.get(url, responseHandler);
		}

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
		@Override
		public void onStart() {
			Log.v(TAG, "onStart");
		}

		@Override
		public void onSuccess(String response) {
			Log.v(TAG, "onSuccess");

			// {"status":1,"message":"Beacon details","data":[{"msb_id":"41","msb_name":"Testing Beacons","msb_location":"","msb_uuid":"5144A35F387A7AC0B254DFB1381C9DFF","msb_minor":"1","msb_major":"1","msb_txpower":"0","msb_mac_address":"test","site_id":"20","site_title":"ESOL OFFICE","company":"Right Tips","notifications":[{"nt_id":"21","nt_name":"Pizza Hut Special Deal","nt_title":"Fun 4 All","nt_details":"Big Time Treat Inside Box, 2 hot spicy pizzaz, 8 Garlic breads, 500 ml Cold Drink.","zone":"1","template":"http:\/\/beaconwatcher.com\/api\/template.php?nt_id=21"}]}]}
			// Log.d(TAG, "All Notifications Loaded: "+response);
			JSONObject jo = null;
			try {
				jo = new JSONObject(response);
				if (jo.has("status")) {

					if (jo.getInt("status") == 1) {
						groupsLoaded(jo);
					} else {
						String msg = "Error getting all category groups";
						if (jo.has("message")) {
							msg = jo.getString("message");
						}

						broadcastMessage(null, msg);
					}
				}
			} catch (JSONException e) {
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
		}

		@Override
		public void onFailure(Throwable error, String content) {
			Log.e(TAG, "onFailure error : " + error.toString() + "content : "
					+ content);
		}

		@Override
		public void onFinish() {
			Log.v(TAG, "onFinish");
		}
	};

	private void groupsLoaded(JSONObject jo) {
		ArrayList<CategoryGroupData> groups = new ArrayList<CategoryGroupData>();

		try {
			JSONArray arr = jo.getJSONArray("data");

			for (int i = 0; i < arr.length(); i++) {
				JSONObject grp = arr.getJSONObject(i);
				int id = 0;
				String name = "";
				String icon = "";
				String marker = "";

				if (grp.has("cgrp_id")) {
					id = grp.getInt("cgrp_id");
				}
				if (grp.has("cgrp_name")) {
					name = grp.getString("cgrp_name");
				}
				if (grp.has("cgrp_icon")) {
					icon = grp.getString("cgrp_icon");
				}
				if (grp.has("cgrp_marker")) {
					marker = grp.getString("cgrp_marker");
				}

				CategoryGroupData cg = new CategoryGroupData();
				cg.setCatGroupID(id);
				cg.setCatGroupName(name);
				// cg.setCatGroupIcon(icon);
				// cg.setCatGroupMarker(marker);

				groups.add(cg);

			}

		} catch (JSONException e) {
			broadcastMessage(null, "Error parcing JSON in Category Groups");
		}

		broadcastMessage(groups, "");
	}

	private void broadcastMessage(ArrayList<CategoryGroupData> arr, String msg) {
		/*
		 * Creates a new Intent containing a Uri object BROADCAST_ACTION is a
		 * custom Intent action
		 */
		Intent localIntent = new Intent(AppConstants.CATEGORY_GROUPS_ACTION);
		localIntent.putParcelableArrayListExtra("groups", arr);
		localIntent.putExtra("message", msg);
		// Broadcasts the Intent to receivers in this app.
		LocalBroadcastManager.getInstance(mContext).sendBroadcast(localIntent);
	}
}