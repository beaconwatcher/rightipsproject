/*
 * Copyright (C) 2015 RighTips Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.beaconwatcher.rightips.services;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.bw.libraryproject.entities.NotificationData;
import com.bw.libraryproject.utils.NotificationUtilities;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

/**
 * This service calls API to get details about a specific notification.
 * 
 */
public class GetSpecificNotificationService extends IntentService {
	private static final String TAG = "GetSpecificNotificationService";
	// Defines a custom Intent action
	public static final String GET_SPECIFIC_NOTIFICATION_SERVICE = "com.beaconwatcher.rightips.services.GetSpecificNotificationService";

	private Context mContext;
	private String appKey = "MTRfX1JpZ2h0VGlwcw==";
	private Boolean singleNote = false;

	public GetSpecificNotificationService() {
		// TODO Auto-generated constructor stub
		super("GetSpecificNotificationService");
	}

	@Override
	protected void onHandleIntent(Intent workIntent) {
		// Gets data from the incoming Intent
		Log.d(TAG, "Sending API call to get specific notification details");
		mContext = this;
		singleNote = workIntent.getBooleanExtra("singleNote", false);
		String note_id = workIntent.getStringExtra("note_id");
		String site_id = workIntent.getStringExtra("site_id");

		String url = "http://beaconwatcher.com/api/index.php?action=getSpecificNotifications&key="
				+ appKey;

		if (note_id != null) {
			url = url + "&nt_id=" + note_id;
		}

		if (site_id != null) {
			url = url + "&site_id=" + site_id;
		}

		AsyncHttpClient httpClient = new AsyncHttpClient();
		try {
			httpClient.get(url, responseHandler);
		}

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
		@Override
		public void onStart() {
			Log.v(TAG, "onStart");
		}

		@Override
		public void onSuccess(String response) {
			Log.v(TAG, "onSuccess");

			// {"status":1,"message":"Beacon details","data":[{"msb_id":"41","msb_name":"Testing Beacons","msb_location":"","msb_uuid":"5144A35F387A7AC0B254DFB1381C9DFF","msb_minor":"1","msb_major":"1","msb_txpower":"0","msb_mac_address":"test","site_id":"20","site_title":"ESOL OFFICE","company":"Right Tips","notifications":[{"nt_id":"21","nt_name":"Pizza Hut Special Deal","nt_title":"Fun 4 All","nt_details":"Big Time Treat Inside Box, 2 hot spicy pizzaz, 8 Garlic breads, 500 ml Cold Drink.","zone":"1","template":"http:\/\/beaconwatcher.com\/api\/template.php?nt_id=21"}]}]}
			// Log.d(TAG, "All Notifications Loaded: "+response);
			JSONObject jo = null;
			try {
				jo = new JSONObject(response);
				if (jo.has("status")) {

					int status = jo.getInt("status");
					if (status == 1) {
						JSONArray ja = jo.getJSONArray("data");
						ArrayList<NotificationData> arr = new ArrayList<NotificationData>();

						for (int i = 0; i < ja.length(); i++) {
							JSONObject noteJO = ja.getJSONObject(i);
							NotificationData nd = NotificationUtilities
									.getNotificationFromJson(noteJO);
							arr.add(nd);
						}
						broadcastData(arr);
					}
					
					else if (status == 0) {
						if (jo.has("message")) {
							broadcastMessage(jo.getString("message"));
						} else {
							broadcastError(AppConstants.RESULT_FAILURE);
						}
					}
					
				}
			} catch (JSONException e) {
				broadcastMessage(null);
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
		}

		@Override
		public void onFailure(Throwable error, String content) {
			broadcastMessage(null);
			Log.e(TAG, "onFailure error : " + error.toString() + "content : "
					+ content);
		}

		@Override
		public void onFinish() {
			Log.v(TAG, "onFinish");
		}
	};
	
	
	
	private void broadcastMessage(String msg) {
		Intent intent = new Intent(AppConstants.ALL_TRIBES_SERVICE_COMPLETED);
		intent.putExtra(AppConstants.INTENT_SERVICE_MESSAGE, msg);
		broadcastIntent(intent);
	}
	
	private void broadcastError(String msg) {
		Intent intent = new Intent(AppConstants.ALL_TRIBES_SERVICE_COMPLETED);
		intent.putExtra(AppConstants.INTENT_SERVICE_ERROR_MESSAGE, msg);
		broadcastIntent(intent);
	}
	
	private void broadcastData(ArrayList<NotificationData> arr) {
		/*
		 * Creates a new Intent containing a Uri object BROADCAST_ACTION is a
		 * custom Intent action
		 */

		if (arr == null) {
			return;
		}

		Intent intent = new Intent(GET_SPECIFIC_NOTIFICATION_SERVICE);
		if (singleNote == true) {
			intent.putExtra("notification", arr.get(0));
		} else {
			intent.putExtra("notifications", arr);
		}
		//broadcastIntent(intent);
		
		// Broadcasts the Intent to receivers in this app.
		LocalBroadcastManager.getInstance(mContext).sendBroadcast(
				intent);
	}
	
	private void broadcastIntent(Intent intent) {
		sendBroadcast(intent);
	}



}