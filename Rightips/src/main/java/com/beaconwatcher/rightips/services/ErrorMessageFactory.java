package com.beaconwatcher.rightips.services;

import android.content.Context;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;

public class ErrorMessageFactory {
	private static ErrorMessageFactory instance = null;

	private ErrorMessageFactory() {
	}

	public static synchronized ErrorMessageFactory getInstance() {
		if (instance == null) {
			instance = new ErrorMessageFactory();
		}
		return instance;
	}

	public String getNoRecordMessage(String action, Context mContext) {
		if (action.equals(AppConstants.ACTION_GET_SITE_REVIEWS)
				|| action.equals(AppConstants.ACTION_GET_USER_REVIEWS)) {
			return mContext.getResources()
					.getString(R.string.no_record_reviews);
		}

		else if (action.equals(AppConstants.ACTION_GET_USER_LIKES)) {
			return mContext.getResources().getString(R.string.no_record_likes);
		}

		else if (action.equals(AppConstants.ACTION_GET_USER_PHOTOS)) {
			return mContext.getResources().getString(R.string.no_record_photos);
		}

		else if (action.equals(AppConstants.ACTION_GET_MY_WALLS)) {
			return mContext.getResources().getString(R.string.no_record_walls);
		}
		return null;
	}

}
