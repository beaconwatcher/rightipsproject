/*
 * Copyright (C) 2015 RighTips Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.beaconwatcher.rightips.services;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.beaconwatcher.rightips.events.ReviewSavedEvent;
import com.beaconwatcher.rightips.events.ReviewsLoadedEvent;
import com.bw.libraryproject.events.intentservice.IntentServiceEvent;
import com.bw.libraryproject.events.intentservice.NoRecordFoundEvent;
import com.bw.libraryproject.utils.BitmapUtilities;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import de.greenrobot.event.EventBus;

/**
 * This service calls API to get details about a specific notification.
 * 
 */
public class ReviewsService extends IntentService {
	private static final String TAG = "SiteReviewsService";

	private Context mContext;
	private RighTipsApplication mApp;
	private String mTag;
	private String action;

	public ReviewsService() {
		// TODO Auto-generated constructor stub
		super("GetSiteReviewsService");
	}

	@Override
	protected void onHandleIntent(Intent workIntent) {
		// Gets data from the incoming Intent
		Log.d(TAG, "Sending API call to get site reviews data");
		mContext = this;
		mApp = (RighTipsApplication) getApplicationContext();
		mTag = workIntent.getStringExtra(AppConstants.INTENT_SERVICE_TAG);

		action = workIntent
				.getStringExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE);
		// Get Profile data
		if (action.equals(AppConstants.ACTION_GET_SITE_REVIEWS)) {
			if (workIntent.hasExtra(AppConstants.SITE_ID)) {
				String siteID = workIntent.getStringExtra(AppConstants.SITE_ID);
				mApp.APICall(
						"http://www.rightips.com/api/index.php?action=getReviews&site_id="
								+ siteID, new RequestParams(), responseHandler);
			}
		}

		else if (action.equals(AppConstants.ACTION_GET_USER_REVIEWS)) {
			if (workIntent.hasExtra(AppConstants.USER_ID)) {
				String uid = workIntent.getStringExtra(AppConstants.USER_ID);
				mApp.APICall(
						"http://www.rightips.com/api/index.php?action=getReviews&uid="
								+ uid, new RequestParams(), responseHandler);
			}
		}

		else if (action.equals(AppConstants.ACTION_POST_REVIEW)) {
			if (workIntent.hasExtra(AppConstants.SITE_ID)) {
				String siteID = workIntent.getStringExtra(AppConstants.SITE_ID);
				String userID = workIntent.getStringExtra(AppConstants.USER_ID);
				String rev_text = workIntent
						.getStringExtra(AppConstants.REVIEW_TEXT);
				String rev_rating = Float.toString(workIntent.getFloatExtra(
						AppConstants.REVIEW_RATING, -1));

				RequestParams params = new RequestParams();
				params.put("rev_text", rev_text);
				Uri uri = workIntent.getParcelableExtra(AppConstants.IMAGE_URI);
				if (uri != null) {
					ByteArrayInputStream stream = BitmapUtilities
							.getByteArrayStream(uri, 500, mContext, null);
					if (stream != null) {
						params.put("file", stream, "myimage.jpg");
					}
				}

				mApp.APICall(
						"http://www.rightips.com/api/index.php?action=saveReview&uid="
								+ userID + "&site_id=" + siteID + "&rev_rate=3",
						params, responseHandler);
			}
		}

		// Get User Photos
		else if (action.equals(AppConstants.ACTION_GET_USER_PHOTOS)) {
			if (workIntent.hasExtra(AppConstants.USER_ID)) {
				String userID = workIntent.getStringExtra(AppConstants.USER_ID);
				mApp.APICall(
						"http://www.rightips.com/api/index.php?action=userPhotos&uid="
								+ userID, new RequestParams(), responseHandler);
			}
		}
	}

	AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
		@Override
		public void onStart() {
			Log.v(TAG, "onStart");
		}

		@Override
		public void onSuccess(String response) {
			Log.v(TAG, "onSuccess response: " + response);
			JSONObject jo = null;
			try {
				jo = new JSONObject(response);
				if (jo.has("status")) {
					int status = jo.getInt("status");

					if (status == 0) {
						if (jo.has("message")) {
							EventBus.getDefault()
									.post(new IntentServiceEvent(
											mTag,
											AppConstants.INTENT_SERVICE_MESSAGE,
											jo.getString("message"), null, "0"));
						} else {
							EventBus.getDefault()
									.post(new IntentServiceEvent(
											mTag,
											AppConstants.INTENT_SERVICE_ERROR_MESSAGE,
											jo.getString("message"), null, "0"));
						}
					}

					else if (status == 1) {
						// get preferences called
						if (jo.has("data")) {
							parseResults(jo);
						}

						// Most probably save new reviews called;
						else if (jo.has("message")) {
							parseResults(jo);
						}

					}

					else if (status == 2) {
						EventBus.getDefault().post(
								new NoRecordFoundEvent(mTag,
										ErrorMessageFactory.getInstance()
												.getNoRecordMessage(action,
														mContext)));
					}

				}
			} catch (JSONException e) {
				EventBus.getDefault().post(
						new IntentServiceEvent(mTag,
								AppConstants.INTENT_SERVICE_ERROR_MESSAGE,
								"JSON Exception in Reviews Webservice", null,
								"0"));
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
		}

		@Override
		public void onFailure(Throwable error, String content) {
			EventBus.getDefault().post(
					new IntentServiceEvent(mTag,
							AppConstants.INTENT_SERVICE_ERROR_MESSAGE,
							"Failed to load data from Reviews Webservice",
							null, "0"));
			Log.e(TAG, "onFailure error : " + error.toString() + "content : "
					+ content);
		}

		@Override
		public void onFinish() {
			Log.v(TAG, "onFinish");
		}
	};

	private void parseResults(JSONObject jo) {
		if (action.equals(AppConstants.ACTION_GET_SITE_REVIEWS)
				|| action.equals(AppConstants.ACTION_GET_USER_REVIEWS)) {
			if (jo.has("data")) {
				try {
					JSONArray jarr = jo.getJSONArray("data");
					ArrayList<ListItemData> arr = new ArrayList<ListItemData>();

					for (int i = 0; i < jarr.length(); i++) {
						JSONObject jitem = jarr.getJSONObject(i);
						ListItemData review = new ListItemData();
						if (jitem.has("id"))
							review.id = (jitem.getInt("id"));
						if (jitem.has("uid"))
							review.uid = (jitem.getInt("uid"));
						if (jitem.has("rate"))
							review.rate = (jitem.getInt("rate"));
						if (jitem.has("site_id"))
							review.site_id = (jitem.getInt("site_id"));
						if (jitem.has("created"))
							review.created = (jitem.getInt("created"));
						if (jitem.has("likes"))
							review.likes = (jitem.getInt("likes"));
						if (jitem.has("type"))
							review.type = (jitem.getString("type"));
						if (jitem.has("site_title"))
							review.site_title = (jitem.getString("site_title"));
						if (jitem.has("name"))
							review.name = (jitem.getString("name"));
						if (jitem.has("text"))
							review.text = (jitem.getString("text"));
						if (jitem.has("profile_pic"))
							review.profile_pic = (jitem
									.getString("profile_pic"));
						if (jitem.has("img"))
							review.img = (jitem.getString("img"));
						arr.add(review);
					}
					EventBus.getDefault().post(new ReviewsLoadedEvent(arr));
				} catch (JSONException e) {
					EventBus.getDefault().post(
							new IntentServiceEvent(mTag,
									AppConstants.INTENT_SERVICE_ERROR_MESSAGE,
									"JSON Exception in Reviews Webservice",
									null, "0"));
					Log.e("log_tag", "Error parsing data " + e.toString());
				}
			}
		}

		else if (action.equals(AppConstants.ACTION_POST_REVIEW)) {
			if (jo.has("message")) {
				String msg = "";
				try {
					msg = jo.getString("message");
				} catch (JSONException e) {
				}
				;
				EventBus.getDefault().post(new ReviewSavedEvent(msg));
			}
		}
	}
}