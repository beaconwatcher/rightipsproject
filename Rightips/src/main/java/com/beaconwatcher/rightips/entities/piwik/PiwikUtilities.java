package com.beaconwatcher.rightips.entities.piwik;

import com.bw.libraryproject.entities.NotificationData;
import com.google.gson.Gson;

public class PiwikUtilities {
	private static Gson gson = new Gson();

	public static String getPiwikSiteEnterJsonString(String uuid, String major,
			String minor) {
		PiwikSiteEnter se = new PiwikSiteEnter();
		se.uuid = uuid;
		se.major = major;
		se.minor = minor;
		se.site_id = major;

		String str = gson.toJson(se);
		return str;
	}

	public String note_id;
	public String note_title;
	public String note_site_id;
	public String note_site_name;
	public String note_zone;

	public static String getPiwikNotificationString(NotificationData nd) {
		return getPiwikNotificationString(nd.getNoteID(), nd.getTitle(),
				nd.getSiteID(), nd.getSiteName(), nd.getZone());
	}

	public static String getPiwikNotificationString(String n_id,
			String n_title, String n_site_id, String n_site_name, String n_zone) {
		PiwikNotification note = new PiwikNotification();
		note.note_id = n_id;
		note.note_title = n_title;
		note.note_site_id = n_site_id;
		note.note_site_name = n_site_name;
		note.note_zone = n_zone;
		String str = gson.toJson(note);
		return str;
	}
}
