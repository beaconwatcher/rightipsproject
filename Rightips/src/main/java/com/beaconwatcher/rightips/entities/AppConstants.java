package com.beaconwatcher.rightips.entities;

public class AppConstants {
	// Misc
	public static String ACTION_TYPE = "ActionType";
	public static String INVOKING_CONTEXT = "InvokingContext";
	public static String USER_ID = "loggedInUserID";

	// List Item Types
	public static String LIST_ITEM_DATA = "listItemData";
	public static String LIST_ITEM_ID = "listItemID";
	public static String LIST_ITEM_USER_ID = "listItemUserID";
	public static String LIST_ITEM_REPORT_ID = "listItemReportID";
	public static String LIST_ITEM_TYPE = "listItemType";

	public static String LIST_ITEM_TYPE_REVIEW = "listItemTypeReview";
	public static String LIST_ITEM_TYPE_COMMENT = "listItemTypeComment";
	public static String LIST_ITEM_USER_WALLS = "listItemUserWalls";
	public static String LIST_ITEM_TYPE_IMAGE = "listItemTypeImage";
	public static String LIST_ITEM_TYPE_LIKES = "listItemTypeLikes";
	public static String LIST_ITEM_TYPE_PHOTOS_GRID = "listItemTypePhotosGrid";
	public static String LIST_ITEM_TYPE_PHOTOS_VERTICAL = "listItemTypePhotosVertical";
	public static String LIST_ITEM_LOCATIONS = "listItemLocations";
	public static String LIST_ITEM_SEARCH_RESULTS = "listItemSearchResults";

	// List Item Actions
	public static String LIST_ITEM_CLICKED = "ListItemClicked";
	public static String LIST_SHOW_ACTIONS = "ListShowActions";
	public static String LIST_ACTIONS_CANCELED = "ListActionsCancelled";

	public static String REPORT_LIST_ITEM = "ReportListItem";
	public static String REMOVE_LIST_ITEM = "DeleteListItem";
	public static String LIST_ITEM_REMOVED = "ListItemRemoved";
	public static String LIKE_LIST_ITEM = "LikeListItem";
	public static String ACTION_LIKE = "ActionLike";
	public static String ACTION_UNLIKE = "ActionUnlike";

	public static String CURRENT_LOGGED_USER_ID = "CurrentloggedInUserID";

	public static String USER_NAME = "loggedInUserName";
	public static String USER_PROFILE_PIC = "userProfilePicture";
	public static String appKey = "MF9fQlcgU2ltdWxhdGlvbg==";

	// Select Image
	public static String IMAGE_URI = "ImageURI";
	public static String IMAGE_BYTE_ARRAY = "ImageByteArray";
	public static String IMAGE_BITMAP = "ImageBitmap";
	public static String IMAGE_THUMBNAIL_SIZE = "ImageThumbnailSize";
	public static String IMAGE_SELECT_FOR_UPLOAD = "ImageSelectForUpload";
	public static String IMAGE_SELECTED_FOR_UPLOAD = "ImageSelectedForUpload";
	public static String IMAGE_SELECTION_CANCELED = "ImageSelectionCanceled";

	// Intent Service Constants
	public static String INTENT_SERVICE_TAG = "IntentServiceActionTag";
	public static String INTENT_SERVICE_ACTION_TYPE = "IntentServiceActionType";
	public static String INTENT_SERVICE_RESPONSE_TYPE = "IntentServiceResponseType";
	public static String INTENT_SERVICE_MESSAGE = "IntentServiceMessage";
	public static String INTENT_SERVICE_ERROR_MESSAGE = "IntentServiceErrorMessage";
	public static String INTENT_SERVICE_DATA = "IntentServiceData";

	public static final String RESULT_SUCCESS = "Result_Successful";
	public static final String RESULT_FAILURE = "Result_Failure";

	// Locations
	public static String LOCATION = "LocationObject";
	public static String LOCATION_CHANGED = "LocationChanged";
	public static String LOCATION_SOURCE = "LocationSource";
	public static String LOCATION_SOURCE_GPS = "LocationSourceGPS";
	public static String LOCATION_SOURCE_SEARCH = "LocationSourceSearch";

	// Search
	public static String SEARCH_KEYWORD = "SearchKeyword";
	public static String START_SEARCH_WITH_KEYWORD = "StartSearchWithKeyword";
	public static String SEARCH_RESULTS = "SearchResults";
	public static String LAST_SEARCH_PARAMS = "LastSearchParams";
	public static String SEARCH_RESULT_ITEM_CLICKED = "SearchResultItemClicked";
	
	//Search Tags
	public static String SEARCH_TAGS_CLICKED = "SearchTagsClicked";
	

	// Login
	public static String ACTION_LOGIN = "ActionLogin";
	public static String ATTEMPT_LOGIN = "AttemptLogin";
	public static String ACTION_VERIFY_EMAIL = "ActionVerifyEmail";
	

	public static String PREF_LOGGED_IN = "LoggedIn";
	public static String PREF_LOGIN_DATA = "LoggedInData";

	public static String USER_SM_ID = "UserSMID";
	public static String USER_FIRST_NAME = "UserFirstName";
	public static String USER_LAST_NAME = "UserLastName";
	public static String USER_EMAIL = "UserEmail";
	public static String USER_PASSWORD = "UserPassword";
	public static String LOGIN_PROVIDER = "LoginProvider";
	public static String IS_SIGNUP = "IsSignup";

	public static String CITY_NAME = "CityName";

	public static int REQUEST_CODE_LOGIN = 900001;
	public static int REQUEST_CODE_LOGIN_TO_LIKE = 900002;
	public static int REQUEST_CODE_LOGIN_TO_SHARE = 900003;
	public static int REQUEST_CODE_LOGIN_TO_COMMENT = 900004;

	public static int REQUEST_CODE_ENABLE_LOCATION_SERVICES = 900022;

	public static String SELECTED_CATEGORIES = "RightTips_Selected_Categories";

	public static String LAST_LOCATION_NOTIFICATIONS = "LastLocationNotifications";
	public static String LAST_LOCATION_DATA = "LastLocation";
	public static String LAST_LATITUDE = "LastLatitude";
	public static String LAST_LONGITUDE = "LastLongitude";

	// Category Group
	public static String LAST_LOADED_CATEGORY_GROUPS = "LastLoadedCategoryGroups";
	public static String CATEGORY_GROUPS_ACTION = "ActionLoadCategoryGroups";

	// Goup Venues
	public static String CATEGORY_GROUP_VENUES_ACTION = "ActionLoadGroupVenuesAction";

	// Inviate Friend
	public static String ACTION_INVITE_FRIEND = "FriendInvitation";
	public static String INVITATION = "Invitation";
	public static String PENDING_INVITATIONS = "PendingInvitations";

	// My Friends
	public static String ACTION_GET_MY_FRIENDS = "ActionGetMyFriends";

	// Time Zone
	public static String ACTION_TIME_ZONE_FOUND = "TimeZoneFound";
	public static String TIME_ZONE_ID = "TimezoneId";

	// Wall Comments
	public static String WALL_COMMENT_SERVICE_COMPLETED = "WallCommentServiceCompleted";
	public static String WALL_COMMENTS = "WallComments";
	public static String WALL_COMMENT_ID = "CommentID";

	// User Profile
	public static String USER_PROFILE = "UserProfile";
	public static String PROFILE_IMAGE_TYPE_PROFILE = "ProfileImageTypeProfile";
	public static String PROFILE_IMAGE_TYPE_COVER = "ProfileImageTypeCover";

	public static String ACTION_SELECT_PROFILE_IMAGE = "ActionSelectProfileImage";
	public static String ACTION_SELECT_COVER_IMAGE = "ActionSelectCoverImage";

	public static String USER_PROFILE_SERVICE_COMPLETED = "UserProfileServiceCompleted";
	public static String ACTION_GET_USER_PROFILE = "ActionGetUserProfile";

	public static String ACTION_SAVE_USER_PROFILE_IMAGE = "ActionSaveUserProfileImage";
	public static String ACTION_SAVE_USER_COVER_IMAGE = "ActionSaveUserCoverImage";

	public static String ACTION_GET_USER_PHOTOS = "ActionGetUserPhotos";
	public static String PROFILE_PHOTOS_MODE_GRID = "ProfilePhotosModeGrid";
	public static String PROFILE_PHOTOS_MODE_VERTICAL = "ProfilePhotosModeVertical";
	public static String ACTION_SHOW_PREVIEW_IMAGE = "ActionShowPreviewImage";

	public static String RESPONSE_USER_PROFILE_LOADED = "ResponseUserProfileLoaded";
	public static String RESPONSE_USER_PHOTOS_LOADED = "ResponseUserPhotosLoaded";
	public static String USER_COMMENT_PHOTOS = "UserCommentPhotos";

	// Reviews
	public static String SITE_ID = "siteID";
	public static String REVIEWS_MODE = "ReviewsMode";
	public static String REVIEWS_MODE_SITE = "ReviewsModeSite";
	public static String REVIEWS_MODE_USER = "ReviewsModeUser";
	public static String SITE_NAME = "siteName";
	public static String REVIEW_TEXT = "reviewText";
	public static String REVIEW_RATING = "reviewRating";
	public static String REVIEW_IMAGE = "reviewImage";
	public static String ACTION_SELECT_REVIEWS_IMAGE = "ActionSelectReviewsImage";

	public static String SITE_REVIEWS_SERVICE_COMPLETED = "SiteReviewsServiceCompleted";
	public static String ACTION_GET_SITE_REVIEWS = "ActionGetSiteReviews";
	public static String ACTION_GET_USER_REVIEWS = "ActionGetUserReviews";

	public static String RESPONSE_SITE_REVIEWS_LOADED = "ResponseSiteReviewsLoaded";

	public static String ACTION_POST_REVIEW = "ActionPostReview";
	public static String RESPONSE_REVIEW_POSTED = "ResponseReviewPosted";

	// Likes
	public static String ACTION_GET_USER_LIKES = "ActionGetUserLikes";

	// Messages
	public static String RATE_VENUE = "rateVenue";

	// Preferences (Tribes)
	public static String PREF_ALL_TRIBES = "PrefAllTribes";
	public static String ACTION_GET_ALL_TRIBES = "ActionGetAllTribes";
	public static String ACTION_SAVE_USER_TRIBES = "ActionSaveUserTribes";
	public static String ALL_TRIBES_SERVICE_COMPLETED = "AllTribesServiceCompleted";

	public static String SELECTED_TRIBES_IDS = "SelectedTribesIds";
	public static String RESPONSE_ALL_TRIBES_LOADED = "ResponseAllTribesLoaded";
	public static String RESPONSE_USER_TRIBES_SAVED = "ResponseUserTribesSaved";

	// Wall Comments
	public static String ACTION_WALL_COMMENTS_ERROR = "ActionWallCommentsError";
	public static String ACTION_WALL_GET_COMMENTS = "ActionWallGetComments";
	public static String ACTION_WALL_COMMENTS_LOADED = "ActionWallCommentsLoaded";

	public static String ACTION_WALL_INSERT_COMMENT = "ActionWallInsertComment";
	public static String ACTION_WALL_COMMENT_INSERTED = "ActionWallCommentInserted";

	public static String ACTION_WALL_REMOVE_COMMENT = "ActionWallRemoveComment";
	public static String ACTION_WALL_COMMENT_REMOVED = "ActionWallCommentRemoved";

	public static String ACTION_WALL_LIKE_COMMENT = "ActionWallLikeComment";
	public static String ACTION_WALL_COMMENT_LIKED = "ActionWallCommentLiked";

	public static String ACTION_WALL_UNLIKE_COMMENT = "ActionWallUnLikeComment";
	public static String ACTION_WALL_COMMENT_UNLIKED = "ActionWallCommentUnLiked";

	public static String ACTION_WALL_UPLOAD_BITMAP = "ActionWallUploadBitmap";
	public static String ACTION_WALL_BITMAP_UPLOADED = "ActionWallBitmapUploaded";

	public static String ACTION_GET_MY_WALLS = "ActionGetMyWalls";
	public static String ACTION_MY_WALLS_LOADED = "ActionWallMyWallsLoaded";

	public static String ACTION_WALL_REPORT_COMMENT = "ActionWallReportComment";
	public static String RESPONSE_WALL_COMMENT_REPORTED = "ResponseWallCommentReported";

}
