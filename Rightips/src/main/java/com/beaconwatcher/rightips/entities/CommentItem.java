package com.beaconwatcher.rightips.entities;

import android.os.Parcel;
import android.os.Parcelable;

public class CommentItem implements Parcelable {
	private String com_id = "0";
	private String uid_fk = "0";
	private String comment = "";
	private String created = "";
	private String like_count = "0";
	private String com_img = "";
	private String username = "";
	private String name = "";
	private String profile_pic = "";
	private String is_liked = "0";
	private String message = "";
	private String msg_id_fk = "0";

	/**
	 * Standard basic constructor for non-parcel object creation
	 */
	public CommentItem() {
		;
	};

	/**
	 * 
	 * Constructor to use when re-constructing object from a parcel
	 * 
	 * @param in
	 *            a parcel from which to read this object
	 */
	public CommentItem(Parcel in) {
		readFromParcel(in);
	}

	/**
	 * standard getter functions
	 */

	public String getComID() {
		return com_id;
	}

	public String getUID() {
		return uid_fk;
	}

	public String getComment() {
		return comment;
	}

	public String getCreated() {
		return created;
	}

	public String getLikeCount() {
		return like_count;
	}

	public String getComImage() {
		return com_img;
	}

	public String getUserName() {
		return username;
	}

	public String getName() {
		return name;
	}

	public String getProfilePic() {
		return profile_pic;
	}

	public String getIsLiked() {
		return is_liked;
	}

	public String getMessage() {
		return message;
	}

	public String getMessageID() {
		return msg_id_fk;
	}

	/**
	 * standard setter functions
	 */
	public void setComID(String v) {
		com_id = v;
	}

	public void setUID(String v) {
		uid_fk = v;
	}

	public void setComment(String v) {
		comment = v;
	}

	public void setCreated(String v) {
		created = v;
	}

	public void setLikeCount(String v) {
		like_count = v;
	}

	public void setComImage(String v) {
		com_img = v;
	}

	public void setUserName(String v) {
		username = v;
	}

	public void setName(String v) {
		name = v;
	}

	public void setProfilePic(String v) {
		profile_pic = v;
	}

	public void setIsLiked(String v) {
		is_liked = v;
	}

	public void setMessage(String msg) {
		message = msg;
	}

	public void setMessageID(String id) {
		msg_id_fk = id;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// We just need to write each field into the
		// parcel. When we read from parcel, they
		// will come back in the same order

		dest.writeString(com_id);
		dest.writeString(uid_fk);
		dest.writeString(comment);
		dest.writeString(created);
		dest.writeString(like_count);
		dest.writeString(com_img);
		dest.writeString(username);
		dest.writeString(name);
		dest.writeString(profile_pic);
		dest.writeString(is_liked);
		dest.writeString(message);
		dest.writeString(msg_id_fk);
	}

	/**
	 * 
	 * Called from the constructor to create this object from a parcel.
	 * 
	 * @param in
	 *            parcel from which to re-create object
	 */
	private void readFromParcel(Parcel in) {

		// We just need to read back each
		// field in the order that it was
		// written to the parcel
		com_id = in.readString();
		uid_fk = in.readString();
		comment = in.readString();
		created = in.readString();
		like_count = in.readString();
		com_img = in.readString();
		username = in.readString();
		name = in.readString();
		profile_pic = in.readString();
		is_liked = in.readString();
		message = in.readString();
		msg_id_fk = in.readString();
	}

	/**
	 * 
	 * This field is needed for Android to be able to create new objects,
	 * individually or as arrays.
	 * 
	 * This also means that you can use use the default constructor to create
	 * the object and use another method to hyrdate it as necessary.
	 * 
	 * I just find it easier to use the constructor. It makes sense for the way
	 * my brain thinks ;-)
	 * 
	 */
	public static final Creator CREATOR = new Creator() {
		public CommentItem createFromParcel(Parcel in) {
			return new CommentItem(in);
		}

		public CommentItem[] newArray(int size) {
			return new CommentItem[size];
		}
	};

	/*
	 * Very important for making it filterable in Custom
	 * FilterableSortableAdapter
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getName();
	}
}
