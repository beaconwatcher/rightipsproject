package com.beaconwatcher.rightips.entities;

public class UserProfileData {
	public long user_id = 0;
	public int uid = 0;
	public int totalLikes = 0;
	public int following = 0;
	public int totalFollowers = 0;
	public int totalPhotos = 0;
	public int totalReviews = 0;
	public int is_followed = 0;
	public String tribes = "";
	public String first_name = "";
	public String last_name = "";
	public String password = "";
	public String user_name = "";
	public String cover_photo = "";
	public String created = "";
	public String profile_pic = "";
	public String email = "";
	public String provider = "0";
}