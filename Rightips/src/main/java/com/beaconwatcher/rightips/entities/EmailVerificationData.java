package com.beaconwatcher.rightips.entities;

public class EmailVerificationData {
	public String address="";
	public String account="";
	public String domain="";
	public String status="";
	public String error_code="";
	public String error="";
	public boolean disposable=false;
	public boolean role_address=false;
	public double duration=0.0;
}
