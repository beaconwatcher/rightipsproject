package com.beaconwatcher.rightips.entities.piwik;

public class PiwikConstants {
	// Categories
	public static String CATEGORY_APPLICATION = "Application";
	public static String CATEGORY_USERS = "User";
	public static String CATEGORY_VENUE = "Venue";
	public static String CATEGORY_NOTIFICATION = "Notification";
	public static String CATEGORY_LOCATION = "Location";
	public static String CATEGORY_SHARE = "Share";
	public static String CATEGORY_SEARCH = "Search";
	

	// Application Actions
	public static String ACTION_DOWNLOAD = "Download";
	public static String ACTION_START = "Start";

	// User Actions
	public static String ACTION_LOGIN = "Login";
	public static String ACTION_LOGOUT = "LogOut";
	public static String ACTION_SIGNUP = "Signup";

	// Venue Actions
	public static String ACTION_ENTER = "Enter";
	public static String ACTION_EXIT = "Exit";

	// Notification Actions
	public static String ACTION_OPENED = "Opened";
	public static String ACTION_POSTED = "Posted";
	public static String ACTION_LIKE = "Like";
	public static String ACTION_VIEW = "View";

	// Location Actions
	public static String ACTION_LOCATION_SEARCHED = "Location Searched";
	public static String ACTION_CURRENT_LOCATION = "User Location";
	
	// Search Actions
	public static String ACTION_SEARCH_CLICKED = "Search Clicked";
	

	// Share
	public static String ACTION_WHATSAPP = "WhatsApp";
	public static String ACTION_FACEBOOK = "Facebook";

}
