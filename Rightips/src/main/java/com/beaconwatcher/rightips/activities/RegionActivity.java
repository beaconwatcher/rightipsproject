package com.beaconwatcher.rightips.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.regionslides.forv4slider.SlideRegionHistory;
import com.beaconwatcher.rightips.regionslides.forv4slider.SlideRegionPhotos;
import com.bw.libraryproject.entities.LocationData;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

public class RegionActivity extends FragmentActivity {

	MyFragmentAdapter mAdapter;
	ViewPager mPager;
	PageIndicator mIndicator;

	LocationData mLocation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_region);

		mAdapter = new MyFragmentAdapter(getSupportFragmentManager());

		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(mAdapter);

		mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
		mIndicator.setViewPager(mPager);

		/*
		 * getViewTreeObserver().addOnGlobalLayoutListener(new
		 * ViewTreeObserver.OnGlobalLayoutListener() {
		 * 
		 * @Override public void onGlobalLayout() {
		 * getView().getViewTreeObserver().removeGlobalOnLayoutListener(this);
		 * int fragmentWidth = getView().getWidth(); int fragmentHeight =
		 * getView().getHeight(); if(fragmentWidth > 0) { // Width greater than
		 * 0, layout should be done. if(fragmentWidth > fragmentHeight ||
		 * fragmentWidth==fragmentHeight){ //mGridColumns=4;
		 * mSlidePhotos.setNumColumns(4);
		 * mSlidePhotos.setCellSize((fragmentWidth/4)-40); } else{
		 * //mGridColumns=2; mSlidePhotos.setNumColumns(2);
		 * mSlidePhotos.setCellSize((fragmentWidth/2)-40); } } } });
		 */

	}

	class MyFragmentAdapter extends FragmentPagerAdapter {
		public MyFragmentAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Fragment fragment = null;
			Intent intent = getIntent();
			mLocation = intent.getParcelableExtra("location");

			switch (position) {
			case 0:
				SlideRegionPhotos slide = new SlideRegionPhotos(mLocation);
				fragment = slide;
				break;

			case 1:
				SlideRegionHistory slide2 = new SlideRegionHistory(mLocation);
				fragment = slide2;
				break;

			case 2:
				SlideRegionHistory slide3 = new SlideRegionHistory(mLocation);
				fragment = slide3;
				break;

			}

			return fragment;
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return "";
		}

	}
}
