package com.beaconwatcher.rightips.activities;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.beaconwatcher.rightips.R;

public class PermissionsActivity extends PreferenceActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
	}
}