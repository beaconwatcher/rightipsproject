package com.beaconwatcher.rightips.activities;

import java.util.ArrayList;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.UserProfileData;
import com.beaconwatcher.rightips.events.EventBusEvent;
import com.beaconwatcher.rightips.events.TribesSavedEvent;
import com.beaconwatcher.rightips.fragments.FragmentProfileCover;
import com.beaconwatcher.rightips.fragments.FragmentTribes;
import com.beaconwatcher.rightips.services.IntentServiceFactory;
import com.bw.libraryproject.activities.InternetActivity;
import com.bw.libraryproject.customrenderers.CircularImageView;
import com.squareup.picasso.Picasso;

public class UserProfileActivity extends InternetActivity {
	private static final String ERROR_TAG = "User Profile";

	private RighTipsApplication mApp;
	private Context mContext;
	private Intent serviceIntent;

	// Views
	View mMainContainer;
	TextView mTxtNumLikes;
	TextView mTxtNumReviews;
	TextView mTxtNumPhotos;
	TextView mTxtNumFollowers;

	// Fragments
	private FragmentTribes mFragmentTribes;
	private FragmentProfileCover mFragmentProficeCover;

	// Data
	private String mUserID;
	private UserProfileData mProfile;

	private Class mClazz;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TAG = "UserProfileActivity";
		mApp = (RighTipsApplication) getApplicationContext();
		mContext = UserProfileActivity.this;
		mFragmentManager = getFragmentManager();

		setContentView(R.layout.activity_user_profile);

		// Get UI
		mMainContainer = findViewById(R.id.main_container);
		mLoader = (RelativeLayout) findViewById(R.id.loader1);

		mFragmentTribes = (FragmentTribes) getFragmentManager()
				.findFragmentById(R.id.fragment_tribes);
		mFragmentTribes.setBackground(Color.WHITE);

		mFragmentProficeCover = (FragmentProfileCover) getFragmentManager()
				.findFragmentById(R.id.fragment_cover);

		mTxtNumLikes = (TextView) findViewById(R.id.num_likes);
		mTxtNumReviews = (TextView) findViewById(R.id.num_reviews);
		mTxtNumPhotos = (TextView) findViewById(R.id.num_photos);
		// mTxtNumFollowers = (TextView) findViewById(R.id.num_followers);

		findViewById(R.id.btn_likes).setOnClickListener(mClickListener);
		findViewById(R.id.btn_reviews).setOnClickListener(mClickListener);
		findViewById(R.id.btn_photos).setOnClickListener(mClickListener);
		findViewById(R.id.btn_settings).setOnClickListener(mClickListener);

		mUserID = getIntent().getStringExtra(AppConstants.USER_ID);
		String loggedUserID = mApp.getUID();

		if (mUserID != null) {
			showLoader(true);
			// Log.d("FragmentReviews", "getReviews id: "+mSiteID);
			mClazz = IntentServiceFactory.getInstance().getIntentServiceClass(
					AppConstants.ACTION_GET_USER_PROFILE);
			Intent intent = new Intent(mContext, mClazz);
			intent.putExtra(AppConstants.INTENT_SERVICE_TAG, TAG);
			intent.putExtra(AppConstants.USER_ID, mUserID);
			intent.putExtra(AppConstants.CURRENT_LOGGED_USER_ID, loggedUserID);
			intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
					AppConstants.ACTION_GET_USER_PROFILE);
			mContext.startService(intent);

			// Logged in user reached to profile screen.
			if (loggedUserID != null && loggedUserID.equals(mUserID)) {
				findViewById(R.id.btn_settings).setVisibility(View.VISIBLE);
			} else {
				findViewById(R.id.btn_settings).setVisibility(View.INVISIBLE);
				// findViewById(R.id.btn_follow).setVisibility(View.VISIBLE);
			}
		} else {
			showLoader(false);
		}
	}

	OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {

			case R.id.btn_photos:
				showUserPhotos();
				break;

			case R.id.btn_likes:
				showLikes();
				break;

			case R.id.btn_reviews:
				showReviews();
				break;

			case R.id.btn_settings:
				showUserPreferences();
				break;
			}
		}
	};

	private void showLikes() {
		Intent intent = new Intent(mContext, UserLikesActivity.class);
		intent.putExtra(AppConstants.USER_ID, mUserID);
		startActivity(intent);
	}

	private void showReviews() {
		Intent intent = new Intent(mContext, UserReviewsActivity.class);
		intent.putExtra(AppConstants.USER_ID, mUserID);
		startActivity(intent);
	}

	public void onEvent(TribesSavedEvent event) {
		Fragment fragment = getFragmentManager().findFragmentByTag(
				FragmentTribes.class.getName());
		if (fragment != null) {
			getFragmentManager().beginTransaction().remove(fragment).commit();
		}

		String selIDs = event.getMessage();
		if (!selIDs.equals("")) {
			mFragmentTribes.setSelectedItems(selIDs);
		}
	}

	public void onEvent(EventBusEvent event) {
		showLoader(false);
		if (event.getType().equals(AppConstants.ACTION_GET_USER_PROFILE)) {
			ArrayList<UserProfileData> arr = (ArrayList<UserProfileData>) event
					.getData();
			mProfile = arr.get(0);
			if (mProfile != null) {
				updateProfileData();
			}
		}
	}

	private void updateProfileData() {
		mMainContainer.setVisibility(View.VISIBLE);
		mTxtNumLikes.setText(Integer.toString(mProfile.totalLikes));
		mTxtNumReviews.setText(Integer.toString(mProfile.totalReviews));
		mTxtNumPhotos.setText(Integer.toString(mProfile.totalPhotos));
		// mTxtNumFollowers.setText(Integer.toString(mProfile.totalFollowers));

		mFragmentTribes.setSelectedItems(mProfile.tribes);
		mFragmentTribes.setBackground(Color.parseColor("#ffffff"));
		mFragmentProficeCover.setProfileData(mProfile);
	}

	private void showUserPhotos() {
		Intent intent = new Intent(mContext, UserPhotosActivity.class);
		intent.putExtra(AppConstants.USER_ID, mUserID);
		intent.putExtra(AppConstants.USER_NAME, mProfile.user_name);
		intent.putExtra(AppConstants.USER_PROFILE_PIC, mProfile.profile_pic);
		startActivity(intent);
	}

	private void showUserPreferences() {
		if (mApp.isLoggedIn() == true && mApp.getUID().equals(mUserID)) {
			changeFragment(new FragmentTribes(), null);
		} else {
			Bundle b = new Bundle();
			b.putString(AppConstants.SELECTED_TRIBES_IDS, mProfile.tribes);
			changeFragment(new FragmentTribes(), b);
		}
	}

	private void changeFragment(Fragment fragment, Bundle bundle) {
		// Set Fragement based on menu item selected
		if (fragment != null) {
			if (bundle != null) {
				// set Fragmentclass Arguments
				fragment.setArguments(bundle);
			}

			String backStateName = fragment.getClass().getName();
			boolean fragmentPopped = mFragmentManager.popBackStackImmediate(
					backStateName, 0);

			// if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) ==
			// null)
			if (!fragmentPopped && findFragmentByTag(backStateName) == null) {
				// fragment not in back stack, create it.
				mFragmentManager
						.beginTransaction()
						.replace(R.id.fragment_container, fragment,
								backStateName).addToBackStack(backStateName)
						.commit();
			}
		}
	}

}
