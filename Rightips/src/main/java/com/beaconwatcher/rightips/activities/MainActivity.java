package com.beaconwatcher.rightips.activities;

import java.util.ArrayList;
import java.util.Arrays;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.SearchItemData;
import com.beaconwatcher.rightips.entities.UserProfileData;
import com.beaconwatcher.rightips.entities.piwik.PiwikConstants;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.beaconwatcher.rightips.events.EventBusEvent;
import com.beaconwatcher.rightips.events.TribesSavedEvent;
import com.beaconwatcher.rightips.fragments.ContactListFragment;
import com.beaconwatcher.rightips.fragments.FragmentHome;
import com.beaconwatcher.rightips.fragments.FragmentInterface;
import com.beaconwatcher.rightips.fragments.FragmentNotification;
import com.beaconwatcher.rightips.fragments.FragmentProfileCover;
import com.beaconwatcher.rightips.fragments.FragmentSearch;
import com.beaconwatcher.rightips.fragments.FragmentSearchResults;
import com.beaconwatcher.rightips.fragments.FragmentTribes;
import com.beaconwatcher.rightips.fragments.FragmentHistory;
import com.beaconwatcher.rightips.fragments.FragmentMyFriends;
import com.beaconwatcher.rightips.fragments.FragmentRegion;
import com.beaconwatcher.rightips.fragments.FragmentSlider;
import com.beaconwatcher.rightips.services.RighTipsMonitorService;
import com.bw.libraryproject.entities.LocationData;
import com.bw.libraryproject.entities.NotificationData;

public class MainActivity extends RighTipsListActivity implements
		FragmentInterface {
	private static final String TAG = "RighTipsMainActivity";
	// Activity vise
	private RighTipsApplication mApp;
	private Context mContext;
	private String appUUID = "5144A35F387A7AC0B254DFB1381C9DFF";

	// UI Elements
	private DrawerLayout mDrawerLayout;
	private View mMenuContainer;
	private ListView mDrawerList;
	private LinearLayout searchBox;

	// Fragments
	private FragmentProfileCover mFragmentProfileCover;
	private FragmentSearch mFragmentSearch;
	private FragmentHome mFragmentHome;

	// Other variables
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;

	// Drawlist items
	private String[] mMenuTitles;
	private String mMenuItemLogout;
	private String mMenuItemMyWalls;
	private ArrayList<String> mMenuTitlesArrayList;
	private ArrayAdapter<String> mMenuAdapter;

	//Just to check git commit added this comment line.

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		super.onCreate(savedInstanceState);
		mApp = (RighTipsApplication) getApplicationContext();
		mContext = MainActivity.this;
		mApp.savePreferences("RightTipsAppUUID",
				"5144a35f-387a-7ac0-b254-dfb1381c9dff");

		setContentView(R.layout.activity_main);
		
		mFragmentSearch = (FragmentSearch) getFragmentManager().findFragmentById(R.id.fragment_search);
		

		/*
		 * View searchBoxView = getLayoutInflater().inflate(
		 * R.layout.titlebar_with_search, null);
		 * getActionBar().setDisplayShowCustomEnabled(true);
		 * ActionBar.LayoutParams params = new ActionBar.LayoutParams(
		 * ActionBar.LayoutParams.WRAP_CONTENT,
		 * ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
		 * getActionBar().setCustomView(searchBoxView, params);
		 * 
		 * searchBox = (LinearLayout) findViewById(R.id.search_box);
		 * searchBox.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub Intent intent = new Intent(mContext,
		 * SearchLocationActivity.class); startActivityForResult(intent, 10009);
		 * } });
		 */

		// Get UI elements
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mMenuContainer = findViewById(R.id.menu_container);
		mDrawerList = (ListView) findViewById(R.id.drawer_list);

		// Fragments
		mFragmentProfileCover = (FragmentProfileCover) getFragmentManager()
				.findFragmentById(R.id.fragment_cover);
		getFragmentManager().beginTransaction().hide(mFragmentProfileCover)
				.commit();
		mFragmentProfileCover.mProfilePicImageView
				.setOnClickListener(mClickListener);

		// Set Manue Items
		mMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
		mMenuItemLogout = getResources().getString(R.string.menu_item_logout);
		mMenuItemMyWalls = getResources()
				.getString(R.string.menu_item_my_walls);

		mMenuTitlesArrayList = new ArrayList<String>(Arrays.asList(mMenuTitles));
		mMenuAdapter = new ArrayAdapter<String>(this, R.layout.item,
				mMenuTitlesArrayList);
		mDrawerList.setAdapter(mMenuAdapter);

		mDrawerList.setOnItemClickListener(new ListView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> list, View v, int position,
					long id) {
				// TODO Auto-generated method stub
				Log.d(TAG, position + "");

				switch (position) {
				case 4:
					showMyWalls();
					break;

				case 5:
					LogOut();
					break;

				default:
					changeFragment(position, null);
				}
			}
		});

		findViewById(R.id.btnFacebook)
				.setOnClickListener(loginBtnClickListener);
		findViewById(R.id.btnMail).setOnClickListener(loginBtnClickListener);

		findViewById(R.id.btn_menu).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
					mDrawerLayout.closeDrawer(Gravity.LEFT);
				} else {
					mDrawerLayout.openDrawer(Gravity.LEFT);
				}
			}
		});

		// Initialize notification etc
		if (getIntent().hasExtra(RighTipsMonitorService.NOTIFICATION_DATA)) {
			NotificationData nd = getIntent().getParcelableExtra(
					RighTipsMonitorService.NOTIFICATION_DATA);
			if (nd != null) {
				// We can put bundle here, but i have provided null yet.
				Bundle b = new Bundle();
				b.putString("id", nd.getNoteID());
				changeFragment(0, null);
				changeFragment(4, b);
			}
		}

		// If get back from map screen with all notifications loaded
		// for current venue, we need to show them all on slider screen
		
		//NOW CHANGED, we show only 1 notification per venue in map screen.
		//we need to open notification detail screen instead of big slider.
		else if (getIntent().hasExtra("notifications")) {
			ArrayList<NotificationData> arr = getIntent()
					.getParcelableArrayListExtra("notifications");
			if (arr != null) {
/*				// We can put bundle here, but i have provided null yet.
				Bundle b = new Bundle();
				b.putParcelableArrayList("notifications", arr);
				// changeFragment(0, null);
				changeFragment(4, b);
*/				
				
				Bundle bundle = new Bundle();
				bundle.putParcelable("notification", arr.get(0));
				changeFragment(5, bundle);
			}
		}

		else if (savedInstanceState == null) {
			// on first time display view for first nav item
			changeFragment(0, null);
		}

		// Check if user has been logged in previous, then hide login button
		// from drawer and show user image and name;

		if (mApp.isLoggedIn() == true) {
			UserProfileData profile = mApp.getProfile();
			if (profile != null) {
				// Set userid for Piwik tracker.
				String userID = Long.toString(profile.user_id);
				mApp.getTracker().setUserId(userID);
				mApp.getTracker().trackEvent(
						PiwikConstants.CATEGORY_APPLICATION,
						PiwikConstants.ACTION_START, userID);
			}
		} else {
			mApp.getTracker().trackEvent(PiwikConstants.CATEGORY_APPLICATION,
					PiwikConstants.ACTION_START);
		}

	}

	protected OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.profile_pic:
				gotoProfile();
				break;

			case R.id.btnMail:
				break;
			}
		}
	};

	private void gotoProfile() {
		Intent intent = new Intent(mContext, UserProfileActivity.class);
		String mUserID = mApp.getUID();
		intent.putExtra(AppConstants.USER_ID, mUserID);
		mContext.startActivity(intent);
	}

	private void showMyWalls() {
		Intent intent = new Intent(mContext, MyWallsActivity.class);
		intent.putExtra("uid", mApp.getUID());
		startActivity(intent);
	}

	private void LogOut() {
		mApp.setIsLoggedIn(false);
		mApp.setProfile(null);
		mApp.getTracker().setUserId("0");
		mApp.getTracker().trackEvent(PiwikConstants.CATEGORY_APPLICATION,
				PiwikConstants.ACTION_LOGOUT, null);

		findViewById(R.id.login_btn_cont).setVisibility(View.VISIBLE);
		getFragmentManager().beginTransaction().hide(mFragmentProfileCover)
				.commit();

		// mApp.clearApplicationData();

		checkIsLoggedIn();

		// TODO Auto-generated method stub
		/*
		 * Intent intent = new Intent(mContext, LoginActivity.class);
		 * startActivityForResult(intent, LoginActivity.REQUEST_CODE_LOGIN);
		 */}

	private boolean checkIsLoggedIn() {
		// Check if user has been logged in previous, then hide login button
		// from drawer and show user image and name;

		// Logged In
		if (mApp.isLoggedIn()) {
			Log.d(TAG, "previous loggin information found");
			alreadyLoggedIn();

			if (!mMenuTitlesArrayList.contains(mMenuItemMyWalls)) {
				mMenuTitlesArrayList.add(mMenuItemMyWalls);
				mMenuAdapter.notifyDataSetChanged();
			}

			if (!mMenuTitlesArrayList.contains(mMenuItemLogout)) {
				mMenuTitlesArrayList.add(mMenuItemLogout);
				mMenuAdapter.notifyDataSetChanged();
			}
			return true;
		}

		// Not Logged In
		else {
			findViewById(R.id.login_btn_cont).setVisibility(View.VISIBLE);
			if (mMenuTitlesArrayList.contains(mMenuItemMyWalls)) {
				mMenuTitlesArrayList.remove(mMenuItemMyWalls);
				mMenuAdapter.notifyDataSetChanged();
			}

			if (mMenuTitlesArrayList.contains(mMenuItemLogout)) {
				mMenuTitlesArrayList.remove(mMenuItemLogout);
				mMenuAdapter.notifyDataSetChanged();
			}
		}

		return false;
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		startMonitorService(true);
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		startMonitorService(true);
		super.onDestroy();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		checkIsLoggedIn();
		startMonitorService(false);
	}

	private void startMonitorService(Boolean background) {
		if (!mApp.isBLEAvailable) {
			return;
		}

		Intent serviceIntent = new Intent(mContext,
				RighTipsMonitorService.class);
		serviceIntent.putExtra("targetActivity", MainActivity.class.getName());
		serviceIntent.putExtra("isBackground", background);
		Log.d(TAG,
				"Before starting Monitor Service: " + serviceIntent.toString());
		mContext.startService(serviceIntent);

		// mIntent.putExtra("notificationData", mNotificationsData);
		// mIntent.putExtra("targetActivity",
		// NotificationActivity.class.getName());

		// serviceIntent.setClass(context, BeaconMonitorService.class);
		// PendingIntent pintent = PendingIntent.getService(mContext, 0,
		// serviceIntent, 0);

		// If you need to start service after specified number of time
		// then uncomment following 3 lines.
		// In case service is destroyed or self started, following code will
		// start the service again.

		/*
		 * Calendar cal = Calendar.getInstance(); cal.add(Calendar.SECOND, 2);
		 * alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
		 * (1000*60), pintent); // 6*60 * 1000
		 */
	}

	protected OnClickListener loginBtnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			int type = 0;
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.btnFacebook:
				type = 1;
				break;

			case R.id.btnMail:
				type = 0;
				break;
			}

			Intent loginIntent = new Intent(MainActivity.this,
					LoginActivity.class);
			// loginIntent.putExtra(LoginActivity.EXTRAS_ACTIVITY_SOURCE,
			// "main");
			loginIntent.putExtra(LoginActivity.EXTRAS_LOGIN_TYPE, type);
			startActivityForResult(loginIntent,
					LoginActivity.REQUEST_CODE_LOGIN);
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void changeFragment(int pos, Bundle bundle) {
		// Set Fragement based on menu item selected
		Fragment fragment = null;

		switch (pos) {
		case 0:
			// fragment = new NotificationFragment();
			fragment = new FragmentHome();
			mFragmentHome = (FragmentHome) fragment;
			showSearch();
			break;

		case 1:
			fragment = new FragmentTribes();
			hideSearch();
			break;

		case 2:
			fragment = new FragmentHistory();
			showSearch();
			break;
			
		case 3:
			fragment = new FragmentMyFriends();
			hideSearch();
			break;
			
		case 4:
			fragment = new FragmentSlider();
			hideSearch();
			break;

		case 5:
			fragment = new FragmentNotification();
			showSearch();
			break;

		case 7:
			fragment = new ContactListFragment();
			showSearch();
			break;

		case 8:
			fragment = new FragmentRegion();
			hideSearch();
			break;
		default:
			showSearch();
			break;
		}

		if (fragment != null) {
			if (bundle != null) {
				// set Fragmentclass Arguments
				fragment.setArguments(bundle);
			}

			String backStateName = fragment.getClass().getName();
			boolean fragmentPopped = mFragmentManager.popBackStackImmediate(
					backStateName, 0);

			// if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) ==
			// null)
			if (!fragmentPopped && findFragmentByTag(backStateName) == null) {
				// fragment not in back stack, create it.
				mFragmentManager
						.beginTransaction()
						.replace(R.id.fragment_container, fragment,
								backStateName).addToBackStack(backStateName)
						.commit();
			}

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(pos, true);
			mDrawerList.setSelection(pos);
			// setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mMenuContainer);
			
			
			if(backStateName.equals(FragmentHome.class.getName())){
				//checkLastLocation();
			}
		}

	}

	@Override
	public void onBackPressed() {
		if (mFragmentManager.getBackStackEntryCount() == 1) {
			finish();
		} else {
			super.onBackPressed();

			String n = mFragmentManager.getBackStackEntryAt(
					mFragmentManager.getBackStackEntryCount() - 1).getName();
			if (n.equals(FragmentHome.class.getName())) {
				showSearch();
				//checkLastLocation();
			} else {
				hideSearch();
			}

		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		// super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == LoginActivity.REQUEST_CODE_LOGIN
				&& resultCode == LoginActivity.LOGIN_RESULT_CODE) {
			Log.d(TAG, "Login activity returned");
			checkIsLoggedIn();
		}
	}

	private void alreadyLoggedIn() {
		findViewById(R.id.login_btn_cont).setVisibility(View.GONE);
		getFragmentManager().beginTransaction().show(mFragmentProfileCover)
				.commit();

		Log.d(TAG, "getting login data from preferences");
		final UserProfileData profile = (UserProfileData) mApp
				.getGSONPreferences(AppConstants.PREF_LOGIN_DATA, "",
						UserProfileData.class);

		if (profile != null) {
			String uid = Integer.toString(profile.uid);
			mApp.getTracker().setUserId(uid);
			mFragmentProfileCover.setProfileData(profile);
		}
	}

	/***********************************************************
	 * Event Bus posted events
	 **********************************************************/
	public void onEvent(ApplicationEvent event) {
		if (event.getType().equals(AppConstants.SEARCH_RESULT_ITEM_CLICKED)) {
			String id = (String) event.getData();
			Bundle bundle = new Bundle();
			bundle.putString("id", id);
			changeFragment(5, bundle);
		}
	}

	public void onEvent(TribesSavedEvent event) {
		changeFragment(0, null);
	}

	public void onEvent(EventBusEvent event) {
		if (event.getType().equals(AppConstants.START_SEARCH_WITH_KEYWORD)) {
			ArrayList<SearchItemData> arr = (ArrayList<SearchItemData>) event
					.getData();
			String str = mApp.getJSONStringFromClassObject(arr);

			Bundle b = new Bundle();
			b.putString(AppConstants.SEARCH_RESULTS, str);
			changeFragment(new FragmentSearchResults(),
					R.id.fragment_container, b);
		}
	}

	/***********************************************************
	 * RighTipsService Notification receiver or Fragment Events
	 **********************************************************/

	/*
	 * When broadcast is received, usually from RighTipsMonitorSerice and it is
	 * on certain fragment It should react accordingly.
	 */
	@Override
	public void onNotificationBroadcastReceived(NotificationData nd) {
		// TODO Auto-generated method stub
		//

		Boolean ifAnyFragmentOnTop = false;

		Fragment f = findFragmentByTag(FragmentHistory.class.getName());
		if (f != null) {
			ifAnyFragmentOnTop = true;
			((FragmentHistory) f).getAllNotifications();
		}

		Fragment f2 = findFragmentByTag(FragmentSlider.class.getName());
		if (f2 != null) {
			ifAnyFragmentOnTop = true;
			((FragmentSlider) f2).getAllNotifications();
		}

		Fragment f3 = findFragmentByTag(FragmentNotification.class.getName());
		if (f3 != null) {
			ifAnyFragmentOnTop = true;
			((FragmentNotification) f3).showNotification(nd);
		}

		// If it is on HOME page and no fragment is open,
		// then we need to open slider when new notificaiton is arrived.
		if (ifAnyFragmentOnTop == false) {
			Bundle b = new Bundle();
			b.putString("id", nd.getNoteID());
			changeFragment(4, b);
		}
	}

	// When settings saved, we need to remove that fragment
	@Override
	public void onFragmentSuicide(String tag) {
		// TODO Auto-generated method stub
		// fm.beginTransaction().remove(fm.findFragmentByTag(tag)).commit();
		// fm.beginTransaction().replace(R.id.fragment_container, new
		// HomeFragment()).commit();
		changeFragment(0, null);
	}

	@Override
	public void onHistoryItemClicked(NotificationData nd) {

		// TODO Auto-generated method stub
		// Toast.makeText(mContext, nd.getTitle(), Toast.LENGTH_LONG).show();
		Bundle b = new Bundle();
		b.putString("id", nd.getNoteID());
		changeFragment(5, b);
	}

	@Override
	public void onInviteFriendsClicked() {
		// TODO Auto-generated method stub
		changeFragment(7, null);
	}

	@Override
	public void onSliderItemClicked(ArrayList<NotificationData> arr) {
		// TODO Auto-generated method stub
		// If minislider "See All" button clicked then this arr contains all
		// location slides.
		// We have to show big slider with all location slides.
		if (arr.size() > 1) {
			Bundle bundle = new Bundle();
			bundle.putParcelableArrayList("notifications", arr);
			changeFragment(4, bundle);
		}
		// Most probably it came from history item click
		// show notificaiton details page
		else {
			Bundle bundle = new Bundle();
			bundle.putParcelable("notification", arr.get(0));
			changeFragment(5, bundle);
		}
	}

	@Override
	public void onMapIconClicked(NotificationData nd) {
		Intent mapIntent = new Intent(MainActivity.this,
				GoogleMapActivity.class);
		mapIntent.putExtra("notification", nd);
		startActivity(mapIntent);
	}

	@Override
	public void onRegionIconClicked(LocationData ld) {
		Bundle b = new Bundle();
		b.putParcelable("location", ld);
		changeFragment(8, b);
	}
	
	
	
	
	private void hideSearch(){
		if(mFragmentSearch!=null){
			getFragmentManager().beginTransaction().hide(mFragmentSearch).commit();
		}
	}
	
	private void showSearch(){
		if(mFragmentSearch!=null){
			getFragmentManager().beginTransaction().show(mFragmentSearch).commit();
		}
	}

}
