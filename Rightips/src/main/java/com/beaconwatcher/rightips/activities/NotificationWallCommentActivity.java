package com.beaconwatcher.rightips.activities;

import java.io.IOException;
import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.webkit.ValueCallback;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.customviews.UserNameTextView;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.CommentItem;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.beaconwatcher.rightips.events.EventBusEvent;
import com.beaconwatcher.rightips.fragments.FragmentPreviewImage;
import com.beaconwatcher.rightips.fragments.FragmentSelectImage;
import com.beaconwatcher.rightips.services.LoadNotificationCommentsService;
import com.bw.libraryproject.customrenderers.CircularImageView;
import com.bw.libraryproject.events.intentservice.NoRecordFoundEvent;
import com.bw.libraryproject.utils.BitmapUtilities;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import de.greenrobot.event.EventBus;

public class NotificationWallCommentActivity extends RighTipsListActivity {
	private static final String ERROR_TAG = "Notification Wall";

	private RighTipsApplication mApp;
	private Context mContext;
	private TextView mTitle;
	private ScrollView mScrollView;
	private LinearLayout mCommentsList;
	private ImageView mBtnSelectFile;
	private RelativeLayout mBtnPostComment;
	private LayoutInflater mLayoutInflater;
	private EditText mTfComment;
	private RelativeLayout mBottomArea;
	private RelativeLayout mBtnEarlier;
	private RelativeLayout mCommentActionContainer;

	private static final int FILECHOOSER_REQUESTCODE = 2888;
	private static final int TAKE_PHOTO_REQUESTCODE = 2889;
	private ValueCallback<Uri> mUploadMessage;
	private Uri mCapturedImageURI = null;

	// Data
	private ArrayList<CommentItem> mCommentsArray = new ArrayList<CommentItem>();
	private String userID = "0";
	private String msgID = "0";
	private int currentPage = 0;
	private int imageLastIndex = 0;
	private int mBottomAreaY = 0;

	private final static int INTERVAL = 30000;
	private boolean loadingData = false;
	private Intent serviceIntent;
	private int biggestCommentID = 0;
	private boolean firstTime = true;

	// Fragments
	FragmentSelectImage mFragmentSelectImage;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mApp = (RighTipsApplication) getApplicationContext();
		mContext = NotificationWallCommentActivity.this;
		TAG = "Notification Wall Comments";

		mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		setContentView(R.layout.activity_notification_comment);

		View topBarView = getLayoutInflater().inflate(
				R.layout.titlebar_with_comment_back, null);
		getActionBar().setDisplayShowCustomEnabled(true);
		ActionBar.LayoutParams params = new ActionBar.LayoutParams(
				ActionBar.LayoutParams.FILL_PARENT,
				ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
		getActionBar().setCustomView(topBarView, params);

		// Get UI
		mLoader = (RelativeLayout) findViewById(R.id.loader);
		mTitle = (TextView) findViewById(R.id.tf_note_title);
		mCommentsList = (LinearLayout) findViewById(R.id.list_comments);
		mScrollView = (ScrollView) findViewById(R.id.scroll_view);
		mBottomArea = (RelativeLayout) findViewById(R.id.bottom_area);
		mBtnSelectFile = (ImageView) findViewById(R.id.btn_choose_file);
		mTfComment = (EditText) findViewById(R.id.tf_comment);
		mBtnPostComment = (RelativeLayout) findViewById(R.id.btn_post_comment);
		mBtnPostComment.setEnabled(false);
		// mBtnPostComment.setEnabled(false);
		mBtnEarlier = (RelativeLayout) findViewById(R.id.btn_earlier);

		// Set button click listeners
		mBtnSelectFile.setOnClickListener(mClickListener);
		mBtnPostComment.setOnClickListener(mClickListener);
		mBtnEarlier.setOnClickListener(mClickListener);

		mTfComment.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				checkIfCanPost();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		mScrollView.getViewTreeObserver().addOnScrollChangedListener(
				new OnScrollChangedListener() {
					@Override
					public void onScrollChanged() {
						// TODO Auto-generated method stub
						int yy = mScrollView.getScrollY();

						if (yy < 20 && mCommentsArray.size() > 8) {
							mBtnEarlier.setVisibility(View.VISIBLE);
						} else {
							mBtnEarlier.setVisibility(View.INVISIBLE);
						}
					}
				});

		mBottomArea.addOnLayoutChangeListener(new OnLayoutChangeListener() {
			@Override
			public void onLayoutChange(View v, int left, int top, int right,
					int bottom, int oldLeft, int oldTop, int oldRight,
					int oldBottom) {
				// TODO Auto-generated method stub
				if (mBottomAreaY == 0) {
					mBottomAreaY = bottom;
				}
				// scrollToBottom();
			}
		});

		Intent intent = getIntent();
		String title = intent.getStringExtra("title");
		if (title != null) {
			mTitle.setText(title);
		}

		// Register Receivers
		addReceiver(wallServiceBroadcastReceiver, wallFillter);

		mHandler = new Handler();
		serviceIntent = new Intent(mContext,
				LoadNotificationCommentsService.class);
		serviceIntent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.ACTION_WALL_GET_COMMENTS);
		serviceIntent.putExtra(AppConstants.INTENT_SERVICE_TAG,TAG);
	}

	private void addImageSelection() {
		mFragmentSelectImage = new FragmentSelectImage();
		getFragmentManager()
				.beginTransaction()
				.add(R.id.fragment_container, mFragmentSelectImage,
						FragmentSelectImage.class.getName())
				.addToBackStack(FragmentSelectImage.class.getName()).commit();
	}

	public void onEvent(ApplicationEvent event) {
		super.onEvent(event);
		if (event.getType().equals(AppConstants.IMAGE_SELECTION_CANCELED)) {
			removeImageSelection();
		}

		else if (event.getType().equals(AppConstants.IMAGE_SELECTED_FOR_UPLOAD)) {
			mCapturedImageURI = (Uri) ((ApplicationEvent) event).getData();
			try {
				Bitmap bmp = BitmapUtilities.getThumbnail(mCapturedImageURI,
						150, mContext);
				((ImageView) findViewById(R.id.img_to_post))
						.setImageBitmap(bmp);
			} catch (IOException e) {
			}
			removeImageSelection();
		}
	}

	public void onEvent(EventBusEvent event) {
		super.onEvent(event);
		if (event.getType().equals(AppConstants.REMOVE_LIST_ITEM)) {
			ArrayList<ListItemData> arr = (ArrayList<ListItemData>) event
					.getData();
			String itemID = Integer.toString(arr.get(0).id);
			removeCommentFromList(itemID);
		}
	}
	

	private void removeImageSelection() {
		if (mFragmentSelectImage != null) {
			getFragmentManager().beginTransaction()
					.remove(mFragmentSelectImage).commit();
			mFragmentSelectImage = null;
		}
	}

	private void checkIfCanPost() {
		if (mTfComment.getText().toString().equals("")
				&& mCapturedImageURI == null) {
			mBtnPostComment.setEnabled(false);
			mBtnPostComment.setSelected(false);
		}

		else {
			mBtnPostComment.setEnabled(true);
			mBtnPostComment.setSelected(true);
		}

	}

	@Override
	public void onResume() {
		super.onResume();
		// TODO Auto-generated method stub
		loadingData = false;
		userID = getIntent().getStringExtra("uid");
		msgID = getIntent().getStringExtra("msg_id");

		if ((userID != null && !userID.equals("0"))
				&& (msgID != null && !msgID.equals("0"))) {
			serviceIntent.putExtra("uid", userID);
			serviceIntent.putExtra("msg_id", msgID);
			startServiceTimer();
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		stopServiceTimer();
		super.onPause();
	}

	private Handler mHandler;
	Runnable mServiceTimer = new Runnable() {
		@Override
		public void run() {
			// Toast.makeText(mContext, "30 seconds passed",
			// Toast.LENGTH_SHORT).show();

			if (loadingData == false && mBottomArea.getBottom() == mBottomAreaY
					&& mFragmentSelectImage == null) {
				getComments(currentPage);
			}
			mHandler.postDelayed(mServiceTimer, INTERVAL);
		}
	};

	void startServiceTimer() {
		mServiceTimer.run();
	}

	void stopServiceTimer() {
		mHandler.removeCallbacks(mServiceTimer);
	}

	IntentFilter wallFillter = new IntentFilter(
			AppConstants.WALL_COMMENT_SERVICE_COMPLETED);
	BroadcastReceiver wallServiceBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			showLoader(false);
			loadingData = false;
			String msg = intent
					.getStringExtra(AppConstants.INTENT_SERVICE_MESSAGE);
			if (msg != null) {
				mApp.showToast(mContext, msg, Toast.LENGTH_SHORT);
				return;
			}
			String errorMsg = intent
					.getStringExtra(AppConstants.INTENT_SERVICE_ERROR_MESSAGE);
			String actionType = intent
					.getStringExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE);
			if (errorMsg != null) {
				hideFileChooser();
				// Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
				mApp.showErrorToast(mContext, ERROR_TAG);
				return;
			}

			if (actionType.equals(AppConstants.ACTION_WALL_COMMENTS_LOADED)) {
				mCommentsArray = intent
						.getParcelableArrayListExtra(AppConstants.INTENT_SERVICE_DATA);
				if (mCommentsArray != null) {
					commentsLoaded();
				}
			}

			else if (actionType
					.equals(AppConstants.ACTION_WALL_COMMENT_INSERTED)) {
				hideFileChooser();
				CommentItem item = intent
						.getParcelableExtra(AppConstants.INTENT_SERVICE_DATA);
				if (item != null) {
					InsertCommentItemIntoView(item, -1);
				}
			}

			else if (actionType.equals(AppConstants.ACTION_WALL_COMMENT_LIKED)) {
				String id = intent
						.getStringExtra(AppConstants.INTENT_SERVICE_DATA);
				updatCommentLikes(id, true);
			}

			else if (actionType
					.equals(AppConstants.ACTION_WALL_COMMENT_UNLIKED)) {
				String id = intent
						.getStringExtra(AppConstants.INTENT_SERVICE_DATA);
				updatCommentLikes(id, false);
			}

			else if (actionType
					.equals(AppConstants.RESPONSE_WALL_COMMENT_REPORTED)) {
				mApp.showToast(mContext,
						mContext.getResources().getString(R.string.lbl_thanks),
						Toast.LENGTH_LONG);
				getFragmentManager().popBackStack(null,
						FragmentManager.POP_BACK_STACK_INCLUSIVE);
				mCommentActionContainer.setVisibility(View.INVISIBLE);
				getComments(currentPage);
			}
		}
	};

	private void commentsLoaded() {
		mCommentsList.removeAllViews();

		// First get index of image comes at last in list.
		// so that we trigger scrollToBottom event on that image load.
		CommentItem comment;
		if (firstTime == true) {
			for (int i = 0; i < mCommentsArray.size(); i++) {
				comment = mCommentsArray.get(i);
				if (!comment.getComImage().equals("")) {
					imageLastIndex = i;
				}
			}
		}

		for (int i = 0; i < mCommentsArray.size(); i++) {
			comment = mCommentsArray.get(i);
			InsertCommentItemIntoView(comment, i);
		}

		if (firstTime == true) {
			firstTime = false;
			scrollToBottom();
		}

	}

	OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {

			case R.id.btn_choose_file:
				addImageSelection();
				break;

			case R.id.btn_back:
				finish();
				break;

			case R.id.btn_post_comment:
				postComment();
				break;

			case R.id.btn_dots:
				CommentItem c = (CommentItem) v.getTag();
				ListItemData d = new ListItemData();
				d.id = Integer.parseInt(c.getComID());
				d.uid = Integer.parseInt(c.getUID());
				d.type = "comment";
				EventBus.getDefault().post(
						new ApplicationEvent(AppConstants.LIST_SHOW_ACTIONS, d,
								null));
				break;

			case R.id.btn_cancel_action:
				mCommentActionContainer.setVisibility(View.INVISIBLE);
				break;

			case R.id.txt_likes:
				likeClicked((CommentItem) v.getTag());
				break;

			case R.id.btn_earlier:
				currentPage++;
				getComments(currentPage);
				break;

			case R.id.img_comment:
				showBigPreview((String) v.getTag());
				break;
			}
		}
	};

	private void hideFileChooser() {
		if (mFragmentSelectImage != null) {
			getFragmentManager().beginTransaction()
					.remove(mFragmentSelectImage).commit();
			mFragmentSelectImage = null;
		}
	}

	private final void getComments(int page) {
		showLoader(true);
		loadingData = true;
		serviceIntent.putExtra("page", Integer.toString(page));
		mContext.startService(serviceIntent);
	}

	private void removeCommentFromList(String id) {
		for (int i = 0; i < mCommentsList.getChildCount(); i++) {
			View v = mCommentsList.getChildAt(i);
			CommentItem comment = (CommentItem) v.getTag();
			if (comment.getComID().equals(id)) {
				mCommentsList.removeViewAt(i);
			}
		}
	}

	private void postComment() {
		if (mTfComment.getText().toString().equals("")
				&& mCapturedImageURI == null) {
			Toast.makeText(
					mContext,
					mContext.getResources().getString(
							R.string.wall_comment_enter_text),
					Toast.LENGTH_SHORT).show();
			return;
		}

		String comment = "";
		if (!mTfComment.getText().toString().equals("")) {
			comment = mTfComment.getText().toString();
		}

		showLoader(true);
		loadingData = true;
		Intent intent = new Intent(mContext,
				LoadNotificationCommentsService.class);
		intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.ACTION_WALL_INSERT_COMMENT);
		intent.putExtra("uid", userID);
		intent.putExtra("msg_id", msgID);
		intent.putExtra("comment", comment);

		if (mCapturedImageURI != null) {
			intent.putExtra("uri", mCapturedImageURI);
		}

		mContext.startService(intent);
		mCapturedImageURI = null;
		((ImageView) findViewById(R.id.img_to_post)).setImageBitmap(null);

		mTfComment.setText("");
		View view = this.getCurrentFocus();
		mApp.hideKeyboard(view);
	}

	private void likeClicked(CommentItem comment) {
		showLoader(true);
		loadingData = true;
		Intent intent = new Intent(mContext,
				LoadNotificationCommentsService.class);
		intent.putExtra("uid", userID);
		intent.putExtra("com_id", comment.getComID());

		if (comment.getIsLiked().equals("0")) {
			intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
					AppConstants.ACTION_WALL_LIKE_COMMENT);
		} else {
			intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
					AppConstants.ACTION_WALL_UNLIKE_COMMENT);
		}

		mContext.startService(intent);
	}

	private void updatCommentLikes(String id, Boolean b) {
		for (int i = 0; i < mCommentsList.getChildCount(); i++) {
			View v = mCommentsList.getChildAt(i);
			CommentItem comment = (CommentItem) v.getTag();
			if (comment.getComID().equals(id)) {
				TextView tv = (TextView) v.findViewById(R.id.txt_likes);
				comment.setIsLiked(b == true ? "1" : "0");
				int totalLikes = Integer.parseInt(comment.getLikeCount());
				totalLikes = (b == true ? totalLikes + 1 : totalLikes - 1);
				comment.setLikeCount(Integer.toString(totalLikes));
				setLikeText(comment, tv);
			}
		}
	}

	private void setLikeText(CommentItem comment, TextView tv) {
		String str = "";
		int totalLikes = Integer.parseInt(comment.getLikeCount());
		if (comment.getIsLiked().equals("1")) {
			str = mContext.getResources().getString(
					R.string.wall_comment_txt_likes)
					+ "(" + totalLikes + ")";
			tv.setTextColor(Color.parseColor("#FF1C50"));
		} else {
			tv.setTextColor(Color.parseColor("#000000"));
			if (totalLikes > 0) {
				str = mContext.getResources().getString(
						R.string.wall_comment_txt_likes)
						+ "(" + totalLikes + ")";
			} else {
				str = mContext.getResources().getString(
						R.string.wall_comment_txt_like);
			}
		}
		tv.setText(str);
	}

	private void InsertCommentItemIntoView(CommentItem comment, final int index) {
		checkIfCanPost();

		View listItem = mLayoutInflater.inflate(
				R.layout.wall_comment_list_item, null);
		mCommentsList.addView(listItem);

		listItem.setTag(comment);
		listItem.addOnLayoutChangeListener(new OnLayoutChangeListener() {
			@Override
			public void onLayoutChange(View v, int left, int top, int right,
					int bottom, int oldLeft, int oldTop, int oldRight,
					int oldBottom) {
				if (v.getWidth() > 0) {
					v.removeOnLayoutChangeListener(this);
					CommentItem comment = (CommentItem) v.getTag();
					if (!comment.getComImage().equals("")) {
						final View loader = (RelativeLayout) v
								.findViewById(R.id.loader_list_item);
						if (loader != null)
							loader.setVisibility(View.VISIBLE);
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
								v.getWidth(), v.getHeight());
						loader.setLayoutParams(params);

						ImageView img = (ImageView) v
								.findViewById(R.id.img_comment);
						img.setTag(comment.getComImage());
						img.setOnClickListener(mClickListener);
						// new PicassoFitWidth(img, img.getWidth(),
						// Picasso.with(mContext).load(comment.getComImage()));
						Picasso.with(mContext).load(comment.getComImage())
								.resize(img.getWidth(), 0)
								.into(img, new Callback() {
									@Override
									public void onSuccess() {
										if (imageLastIndex > 0) {
											imageLastIndex = 0;
											scrollToBottom();
										}
										loader.setVisibility(View.INVISIBLE);
									}

									@Override
									public void onError() {
										loader.setVisibility(View.INVISIBLE);
									}
								});
					}

					CircularImageView img = (CircularImageView) v
							.findViewById(R.id.img_profile);
					img.setBorderWidth(2);

					if (!comment.getProfilePic().equals("")
							&& !comment.getProfilePic().equals("null")) {
						Picasso.with(mContext).load(comment.getProfilePic())
								.resize(img.getWidth(), img.getHeight())
								.into(img);
					}

					FrameLayout btnDots = (FrameLayout) v
							.findViewById(R.id.btn_dots);
					btnDots.setTag(comment);
					btnDots.setOnClickListener(mClickListener);
				}
			}
		});

		long miliseconds = Long.parseLong(comment.getCreated() + "000");
		CharSequence charSequence = DateUtils
				.getRelativeTimeSpanString(miliseconds);
		StringBuilder sb = new StringBuilder(charSequence.length());
		sb.append(charSequence);
		String duration = sb.toString();

		((UserNameTextView) listItem.findViewById(R.id.txt_username))
				.setText(comment.getName());
		((UserNameTextView) listItem.findViewById(R.id.txt_username))
				.setUserID(comment.getUID());

		((TextView) listItem.findViewById(R.id.txt_comment)).setText(comment
				.getComment());
		((TextView) listItem.findViewById(R.id.txt_duration)).setText(duration
				+ " - ");

		TextView txtLikes = ((TextView) listItem.findViewById(R.id.txt_likes));
		if (txtLikes != null) {
			txtLikes.setTag(comment);
			txtLikes.setOnClickListener(mClickListener);
			setLikeText(comment, txtLikes);
		}
	}

	private void scrollToBottom() {
		mScrollView.post(new Runnable() {
			@Override
			public void run() {
				mScrollView.fullScroll(ScrollView.FOCUS_DOWN);
			}
		});
	}

	// BIG IMAGE
	// PREVIEW**********************************************************

	private void showBigPreview(String url) {
		Bundle b = new Bundle();
		b.putString(AppConstants.IMAGE_URI, url);
		ViewGroup mRootView = (ViewGroup) getWindow().getDecorView()
				.findViewById(android.R.id.content);
		FrameLayout mFragmentContainer = new FrameLayout(this);
		mFragmentContainer.setLayoutParams(new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		mFragmentContainer.setId(9999);
		mRootView.addView(mFragmentContainer);

		Fragment fragment = new FragmentPreviewImage();
		fragment.setArguments(b);
		changeFragment(fragment, mFragmentContainer.getId(), null);
	}
}
