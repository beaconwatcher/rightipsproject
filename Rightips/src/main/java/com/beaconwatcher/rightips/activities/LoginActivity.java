package com.beaconwatcher.rightips.activities;

import java.util.ArrayList;

import org.brickred.socialauth.Profile;
import org.brickred.socialauth.android.DialogListener;
import org.brickred.socialauth.android.SocialAuthAdapter;
import org.brickred.socialauth.android.SocialAuthAdapter.Provider;
import org.brickred.socialauth.android.SocialAuthError;
import org.brickred.socialauth.android.SocialAuthListener;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ContactListItem;
import com.beaconwatcher.rightips.entities.UserProfileData;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.beaconwatcher.rightips.events.EventBusEvent;
import com.beaconwatcher.rightips.events.TribesSavedEvent;
import com.beaconwatcher.rightips.fragments.FragmentInterface;
import com.beaconwatcher.rightips.fragments.FragmentTribes;
import com.beaconwatcher.rightips.fragments.FragmentLogin;
import com.beaconwatcher.rightips.services.IntentServiceFactory;
import com.bw.libraryproject.activities.InternetActivity;
import com.bw.libraryproject.customtasks.ConvertFBPicIDtoURLTask;
import com.bw.libraryproject.customtasks.ConvertFBPicIDtoURLTask.FBProfilePicUrlHandler;
import com.bw.libraryproject.entities.LocationData;
import com.bw.libraryproject.entities.NotificationData;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

public class LoginActivity extends InternetActivity implements
		FragmentInterface {
	private static final String ERROR_TAG = "Login Webservice";

	public static String EXTRAS_ACTIVITY_SOURCE = "extraActivitySource";
	public static String EXTRAS_LOGIN_TYPE = "extraLoginType";
	public static String EXTRAS_ACTIVITY_RESULT = "extraActivityResult";

	public static int REQUEST_CODE_SIGNUP = 1000;
	public static int REQUEST_CODE_LOGIN = 1001;
	public static int LOGIN_RESULT_CODE = 1109;

	private RighTipsApplication mApp;
	private Context mContext;

	// Social Auth instance
	SocialAuthAdapter adapter;

	private FragmentManager fm;

	private String appUUID = "5144A35F387A7AC0B254DFB1381C9DFF";
	private AsyncHttpResponseHandler handler;

	// To show who invited to launch this app.
	private ContactListItem invitation;

	// Used to check if login activity is launched itself
	// or user clicked login button on Home Drawer.
	private int loginType = -1;

	private UserProfileData mPreLoginProfile;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TAG = "LoginActivity";

		mApp = (RighTipsApplication) getApplicationContext();
		mContext = LoginActivity.this;
		fm = getFragmentManager();

		adapter = new SocialAuthAdapter(new DialogListener() {
			@Override
			public void onError(SocialAuthError arg0) {
				mApp.hideDialog();
				Log.d("FACEBOOK", arg0.toString());
			}

			@Override
			public void onComplete(Bundle response) {
				// Connected to facebook, fetch user profile.
				getFacebookProfile();
				Log.d("FACEBOOK", "connected");
			}

			@Override
			public void onCancel() {
				mApp.hideDialog();
				Log.d("FACEBOOK", "Authentication Cancellled");
			}

			@Override
			public void onBack() {
				Log.d("FACEBOOK", "Authentication Cancelled by Back Button");
			}
		});

		// if(checkNetworkConnection()){
		loginType = getIntent()
				.getIntExtra(LoginActivity.EXTRAS_LOGIN_TYPE, -1);

		// Came to login activity by pressing button on Home Drawer
		if (loginType == 0 || loginType == 1) {
			switch (loginType) {
			case 0:
				loginWithMail();
				break;

			case 1:
				loginWithFacebook();
				break;

			default:
				loginWithMail();
				break;
			}
		}

		// Came to login activity by directly launching activity
		else {
			setContentView(R.layout.activity_login);

			findViewById(R.id.btnFacebook).setOnClickListener(clickListener);
			findViewById(R.id.btnMail).setOnClickListener(clickListener);
			findViewById(R.id.btnSkip).setOnClickListener(clickListener);

			// Came to login with friend invitation feature
			// if(checkNetworkConnection()){
			invitation = getIntent()
					.getParcelableExtra(AppConstants.INVITATION);
			if (invitation != null) {
				findViewById(R.id.invitation_container).setVisibility(
						View.VISIBLE);
				// If photo url found, it is most probably FB image.
				if (!invitation.getPhotoUrl().equals("")) {
					ImageView img = (ImageView) findViewById(R.id.invitation_pic);
					Picasso.with(mContext).load(invitation.getPhotoUrl())
							.into(img);
				}

				TextView tv = (TextView) findViewById(R.id.invitation_text);
				String str = "<b>" + invitation.getName() + "</b> "
						+ getResources().getString(R.string.invitation_txt2)
						+ "<br/><br/>"
						+ getResources().getString(R.string.invitation_txt3)
						+ " <b>" + invitation.getName() + "</b> "
						+ getResources().getString(R.string.invitation_txt4);

				tv.setText(Html.fromHtml(str));

				// If already logged in, we need to hide login buttons and
				// replace
				// Skip text to "Accept Invitation";
				if (!mApp.getPreferences(AppConstants.PREF_LOGGED_IN, "")
						.equals("")) {
					findViewById(R.id.btn_container).setVisibility(
							View.INVISIBLE);
					TextView btnSkip = (TextView) findViewById(R.id.btnSkip);
					btnSkip.setText("Accept Invitation");
					btnSkip.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							gotoHomeScreen();
						}
					});
				}
			}
		}

	}

	protected OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.btnFacebook:
				loginWithFacebook();
				break;

			case R.id.btnMail:
				loginWithMail();
				break;

			case R.id.btnSkip:
				showSettings();
				break;
			}

		}
	};

	protected void loginWithMail() {
		FragmentTransaction fragmentTransaction = fm.beginTransaction();
		fragmentTransaction.add(android.R.id.content, new FragmentLogin());
		fragmentTransaction.commit();
	}

	protected void loginWithFacebook() {
		mApp.showDialog(mContext, "Facebook Login", "Please wait...");
		adapter.authorize(LoginActivity.this, Provider.FACEBOOK);
	}

	protected void getFacebookProfile() {
		adapter.getUserProfileAsync(new SocialAuthListener<Profile>() {
			@Override
			public void onExecute(String str, final Profile profile) {
				// After getting social media profile, we need to login on
				// RighTips login system.
				Log.i("Facebook Profile", profile.toString());

				// No need to keep social auth login.
				adapter.signOut(mContext, Provider.FACEBOOK.toString());

				// After login, we need to find profile picture url from profile
				// id.
				ConvertFBPicIDtoURLTask task = new ConvertFBPicIDtoURLTask(
						profile.getProfileImageURL(),
						new FBProfilePicUrlHandler() {
							@Override
							public void onFoundUrl(String resultUrl) {

								// TODO Auto-generated method stub
								mPreLoginProfile = new UserProfileData();
								mPreLoginProfile.user_id = Long
										.parseLong(profile.getValidatedId());
								mPreLoginProfile.uid = 0;
								mPreLoginProfile.email = profile.getEmail() == null ? ""
										: profile.getEmail();
								mPreLoginProfile.password = "";
								mPreLoginProfile.provider = "1";
								mPreLoginProfile.first_name = profile
										.getFirstName();
								mPreLoginProfile.last_name = profile
										.getLastName();
								mPreLoginProfile.profile_pic = resultUrl;
								login();
							}

							@Override
							public void onError() {
								// TODO Auto-generated method stub

							}
						});

				task.execute();
			}

			@Override
			public void onError(SocialAuthError err) {
				// TODO Auto-generated method stub
				mApp.showErrorToast(mContext, ERROR_TAG);
				// Toast.makeText(mContext, err.getMessage(),
				// Toast.LENGTH_SHORT).show();
			}
		});
	}

	// we call this after Facebook Login.
	public void login() {

		// Hide Soft Keyboard.
		View view = this.getCurrentFocus();
		mApp.hideKeyboard(view);

		if (mPreLoginProfile.provider == "0") {
			mApp.showDialog(mContext,
					mContext.getResources().getString(R.string.api_call),
					mContext.getResources().getString(R.string.lbl_signing_in));
		}
		showLoader(true);
		// Log.d("FragmentReviews", "getReviews id: "+mSiteID);
		Class mClazz = IntentServiceFactory.getInstance()
				.getIntentServiceClass(AppConstants.ACTION_LOGIN);
		Intent intent = new Intent(mContext, mClazz);
		intent.putExtra(AppConstants.INTENT_SERVICE_TAG, TAG);
		intent.putExtra(AppConstants.USER_SM_ID, mPreLoginProfile.user_id);
		intent.putExtra(AppConstants.USER_FIRST_NAME,
				mPreLoginProfile.first_name);
		intent.putExtra(AppConstants.USER_LAST_NAME, mPreLoginProfile.last_name);
		intent.putExtra(AppConstants.USER_EMAIL, mPreLoginProfile.email);
		intent.putExtra(AppConstants.USER_PASSWORD, mPreLoginProfile.password);
		intent.putExtra(AppConstants.LOGIN_PROVIDER, mPreLoginProfile.provider);
		intent.putExtra(AppConstants.IS_SIGNUP, "0");
		intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.ACTION_LOGIN);
		mContext.startService(intent);
	}

	// When login service completed
	public void onEvent(EventBusEvent event) {
		mApp.hideDialog();
		if (event.getType().equals(AppConstants.ACTION_LOGIN)) {
			ArrayList<UserProfileData> arr = (ArrayList<UserProfileData>) event
					.getData();
			UserProfileData mProfile = arr.get(0);
			if (mProfile != null) {
				loggedIn(mProfile);
			}
		}
	}

	// when login fragment fires event to attempt login.
	public void onEvent(ApplicationEvent evt) {
		mPreLoginProfile = (UserProfileData) evt.getData();
		login();
	}

	public void loggedIn(UserProfileData profile) {
		mApp.savePreferences(AppConstants.PREF_LOGGED_IN, "1");
		mApp.saveGSONPreferences(AppConstants.PREF_LOGIN_DATA, profile);
		Toast.makeText(mContext,
				"Welcome " + profile.user_name + ", You are logged in.",
				Toast.LENGTH_LONG).show();

		// If login activity was launched by click login button from home drawer
		// then we need to send login result back to drawer activity.
		if (loginType != -1) {

			String str = mApp.getJSONStringFromClassObject(profile);
			Intent intent = new Intent();
			intent.putExtra(EXTRAS_ACTIVITY_RESULT, str);
			setResult(LOGIN_RESULT_CODE, intent);
			finish();
		}

		// In case login activity was launched independentaly
		// It is most probably launched after first installation.
		// We need to goto setting activity.
		else {
			showSettings();
		}
	}

	protected void showSettings() {
		// need to tell setting fragment to start HOME activity
		// after user click "Save" button.
		// Because settings fragment was NOT launched by drawer.
		Bundle bundle = new Bundle();
		bundle.putString("activity", "login");
		FragmentTribes fragInfo = new FragmentTribes();
		fragInfo.setArguments(bundle);
		fm.beginTransaction().add(android.R.id.content, fragInfo).commit();
	}

	/***********************************************************
	 * Event Bus posted events
	 **********************************************************/
	public void onEvent(TribesSavedEvent event) {
		gotoHomeScreen();
	}

	private void gotoHomeScreen() {
		Intent i = new Intent(mContext, MainActivity.class);
		startActivity(i);
		finish();
	}

	// Empty implementations of FragmentInterface
	@Override
	public void onHistoryItemClicked(NotificationData nd) {
	}

	@Override
	public void onInviteFriendsClicked() {
	}

	@Override
	public void onMapIconClicked(NotificationData nd) {
	}

	@Override
	public void onRegionIconClicked(LocationData ld) {
	}

	@Override
	public void onSliderItemClicked(ArrayList<NotificationData> arr) {
	}

	// When setting activity kills itself
	@Override
	public void onFragmentSuicide(String tag) {
		// TODO Auto-generated method stub
		gotoHomeScreen();
	}

}
