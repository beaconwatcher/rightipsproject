package com.beaconwatcher.rightips.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.beaconwatcher.rightips.services.RighTipsMonitorService;
import com.bw.libraryproject.activities.InternetActivity;
import com.bw.libraryproject.entities.NotificationData;

/*
 * This Activity listens for Broadcast of Notification from RighTipsMonitorService
 * It receive the broadcast and call it's onNotificationReceived function.
 * Other activities extend this as base class and override onNotificationReceived function.
 * Most probably child activities call db to fetch updated records.
 */
public class NotificationReceiverActivity extends InternetActivity {
	private NotificationReceiver notificationReceiver;

	public void onNotificationBroadcastReceived(NotificationData nd) {
	};

	@Override
	public void onPause() {
		if (notificationReceiver != null) {
			try {
				unregisterReceiver(notificationReceiver);
				notificationReceiver = null;
			} catch (IllegalArgumentException e) {
				Log.e("NotificationReceiverActivity",
						"cannot unregister receiver");
			}
		}
		super.onPause();
	}

	@Override
	public void onResume() {
		if (notificationReceiver == null) {
			try {
				notificationReceiver = new NotificationReceiver();
				IntentFilter filter = new IntentFilter();
				filter.addAction(RighTipsMonitorService.FOREGROUND_NOTIFICATION);
				registerReceiver(notificationReceiver, filter);
			} catch (IllegalArgumentException e) {
				Log.e("NotificationReceiverActivity",
						"cannot register receiver");
			}
		}
		super.onResume();
	}

	private class NotificationReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(
					RighTipsMonitorService.FOREGROUND_NOTIFICATION)) {
				NotificationData nd = (NotificationData) intent
						.getParcelableExtra(RighTipsMonitorService.NOTIFICATION_DATA);
				onNotificationBroadcastReceived(nd);
			}
		}
	}
}
