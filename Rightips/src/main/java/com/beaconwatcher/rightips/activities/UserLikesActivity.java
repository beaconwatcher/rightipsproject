package com.beaconwatcher.rightips.activities;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.customviews.CustomToggleGroup;
import com.beaconwatcher.rightips.customviews.CustomToggleGroup.OnCheckedChangeListener;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.fragments.FragmentUserLikes;
import com.bw.libraryproject.activities.InternetActivity;

public class UserLikesActivity extends RighTipsListActivity {
	private static final String ERROR_TAG = "User Likes";

	private RighTipsApplication mApp;
	private Context mContext;
	private Intent serviceIntent;

	// Views
	private View mMainContainer;
	private LayoutInflater mInflater;

	// Fragments
	FragmentUserLikes mLikesFragment;

	// Data
	private String mUserID;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TAG = "UserLikesActivity";
		mApp = (RighTipsApplication) getApplicationContext();
		mContext = this;
		mInflater = getLayoutInflater();

		mFragmentManager = getFragmentManager();

		setContentView(R.layout.activity_user_likes);

		View titleBar = getLayoutInflater().inflate(
				R.layout.titlebar_user_likes, null);
		getActionBar().setDisplayShowCustomEnabled(true);
		ActionBar.LayoutParams params = new ActionBar.LayoutParams(
				ActionBar.LayoutParams.FILL_PARENT,
				ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
		getActionBar().setCustomView(titleBar, params);

		// Get UI
		mMainContainer = findViewById(R.id.main_container);
		mLikesFragment = (FragmentUserLikes) getFragmentManager()
				.findFragmentById(R.id.fragment_likes);

		CustomToggleGroup grp = (CustomToggleGroup) findViewById(R.id.tab_bar);
		grp.setOnCheckedChangeListener(tabListener);
		findViewById(R.id.btn_back).setOnClickListener(mClickListener);

		Intent intent = getIntent();
		if (intent != null) {
			mUserID = intent.getExtras().getString(AppConstants.USER_ID);
			mLikesFragment.loadLikes(mUserID);
		}
	}

	OnCheckedChangeListener tabListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CustomToggleGroup group, int checkedId) {
			// TODO Auto-generated method stub
			// TODO Auto-generated method stub
			switch (checkedId) {
			case R.id.btn_likes:
				break;

			case R.id.btn_likers:
				break;
			}

		}
	};

	OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {

			case R.id.btn_back:
				finish();
				break;
			}
		}
	};

}
