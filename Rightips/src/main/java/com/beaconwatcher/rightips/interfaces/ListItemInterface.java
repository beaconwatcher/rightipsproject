package com.beaconwatcher.rightips.interfaces;

public interface ListItemInterface {
	public void onListItemClicked(Object obj);

	public void onShowActionsClicked(Object obj);
}
