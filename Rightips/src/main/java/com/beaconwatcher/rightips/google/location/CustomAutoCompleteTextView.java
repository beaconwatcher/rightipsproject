package com.beaconwatcher.rightips.google.location;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import com.bw.libraryproject.entities.PlaceData;

/**
 * Customizing AutoCompleteTextView to return Place Description corresponding to
 * the selected item
 * 
 * We could use AutoCompleteTextView but since we are using custom entity
 * PlaceData instead of just String for adapter we need to create custom
 * textview just to fetch only desired field "description" to show in textview.
 * 
 */
public class CustomAutoCompleteTextView extends AutoCompleteTextView {

	public CustomAutoCompleteTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/** Returns the place description corresponding to the selected item */
	@Override
	protected CharSequence convertSelectionToString(Object selectedItem) {
		/**
		 * Each item in the autocompetetextview suggestion list is a PlaceData
		 * object
		 */
		PlaceData pd = (PlaceData) selectedItem;
		return pd.getDescription();
	}
}