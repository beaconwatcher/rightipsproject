package com.beaconwatcher.rightips.events;

public class TribesSavedEvent {
	private String message;

	public TribesSavedEvent(String msg) {
		message = msg;
	}

	public String getMessage() {
		return message;
	}
}