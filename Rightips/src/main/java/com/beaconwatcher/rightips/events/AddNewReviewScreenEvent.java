package com.beaconwatcher.rightips.events;

public class AddNewReviewScreenEvent {
	private float rating = -1;

	public AddNewReviewScreenEvent(float rating) {
		this.rating = rating;
	}

	public float getRating() {
		return rating;
	}
}