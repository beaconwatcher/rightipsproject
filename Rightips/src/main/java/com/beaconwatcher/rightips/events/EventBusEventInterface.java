package com.beaconwatcher.rightips.events;

public interface EventBusEventInterface {
	public String getType();

	public void setData(Object data);

	public Object getData();
}
