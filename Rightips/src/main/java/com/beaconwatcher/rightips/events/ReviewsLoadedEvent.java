package com.beaconwatcher.rightips.events;

import java.util.ArrayList;

import com.beaconwatcher.rightips.entities.ListItemData;

public class ReviewsLoadedEvent {
	private ArrayList<ListItemData> reviewsArray;

	public ReviewsLoadedEvent(ArrayList<ListItemData> arr) {
		reviewsArray = arr;
	}

	public ArrayList<ListItemData> getReviews() {
		return reviewsArray;
	}
}