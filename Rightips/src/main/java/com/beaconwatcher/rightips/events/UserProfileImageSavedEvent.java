package com.beaconwatcher.rightips.events;

import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.UserProfileData;

public class UserProfileImageSavedEvent {
	private String imageUrl = "";
	private String action = "";

	public UserProfileImageSavedEvent(String url, String action) {
		this.imageUrl = url;
		this.action = action;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public String getImageType() {
		return action.equals(AppConstants.ACTION_SAVE_USER_PROFILE_IMAGE) ? AppConstants.PROFILE_IMAGE_TYPE_PROFILE
				: AppConstants.PROFILE_IMAGE_TYPE_COVER;
	}

}