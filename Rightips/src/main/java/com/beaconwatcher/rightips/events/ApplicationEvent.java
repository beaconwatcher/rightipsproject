package com.beaconwatcher.rightips.events;

public class ApplicationEvent {
	private String sender_action;
	private String type;
	private Object data;

	public ApplicationEvent(String type, Object data, String action) {
		this.type = type;
		this.data = data;
		this.sender_action = action;
	}

	public String getType() {
		return type;
	}

	public Object getData() {
		return data;
	}

	public String getSenderAction() {
		return sender_action;
	}
}