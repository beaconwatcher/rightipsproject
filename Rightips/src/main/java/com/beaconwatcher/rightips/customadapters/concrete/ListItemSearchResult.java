package com.beaconwatcher.rightips.customadapters.concrete;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.ListItemData;

public class ListItemSearchResult extends ListItemBase {
	private ImageView mImage;
	private ViewGroup mTagsContainer;
	private TextView mTxtTitle;
	private TextView mTxtSite;
	private TextView mTxtDistance;
	private Context mContext;

	public ListItemSearchResult(Context context, String type) {
		super(context, type);
		mContext = context;
		LayoutInflater.from(context).inflate(R.layout.list_item_search_result,
				this, true);
		setupChildren();
	}

	protected void setupChildren() {
		mImage = (ImageView) findViewById(R.id.img);
		mTagsContainer = (ViewGroup) findViewById(R.id.tag_container);
		mTxtTitle = (TextView) findViewById(R.id.txt_title);
		mTxtSite = (TextView) findViewById(R.id.txt_site);
		mTxtDistance = (TextView) findViewById(R.id.txt_distance);
	}

	@Override
	public void setItem(final Object obj) {
		super.setItem(obj);
		ListItemData item = (ListItemData) obj;
		if (item.img != null && !item.img.equals("")
				&& !item.img.equals("null"))
			loadImage(mImage, item.img, findViewById(R.id.loader_list_item));
		else
			mImage.setImageBitmap(null);

		mTxtTitle.setText(item.title);
		mTxtSite.setText(item.site_title);
		mTxtDistance.setText(item.distance + "km");

		mTagsContainer.removeAllViews();
		TextView tv;
		
		if(item.tags!=null && item.tags.length > 0){
			for (int i = 0; i < item.tags.length; i++) {
				tv = new TextView(mContext);
				tv.setText(item.tags[i]);
				tv.setTextColor(Color.WHITE);
				tv.setBackgroundColor(mContext.getResources().getColor(
						R.color.rightips_pink));
				tv.setPadding(10, 10, 10, 10);
				LinearLayout.LayoutParams parmas = new LinearLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				parmas.setMargins(0, 10, 10, 0); // llp.se
				tv.setLayoutParams(parmas);
				mTagsContainer.addView(tv);
			}
		}
	}
}
