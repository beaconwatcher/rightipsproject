package com.beaconwatcher.rightips.customadapters.concrete;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.ListItemData;

public class ListItemPhotosGrid extends ListItemBase {
	private ImageView mImage;

	public ListItemPhotosGrid(Context context, String type) {
		super(context, type);
		LayoutInflater.from(context).inflate(R.layout.list_item_photos_grid,
				this, true);
		setupChildren();
	}

	protected void setupChildren() {
		mImage = (ImageView) findViewById(R.id.img);
		mImage.setOnClickListener(mClickListener);
	}

	@Override
	public void setItem(final Object obj) {
		super.setItem(obj);
		ListItemData item = (ListItemData) obj;
		if (item.img != null && !item.img.equals("")
				&& !item.img.equals("null"))
			loadImage(mImage, item.img, findViewById(R.id.loader_list_item));
		else
			mImage.setImageBitmap(null);
	}
}
