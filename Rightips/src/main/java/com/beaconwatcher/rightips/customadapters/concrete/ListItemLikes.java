package com.beaconwatcher.rightips.customadapters.concrete;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.beaconwatcher.rightips.entities.UserLikes;
import com.bw.libraryproject.utils.DateUtilities;

public class ListItemLikes extends ListItemBase {
	private Context mContext;
	private TextView mTitle;
	private TextView mText;
	private TextView mDuration;
	private ImageView mProfile;
	private ImageView mImage;
	private View mBtnDots;

	public ListItemLikes(Context context, String type) {
		super(context, type);
		mContext = context;
		LayoutInflater.from(context).inflate(R.layout.list_item_likes, this,
				true);
		setupChildren();
	}

	protected void setupChildren() {
		mTitle = (TextView) findViewById(R.id.txt_username);
		mText = (TextView) findViewById(R.id.txt_comment);
		mDuration = (TextView) findViewById(R.id.txt_duration);
		mProfile = (ImageView) findViewById(R.id.img_profile);
		mImage = (ImageView) findViewById(R.id.img);
		mBtnDots = findViewById(R.id.btn_dots);
		// mBtnDots.setVisibility(View.INVISIBLE);
		if (mBtnDots != null)
			mBtnDots.setOnClickListener(mClickListener);
	}

	@Override
	public void setItem(final Object obj) {
		super.setItem(obj);
		ListItemData item = (ListItemData) obj;
		item.deleteable = false;
		if (mTitle != null) {
			mTitle.setText(item.name + " liked");
		}
		if (mText != null)
			mText.setText(item.type.equals("notification") ? "notification "
					+ item.title : item.type + " " + item.title);
		if (mDuration != null)
			mDuration.setText(DateUtilities.getDuration(Integer
					.toString(item.created)));

		if (item.profile_pic != null && !item.profile_pic.equals("")
				&& !item.profile_pic.equals("null"))
			loadImage(mProfile, item.profile_pic, null);
		else
			mProfile.setImageBitmap(null);

		if (item.img != null && !item.img.equals("")
				&& !item.img.equals("null"))
			loadImage(mImage, item.img, findViewById(R.id.loader_list_item));
		else
			mImage.setImageBitmap(null);

		if (item.type.equals("notification")) {
			mBtnDots.setVisibility(View.INVISIBLE);
		}
	}
}
