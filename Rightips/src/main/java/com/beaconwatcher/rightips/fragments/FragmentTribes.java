package com.beaconwatcher.rightips.fragments;

import java.util.ArrayList;
import java.util.List;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beaconwatcher.rightips.events.TribesSavedEvent;
import com.beaconwatcher.rightips.AnimatorUtils;
import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.CategoryItem;
import com.beaconwatcher.rightips.services.GetAllTribesService;
import com.ogaclejapan.arclayout.ArcLayout;

import de.greenrobot.event.EventBus;

public class FragmentTribes extends AbstractFragment {
	private static final String TAG = "FragmentTribes";
	private static final String ERROR_TAG = "User Tribes";

	RighTipsApplication mApp;
	Context mContext;
	Intent serviceIntent;

	// Views
	View mRootView;
	TextView mHeading;
	// RelativeLayout mLoader;
	LayoutInflater mInflater;
	View rootLayout;
	View mBtnSave;

	ArcLayout arcLayout;
	LinearLayout mHLayout1;
	LinearLayout mHLayout2;

	ArrayList<CategoryItem> allCategories;
	TypedArray categoryIcons = null;

	private int mOrientation = 1;
	private boolean mEditable = true;

	String mSelCats = "";

	public void setEditable(boolean b) {
		mEditable = b;
		if (mEditable == true) {
			mBtnSave.setVisibility(View.VISIBLE);
			mHeading.setVisibility(View.VISIBLE);
			mHeading.setText(mContext.getResources().getString(
					R.string.choose_pref));
		} else {
			mBtnSave.setVisibility(View.GONE);
			mHeading.setVisibility(View.GONE);
		}
	}

	/**
	 * Parse attributes during inflation from a view hierarchy into the
	 * arguments we handle.
	 */
	@Override
	public void onInflate(Activity activity, AttributeSet attrs,
			Bundle savedInstanceState) {
		super.onInflate(activity, attrs, savedInstanceState);
		TypedArray arr = activity.obtainStyledAttributes(attrs,
				R.styleable.fragment_tribes);
		if (arr != null) {
			int o = arr.getInt(R.styleable.fragment_tribes_orientation, -1);
			if (o != -1) {
				mOrientation = o;
			}

			boolean e = arr.getBoolean(R.styleable.fragment_tribes_editable,
					true);
			mEditable = e;
			arr.recycle();
		}
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		mApp = (RighTipsApplication) getActivity()
				.getApplicationContext();
		mContext = getActivity();
		mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		categoryIcons = getResources().obtainTypedArray(R.array.tribes);

		Bundle b = getArguments();
		if (b != null) {
			mSelCats = b.getString(AppConstants.SELECTED_TRIBES_IDS);
		} else {
			mSelCats = mApp
					.getPreferences(AppConstants.SELECTED_CATEGORIES, "");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragement_tribes, container,
				false);

		rootLayout = mRootView.findViewById(R.id.rootLayout);
		mHeading = (TextView) mRootView.findViewById(R.id.settings_heading);
		mHeading.setVisibility(View.INVISIBLE);

		arcLayout = (ArcLayout) mRootView.findViewById(R.id.arc_layout);
		mHLayout1 = (LinearLayout) mRootView.findViewById(R.id.h_layout1);
		mHLayout2 = (LinearLayout) mRootView.findViewById(R.id.h_layout2);

		mBtnSave = mRootView.findViewById(R.id.btn_save);
		mLoader = (RelativeLayout) mRootView.findViewById(R.id.loader);

		mBtnSave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				saveSettings();
			}
		});
		return mRootView;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		getActivity().registerReceiver(serviceResponseReceiver,
				new IntentFilter(AppConstants.ALL_TRIBES_SERVICE_COMPLETED));
		getAllCategories();
		setEditable(mEditable);
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		getActivity().unregisterReceiver(serviceResponseReceiver);
	}

	public void getAllCategories() {
		if (mLoader != null) {
			mLoader.setVisibility(View.VISIBLE);
		}
		serviceIntent = new Intent(mContext, GetAllTribesService.class);
		serviceIntent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.ACTION_GET_ALL_TRIBES);
		serviceIntent.putExtra(AppConstants.SELECTED_TRIBES_IDS, mSelCats);
		mContext.startService(serviceIntent);
	}

	BroadcastReceiver serviceResponseReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			showLoader(false);
			String msg = intent
					.getStringExtra(AppConstants.INTENT_SERVICE_MESSAGE);
			if (msg != null) {
				mApp.showToast(mContext, msg, Toast.LENGTH_SHORT);
				return;
			}

			String errorMsg = intent
					.getStringExtra(AppConstants.INTENT_SERVICE_ERROR_MESSAGE);
			if (errorMsg != null) {
				mApp.showErrorToast(mContext, ERROR_TAG);
				return;
			}

			String responseType = intent
					.getStringExtra(AppConstants.INTENT_SERVICE_RESPONSE_TYPE);
			if (responseType.equals(AppConstants.RESPONSE_ALL_TRIBES_LOADED)) {
				allCategories = intent
						.getParcelableArrayListExtra(AppConstants.INTENT_SERVICE_DATA);
				if (allCategories != null) {
					addCategoriesIntoLayout(allCategories);
				}
			}
		}
	};

	/*
	 * On Complete //All Categories fetched from server. if(jo.has("data")){
	 * JSONArray arr=jo.getJSONArray("data");
	 * mHeading.setText(mContext.getResources
	 * ().getString(R.string.choose_pref)); onCategoriesFound(arr); }
	 * 
	 * //User Preferences Saved on Server else{ mApp.showToast(mContext,
	 * "Your Preferences saved", Toast.LENGTH_SHORT); //Tell parent activity to
	 * kill this fragment mFragmentInterface.onFragmentSuicide(getTag()); }
	 */

	public void setSelectedItems(String cats) {
		mSelCats = cats;
		getAllCategories();
	}

	public void setBackground(int c) {
		mRootView.setBackgroundColor(c);
	}

	private void addCategoriesIntoLayout(ArrayList<CategoryItem> cats) {
		arcLayout.removeAllViews();
		mHLayout1.removeAllViews();
		mHLayout2.removeAllViews();

		View cat;
		ImageView img;
		Drawable d;
		TextView txt;

		// fill in any details dynamically here
		for (int i = 0; i < cats.size(); i++) {
			if (i >= categoryIcons.length()) {
				return;
			}

			cat = mInflater.inflate(R.layout.categoryitem, null, false);

			cat.setX(mBtnSave.getX());
			cat.setY(mBtnSave.getY());

			img = (ImageView) cat.findViewById(R.id.img);
			try {
				// d = getResources().getDrawable(categoryIcons.get(i));
				img.setImageDrawable(categoryIcons.getDrawable(i));
			}

			catch (IllegalStateException e) {
				Log.e(TAG, e.getMessage());
			}

			txt = (TextView) cat.findViewById(R.id.txt);
			txt.setText(cats.get(i).getName());

			cat.setId(i);

			if (cats.get(i).getSelected() == 1) {
				cat.setSelected(true);
				checkSelected(cat, i);
			}

			if (mEditable == true) {
				cat.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						v.setSelected(!v.isSelected());
						int id = v.getId();

						checkSelected(v, id);
					}
				});
			}

			if (mOrientation == 1) {
				arcLayout.setVisibility(View.VISIBLE);
				mHLayout1.setVisibility(View.INVISIBLE);
				mHLayout2.setVisibility(View.INVISIBLE);
				arcLayout.addView(cat);
				if (i == cats.size() - 1) {
					initialize(arcLayout);
				}
			}

			else if (mOrientation == 3) {
				txt.setVisibility(View.GONE);
				arcLayout.setVisibility(View.INVISIBLE);
				mHLayout1.setVisibility(View.VISIBLE);
				mHLayout2.setVisibility(View.VISIBLE);

				if (i < 4) {
					mHLayout1.addView(cat);
				} else {
					mHLayout2.addView(cat);
				}
				mBtnSave.setVisibility(View.GONE);
				mHeading.setVisibility(View.GONE);
			}
		}
	}

	private void checkSelected(View v, int id) {
		View over = v.findViewById(R.id.item_over);
		if (v.isSelected()) {
			allCategories.get(id).setSelected(1);
			over.setVisibility(View.VISIBLE);
		} else {
			allCategories.get(id).setSelected(0);
			over.setVisibility(View.INVISIBLE);
		}
	}

	public void onNoCategories() {
		// Toast.makeText(mContext, "No categories found",
		// Toast.LENGTH_SHORT).show();
		mApp.showErrorToast(mContext, "Preferences API");
	}

	private void saveSettings() {
		String str = "";
		for (int i = 0; i < allCategories.size(); i++) {
			CategoryItem cat = allCategories.get(i);
			if (cat.getSelected() == 1) {
				str += cat.getId() + ",";
			}
		}
		// Save selected categories into device memory.
		if (!str.equals("")) {
			str = str.substring(0, str.length() - 1);
		}
		mApp.savePreferences(AppConstants.SELECTED_CATEGORIES, str);

		if (mApp.isLoggedIn() == true) {
			String uid = mApp.getUID();
			if (uid != null) {
				savePreferencesOnServer(uid, str);
			}
		}

		else {
			Toast.makeText(
					mContext,
					"Thank you for downloading RighTips. You will start receiving your Tips.",
					Toast.LENGTH_LONG).show();
			if (mFragmentInterface != null)
				mFragmentInterface.onFragmentSuicide(getTag());
		}

		EventBus.getDefault().post(new TribesSavedEvent(str));

	}

	private void savePreferencesOnServer(String uid, String ids) {
		if (mLoader != null) {
			mLoader.setVisibility(View.VISIBLE);
		}
		serviceIntent = new Intent(mContext, GetAllTribesService.class);
		serviceIntent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.ACTION_SAVE_USER_TRIBES);
		serviceIntent.putExtra("uid", uid);
		serviceIntent.putExtra("number", "");
		serviceIntent.putExtra("deviceID", mApp.getUDID());
		serviceIntent.putExtra(AppConstants.SELECTED_TRIBES_IDS, ids);
		mContext.startService(serviceIntent);

		// mApp.showDialog(mContext,
		// mContext.getResources().getString(R.string.api_call),
		// mContext.getResources().getString(R.string.loading_categories)+"...");
		/*
		 * if(mLoader!=null){mLoader.setVisibility(View.VISIBLE);} RequestParams
		 * params = new RequestParams(); params.put("uid", uid);
		 * params.put("number", ""); params.put("deviceID", mApp.getUDID());
		 * params.put("categories", ids);
		 */

		// mApp.APICall("http://www.rightips.com/api/index.php?action=savePreferences",
		// params, responseHandler());
	}

	private void initialize(View v) {
		int x = (v.getLeft() + v.getRight()) / 2;
		int y = (v.getTop() + v.getBottom()) / 2;
		float radiusOfFab = 1f * v.getWidth() / 2f;
		float radiusFromFabToRoot = (float) Math.hypot(
				Math.max(x, rootLayout.getWidth() - x),
				Math.max(y, rootLayout.getHeight() - y));

		showCategories(x, y, radiusOfFab, radiusFromFabToRoot);
	}

	private void showCategories(int cx, int cy, float startRadius,
			float endRadius) {
		List<Animator> animList = new ArrayList<Animator>();
		animList.add(createShowItemAnimator(mBtnSave));

		for (int i = 0, len = arcLayout.getChildCount(); i < len; i++) {
			animList.add(createShowItemAnimator(arcLayout.getChildAt(i)));
		}

		AnimatorSet animSet = new AnimatorSet();
		animSet.playSequentially(animList);
		animSet.start();
	}

	private Animator createShowItemAnimator(View item) {
		float dx = mBtnSave.getX() - item.getX();
		float dy = mBtnSave.getY() - item.getY();

		item.setScaleX(0f);
		item.setScaleY(0f);
		item.setTranslationX(dx);
		item.setTranslationY(dy);

		Animator anim = ObjectAnimator.ofPropertyValuesHolder(item,
				AnimatorUtils.scaleX(0f, 1f), AnimatorUtils.scaleY(0f, 1f),
				AnimatorUtils.translationX(dx, 0f),
				AnimatorUtils.translationY(dy, 0f));

		anim.setInterpolator(new DecelerateInterpolator());
		anim.setDuration(100);
		return anim;
	}

}
