package com.beaconwatcher.rightips.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beaconwatcher.rightips.R;
import com.bw.libraryproject.app.ApplicationController;

public class FriendsFragment extends NotificationReceiverFragment {
	private static final String TAG = "RighTips Activity Fragment";
	private ApplicationController mApp;
	private Context mContext;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragement_friends, container,
				false);
		return rootView;
	}

}
