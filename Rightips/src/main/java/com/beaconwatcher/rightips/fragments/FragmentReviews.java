package com.beaconwatcher.rightips.fragments;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.customadapters.ListItemAdapter;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.beaconwatcher.rightips.events.AddNewReviewScreenEvent;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.beaconwatcher.rightips.events.EventBusEvent;
import com.beaconwatcher.rightips.events.ReviewSavedEvent;
import com.beaconwatcher.rightips.services.IntentServiceFactory;
import com.bw.libraryproject.events.intentservice.NoRecordFoundEvent;

import de.greenrobot.event.EventBus;

public class FragmentReviews extends AbstractFragment {
	// Views
	private View mRootView;
	private ListView mReviewsList;
	private View mRatingBarCont;
	private RatingBar mRatingBar;

	// varibales
	private String mSiteID;
	private String mUserID;
	private String mode;
	private int mLoggedUserID = 0;
	private boolean loggedUserHasReviewed = false;

	private ListItemAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		TAG = "FragmentReviews";
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		/*
		 * mContext = getActivity(); mApp=(PiwikApplicationController)
		 * getActivity().getApplicationContext();
		 */
		String uid = mApp.getUID();
		if (uid != null) {
			mLoggedUserID = Integer.parseInt(uid);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mRootView = inflater.inflate(R.layout.fragment_reviews, container,
				false);
		mLoader = mRootView.findViewById(R.id.loader);
		mRatingBar = (RatingBar) mRootView.findViewById(R.id.rating_bar);
		mReviewsList = (ListView) mRootView.findViewById(R.id.list_reviews);

		((TextView) mRootView.findViewById(R.id.txt_rate_this))
				.setText(getResources().getString(R.string.lbl_rate_this_place));
		mRatingBarCont = mRootView.findViewById(R.id.rating_bar_cont);

		mRatingBar
				.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
					@Override
					public void onRatingChanged(RatingBar ratingBar,
							float rating, boolean fromUser) {
						if (fromUser)
							EventBus.getDefault().post(
									new AddNewReviewScreenEvent(rating));
					}
				});
		checkReviews();
		return mRootView;
	}

	private void checkReviews() {
		Bundle b = getArguments();
		if (!(b == null) && !b.isEmpty()) {
			mode = b.getString(AppConstants.REVIEWS_MODE);
			if (mode.equals(AppConstants.REVIEWS_MODE_SITE)) {
				mSiteID = b.getString(AppConstants.SITE_ID);
			} else if (mode.equals(AppConstants.REVIEWS_MODE_USER)) {
				mRatingBarCont.setVisibility(View.INVISIBLE);
				mUserID = b.getString(AppConstants.USER_ID);
			}
			loadReviews();
		}
	}

	public void loadUserReviews(String mUserID) {
		this.mUserID = mUserID;
		mode = AppConstants.REVIEWS_MODE_USER;
		mRatingBarCont.setVisibility(View.INVISIBLE);
		loadReviews();
	}

	public void resetRatingBar() {
		mRatingBar.setRating(-1);
	}

	public void loadReviews() {
		if (mSiteID == null && mUserID == null)
			return;
		showLoader(true);
		Class clazz;
		Intent intent;
		if (mode.equals(AppConstants.REVIEWS_MODE_SITE) && mSiteID != null) {
			clazz = IntentServiceFactory.getInstance().getIntentServiceClass(
					AppConstants.ACTION_GET_SITE_REVIEWS);
			intent = new Intent(mContext, clazz);
			intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
					AppConstants.ACTION_GET_SITE_REVIEWS);
			intent.putExtra(AppConstants.SITE_ID, mSiteID);
			intent.putExtra(AppConstants.INTENT_SERVICE_TAG, TAG);
			intent.putExtra(AppConstants.USER_ID, mApp.getUID());
		} else {
			clazz = IntentServiceFactory.getInstance().getIntentServiceClass(
					AppConstants.ACTION_GET_USER_REVIEWS);
			intent = new Intent(mContext, clazz);
			intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
					AppConstants.ACTION_GET_USER_REVIEWS);
			intent.putExtra(AppConstants.USER_ID, mUserID);
		}

		mContext.startService(intent);
	}

	public void onEvent(ReviewSavedEvent event) {
		resetRatingBar();
		loadReviews();
	}

	public void onEvent(ApplicationEvent event) {
		if (event.getType().equals(AppConstants.LIST_ITEM_REMOVED)) {
			checkIfUserReviewed();
		}
	}

	public void onEvent(EventBusEvent event) {
		showLoader(false);
		if (event.getType().equals(AppConstants.ACTION_GET_SITE_REVIEWS)
				|| event.getType().equals(AppConstants.ACTION_GET_USER_REVIEWS)) {
			ArrayList<ListItemData> arr = (ArrayList<ListItemData>) event
					.getData();
			if (arr != null) {
				mAdapter = new ListItemAdapter(
						AppConstants.LIST_ITEM_TYPE_REVIEW, arr);
				mReviewsList.setAdapter(mAdapter);
				checkIfUserReviewed();
			}
		}
	}
	
	
	public void onEvent(NoRecordFoundEvent evt) {
		showLoader(false);
		if(evt.tag.equals(TAG)){
			mApp.showDialogMessage(mContext, mContext.getResources().getString(R.string.no_record_reviews));
		}
	}

	

	private void checkIfUserReviewed() {
		mRatingBarCont.setVisibility(View.VISIBLE);
		for (int i = 0; i < mAdapter.getCount(); i++) {
			if (mLoggedUserID != 0
					&& ((ListItemData) mAdapter.getItem(i)).uid == mLoggedUserID) {
				mRatingBarCont.setVisibility(View.INVISIBLE);
			}
		}
	}
}
