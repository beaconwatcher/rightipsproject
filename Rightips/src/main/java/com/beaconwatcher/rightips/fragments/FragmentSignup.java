package com.beaconwatcher.rightips.fragments;

import java.util.ArrayList;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.activities.LoginActivity;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.EmailVerificationData;
import com.beaconwatcher.rightips.entities.UserProfileData;
import com.beaconwatcher.rightips.entities.piwik.PiwikConstants;
import com.beaconwatcher.rightips.events.EventBusEvent;
import com.beaconwatcher.rightips.services.IntentServiceFactory;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class FragmentSignup extends AbstractFragment {
	private RighTipsApplication mApp;
	private Context mContext;

	// UI Elements
	private LinearLayout mBtnSignup;

	private EditText tfFirstName;
	private EditText tfLastName;
	private EditText tfEmail;
	private EditText tfPassword;

	private String firstName = "";
	private String lastName = "";
	private String email = "";
	private String password = "";

	AsyncHttpResponseHandler handler;

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		TAG = "SignupFragment";
		mApp = (RighTipsApplication) getActivity()
				.getApplicationContext();
		mContext = getActivity();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragement_signup, container,
				false);

		tfFirstName = (EditText) rootView.findViewById(R.id.tfFirstName);
		tfLastName = (EditText) rootView.findViewById(R.id.tfLastName);
		tfEmail = (EditText) rootView.findViewById(R.id.tfEmail);
		tfPassword = (EditText) rootView.findViewById(R.id.tfPassword);

		rootView.findViewById(R.id.btnSignUp).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						signUp();
					}
				});

		rootView.findViewById(R.id.btnSignIn).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						FragmentTransaction ft = getFragmentManager()
								.beginTransaction();
						ft.replace(android.R.id.content, new FragmentLogin());
						ft.commit();
					}
				});
		return rootView;
	}

	
	protected void signUp() {
		firstName = tfFirstName.getText().toString();
		lastName = tfLastName.getText().toString();
		email = tfEmail.getText().toString();
		password = tfPassword.getText().toString();

		if (firstName.equals("") || lastName.equals("") || email.equals("")
				|| password.equals("")) {
			mApp.showDialogMessage(mContext, mContext.getResources().getString(
							R.string.lbl_fill_all_fields));
		}
		else{
			verifyEmail();
		}
	}
	
	protected void verifyEmail(){
		mApp.showDialog(mContext,
				mContext.getResources().getString(R.string.api_call), mContext
						.getResources().getString(R.string.lbl_verifying_email));

		Class mClazz = IntentServiceFactory.getInstance()
				.getIntentServiceClass(AppConstants.ACTION_VERIFY_EMAIL);
		Intent intent = new Intent(mContext, mClazz);
		intent.putExtra(AppConstants.INTENT_SERVICE_TAG, TAG);
		intent.putExtra(AppConstants.USER_EMAIL, email);
		intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.ACTION_VERIFY_EMAIL);
		mContext.startService(intent);
	}
	
	

	public void onEvent(EventBusEvent event) {
		mApp.hideDialog();
		if(event.getType().equals(AppConstants.ACTION_VERIFY_EMAIL)){
			EmailVerificationData d=(EmailVerificationData) event.getData();
			if(d.status.equals("valid")){
				callSignUpAPI();
			}
			else{
				mApp.showDialogMessage(mContext, mContext.getResources().getString(R.string.lbl_invalid_email));
			}
		}
		
		
		if (event.getType().equals(AppConstants.ACTION_LOGIN)) {
			ArrayList<UserProfileData> arr = (ArrayList<UserProfileData>) event
					.getData();
			UserProfileData mProfile = arr.get(0);
			if (mProfile != null) {
				loggedIn(mProfile);
			}
		}
	}

	protected void loggedIn(UserProfileData profile) {
		mApp.getTracker().setUserId(profile.uid);
		mApp.getTracker().trackEvent(PiwikConstants.CATEGORY_USERS,
				PiwikConstants.ACTION_SIGNUP, Integer.toString(profile.uid));

		((LoginActivity) getActivity()).loggedIn(profile);
		getActivity().getFragmentManager().beginTransaction().remove(this)
				.commit();
	}
	
	
	protected void callSignUpAPI(){
		// Provider is as follows (0=Rightips, 1=Facebook, 2=Twitter, 3=Google+)
		mApp.showDialog(mContext,
				mContext.getResources().getString(R.string.api_call), mContext
						.getResources().getString(R.string.lbl_signing_up));

		Class mClazz = IntentServiceFactory.getInstance()
				.getIntentServiceClass(AppConstants.ACTION_LOGIN);
		Intent intent = new Intent(mContext, mClazz);
		intent.putExtra(AppConstants.INTENT_SERVICE_TAG, TAG);
		intent.putExtra(AppConstants.USER_FIRST_NAME, firstName);
		intent.putExtra(AppConstants.USER_LAST_NAME, lastName);
		intent.putExtra(AppConstants.USER_EMAIL, email);
		intent.putExtra(AppConstants.USER_PASSWORD, password);
		intent.putExtra(AppConstants.LOGIN_PROVIDER, "0");
		intent.putExtra(AppConstants.IS_SIGNUP, "1");
		intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.ACTION_LOGIN);
		mContext.startService(intent);

	}
}
