package com.beaconwatcher.rightips.fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ContactListItem;
import com.beaconwatcher.rightips.entities.MyFriendData;
import com.beaconwatcher.rightips.events.EventBusEvent;
import com.beaconwatcher.rightips.services.IntentServiceFactory;
import com.bw.libraryproject.customadapers.FilterableSortableAdapter;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class FragmentMyFriends extends AbstractFragment {
	private static String TAG = "com.beaconwatcher.rightips.MyFriendsFragment";

	private LayoutInflater inflater;
	private TextView mBtnInvite;
	private View rootView;
	private EditText mTfSearch;
	private ListView mFriendsList;

	private MyFriendsAdapter mAdapter;
	private ArrayList<MyFriendData> mFriendsArray;// = new
													// ArrayList<ContactListItem>();

	private RighTipsApplication mApp;
	private Context mContext;
	private AsyncHttpResponseHandler addFriendHandler;
	private AsyncHttpResponseHandler loadFriendHandler;
	private String mUid = "";

	private static int REQUEST_INVITE = 100999;

	@Override
	public View onCreateView(LayoutInflater infl, ViewGroup container,
			Bundle savedInstanceState) {
		TAG = "MyFriendsFragment";
		mContext = getActivity();
		mApp = (RighTipsApplication) mContext.getApplicationContext();
		mAdapter = new MyFriendsAdapter(mFriendsArray, mApp);

		// TODO Auto-generated method stub
		rootView = infl
				.inflate(R.layout.fragement_my_friends, container, false);

		mLoader = rootView.findViewById(R.id.loader_friends);
		inflater = infl;
		mBtnInvite = (TextView) rootView.findViewById(R.id.btn_invite);
		mTfSearch = (EditText) rootView.findViewById(R.id.tf_search);
		mFriendsList = (ListView) rootView.findViewById(R.id.list_friends);

		mFriendsList.setAdapter(mAdapter);

		mTfSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				mAdapter.getFilter().filter(s.toString());
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});

		mBtnInvite.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// invokeGoogleInvite();
				mFragmentInterface.onInviteFriendsClicked();
			}
		});

		getFriends();

		return rootView;
	}

	public void getFriends() {
		// Not Logged In hide Invite Button
		if (mApp.isLoggedIn() == false) {
			return;// mBtnInvite.setVisibility(View.INVISIBLE);
		} else {
			mUid = mApp.getUID();
			String pendingInvitations = mApp.getPreferences(
					AppConstants.PENDING_INVITATIONS, "");
			if (!pendingInvitations.equals("")) {
				makeFriends(pendingInvitations);
			} else {
				loadFriends();
			}
		}
	}

	protected void makeFriends(String fid) {
		mApp.showDialog(mContext,
				mApp.getResources().getString(R.string.api_call),
				"Loading RighTips Friends...");
		RequestParams params = new RequestParams();
		params.put("uid", mUid);
		params.put("fid", fid);
		mApp.APICall("http://www.rightips.com/api/index.php?action=addFriend",
				params, addFriendResponseHandler());
	}

	protected AsyncHttpResponseHandler addFriendResponseHandler() {
		if (addFriendHandler != null) {
			return addFriendHandler;
		}

		AsyncHttpResponseHandler handler = new AsyncHttpResponseHandler() {
			@Override
			public void onStart() {
				Log.v(TAG, "onStart");
			}

			@Override
			public void onSuccess(String response) {
				mApp.hideDialog();
				Log.v(TAG, "onSuccess");
				JSONObject jo = null;

				try {
					jo = new JSONObject(response);
					int status = jo.getInt("status");
					String msg = jo.getString("message");

					if (status == 0) {
						// Toast.makeText(mContext, msg,
						// Toast.LENGTH_SHORT).show();
						mApp.showErrorToast(mContext, "Add Friend API");
					} else {
						// Toast.makeText(mContext,
						// "All Pending Freind Requests Processed",
						// Toast.LENGTH_SHORT).show();
						mApp.showErrorToast(mContext,
								"All friend requests processed");
						mApp.savePreferences(AppConstants.PENDING_INVITATIONS,
								"");
						loadFriends();
					}
				} catch (JSONException e) {
					// Toast.makeText(mContext, "Error in server response",
					// Toast.LENGTH_SHORT).show();
					mApp.showErrorToast(mContext, "My Friends API");
					Log.e("log_tag", "Error parsing data " + e.toString());
				}
			}

			@Override
			public void onFailure(Throwable error, String content) {
				mApp.hideDialog();
				// Toast.makeText(mContext,
				// "Failed to login, please make sure internet connection is working.",
				// Toast.LENGTH_SHORT).show();
				mApp.showErrorToast(mContext, "My Friends API");
				Log.e(TAG, "onFailure error : " + error.toString()
						+ "content : " + content);
			}

			@Override
			public void onFinish() {
				mApp.hideDialog();
				Log.v(TAG, "onFinish");
			}
		};
		return handler;
	}

	protected void loadFriends() {
		showLoader(true);
		Class clazz = IntentServiceFactory.getInstance().getIntentServiceClass(
				AppConstants.ACTION_GET_MY_FRIENDS);
		Intent intent = new Intent(mContext, clazz);
		intent.putExtra(AppConstants.INTENT_SERVICE_TAG, TAG);
		intent.putExtra(AppConstants.USER_ID, mUid);
		intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.ACTION_GET_MY_FRIENDS);
		mContext.startService(intent);
	}

	public void onEvent(EventBusEvent event) {
		if (event.getType().equals(AppConstants.ACTION_GET_MY_FRIENDS)) {
			showLoader(false);
			ArrayList<MyFriendData> arr = (ArrayList<MyFriendData>) event
					.getData();
			mAdapter.clear();
			mAdapter.addAll(arr);
			mAdapter.getFilter().filter(mTfSearch.getText());
			mAdapter.notifyDataSetChanged();
		}
	}

	public class MyFriendsAdapter extends FilterableSortableAdapter {
		ViewHolder viewHolder;

		public MyFriendsAdapter(ArrayList<MyFriendData> arr, Context c) {
			super(c, 0);
		}

		@Override
		public View getView(int i, View reuseableView, ViewGroup viewGroup) {
			if (reuseableView == null) {
				reuseableView = inflater.inflate(R.layout.contact_list_item,
						null);
				reuseableView.setLayoutParams(new AbsListView.LayoutParams(
						AbsListView.LayoutParams.MATCH_PARENT, getResources()
								.getDimensionPixelSize(
										R.dimen.contact_item_height)));

				viewHolder = new ViewHolder();

				// cache the views
				viewHolder.name = (TextView) reuseableView
						.findViewById(R.id.txt_name);
				viewHolder.img = (ImageView) reuseableView
						.findViewById(R.id.profile_pic);
				/*
				 * viewHolder.contact = (TextView) reuseableView
				 * .findViewById(R.id.txt_contact);
				 */
				viewHolder.checkBox = (CheckBox) reuseableView
						.findViewById(R.id.cb_contact);

				// link the cached views to the convertview
				reuseableView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) reuseableView.getTag();
			}
			viewHolder.checkBox.setVisibility(View.GONE);

			MyFriendData item = (MyFriendData) getItem(i);
			viewHolder.name.setText(item.username);

			return reuseableView;
		}

		// class for caching the views in a row
		private class ViewHolder {
			TextView name, contact;
			ImageView img;
			CheckBox checkBox;
		}
	}

}
