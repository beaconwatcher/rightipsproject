package com.beaconwatcher.rightips.fragments;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.customadapters.ListItemAdapter;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.events.EventBusEvent;
import com.beaconwatcher.rightips.services.IntentServiceFactory;

public class FragmentUserLikes extends AbstractFragment {

	// protected static final String TAG="FragmentReviews";
	private LayoutInflater mLayoutInflater;
	// Views
	private View mRootView;
	private ListView mLikesList;

	// varibales
	private String mUserID;
	private ListItemAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		TAG = "FragmentUserLikes";
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mContext = getActivity();
		mApp = (RighTipsApplication) getActivity()
				.getApplicationContext();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mLayoutInflater = inflater;
		mRootView = inflater.inflate(R.layout.fragment_likes, container, false);
		mLoader = mRootView.findViewById(R.id.loader_likes);
		mLikesList = (ListView) mRootView.findViewById(R.id.list_likes);
		return mRootView;
	}

	// Events
	public void loadLikes(String mUserID) {
		this.mUserID = mUserID;
		showLoader(true);
		// Log.d("FragmentReviews", "getReviews id: "+mSiteID);
		Class clazz = IntentServiceFactory.getInstance().getIntentServiceClass(
				AppConstants.ACTION_GET_USER_LIKES);
		Intent intent = new Intent(mContext, clazz);
		intent.putExtra(AppConstants.INTENT_SERVICE_TAG, TAG);
		intent.putExtra(AppConstants.USER_ID, mUserID);
		intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.ACTION_GET_USER_LIKES);
		mContext.startService(intent);
	}

	public void onEvent(EventBusEvent event) {
		if (event.getType().equals(AppConstants.ACTION_GET_USER_LIKES)) {
			showLoader(false);
			ArrayList<Object> arr = (ArrayList<Object>) event.getData();
			mAdapter = new ListItemAdapter(AppConstants.LIST_ITEM_TYPE_LIKES,
					arr);
			mLikesList.setAdapter(mAdapter);
		}
	}
}
