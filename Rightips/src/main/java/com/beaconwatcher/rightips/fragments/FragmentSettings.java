package com.beaconwatcher.rightips.fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.CategoryItem;
import com.bw.libraryproject.customrenderers.CircularImageView;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

public class FragmentSettings extends AbstractFragment {
	private static final String TAG = "RighTips Settings Fragment";

	RighTipsApplication mApp;
	Context mContext;
	AsyncHttpResponseHandler handler;

	TextView mHeading;
	ViewGroup catContainer;
	LinearLayout mBtnSave;
	RelativeLayout mLoader;

	GridView categoriesGrid;
	OrderGridViewAdapter adapter;
	ArrayList<Integer> selItems;

	private ArrayList<Integer> categoryIcons = new ArrayList<Integer>();

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		mApp = (RighTipsApplication) getActivity()
				.getApplicationContext();
		mContext = getActivity();

		categoryIcons.add(R.drawable.category_icon_students);
		categoryIcons.add(R.drawable.category_icon_entertainment);
		categoryIcons.add(R.drawable.category_icon_events);
		categoryIcons.add(R.drawable.category_icon_bar);
		categoryIcons.add(R.drawable.category_icon_shop);
		categoryIcons.add(R.drawable.category_icon_services);
		categoryIcons.add(R.drawable.category_icon_tourism);

		getAllCategories();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragement_settings,
				container, false);
		mHeading = (TextView) rootView.findViewById(R.id.settings_heading);
		mBtnSave = (LinearLayout) rootView.findViewById(R.id.btnSave);
		mLoader = (RelativeLayout) rootView.findViewById(R.id.loader);

		mBtnSave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				saveSettings();
			}
		});

		// View view = inflater.inflate(R.layout.g, null);
		categoriesGrid = (GridView) rootView.findViewById(R.id.myGrid);
		categoriesGrid.setVerticalSpacing(50);
		categoriesGrid.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				adapter.onItemSelect(arg0, arg1, arg2, arg3);
			}
		});

		return rootView;
	}

	public void getAllCategories() {
		// mApp.showDialog(mContext,
		// mContext.getResources().getString(R.string.api_call),
		// mContext.getResources().getString(R.string.loading_categories)+"...");
		if (mLoader != null) {
			mLoader.setVisibility(View.VISIBLE);
		}
		RequestParams params = new RequestParams();
		// params.put("userID",
		// mApp.getPreferences(LoginActivity.PREF_LOGIN_USER_ID, ""));

		mApp.APICall(
				"http://api.beaconwatcher.com/index.php?action=getCategories",
				params, responseHandler());
	}

	protected AsyncHttpResponseHandler responseHandler() {
		if (handler != null) {
			return handler;
		}

		AsyncHttpResponseHandler handler = new AsyncHttpResponseHandler() {
			@Override
			public void onStart() {
				Log.v(TAG, "onStart");
			}

			@Override
			public void onSuccess(String response) {
				mBtnSave.setVisibility(View.VISIBLE);
				if (mLoader != null) {
					mLoader.setVisibility(View.GONE);
				}

				// mApp.hideDialog();
				Log.v(TAG, "onSuccess");
				JSONObject jo = null;

				try {
					jo = new JSONObject(response);
					int status = jo.getInt("status");
					String msg = jo.getString("message");

					if (status == 0) {
						// mApp.showToast(mContext, msg, Toast.LENGTH_SHORT);
						mApp.showErrorToast(mContext, "Preferences API");
						onNoCategories();
					} else {
						JSONArray arr = jo.getJSONArray("data");
						mHeading.setText(mContext.getResources().getString(
								R.string.choose_pref));
						onCategoriesFound(arr);
					}
				} catch (JSONException e) {
					Log.e("log_tag", "Error parsing data " + e.toString());
				}
			}

			@Override
			public void onFailure(Throwable error, String content) {
				if (mLoader != null) {
					mLoader.setVisibility(View.GONE);
				}
				mHeading.setText(mContext.getResources().getString(
						R.string.internet_access));
				Log.e(TAG, "onFailure error : " + error.toString()
						+ "content : " + content);
			}

			@Override
			public void onFinish() {
				if (mLoader != null) {
					mLoader.setVisibility(View.GONE);
				}
				Log.v(TAG, "onFinish");
			}
		};
		return handler;
	}

	public void onCategoriesFound(JSONArray arr) {
		Log.d(TAG, arr.toString());
		String selCats = mApp.getPreferences(AppConstants.SELECTED_CATEGORIES,
				"");
		String[] selArray = selCats.split("\\, ");

		ArrayList<CategoryItem> categories = new ArrayList<CategoryItem>();

		for (int i = 0; i < arr.length(); i++) {
			try {
				JSONObject jo = arr.getJSONObject(i);

				CategoryItem item = new CategoryItem();
				item.setId(jo.getString("cat_id"));
				item.setName(jo.getString("cat_name"));
				// item.setUrl(jo.getString("cat_image"));
				item.setUrl(jo.getString("cat_icon"));

				if (hasArrayItem(item.getId(), selArray)) {
					item.setSelected(1);
				}

				categories.add(item);
			} catch (JSONException e) {
				Log.e("log_tag", "Error parsing data " + e.toString());
			}
		}

		adapter = new OrderGridViewAdapter(categories);
		categoriesGrid.setAdapter(adapter);

	}

	private Boolean hasArrayItem(String item, String[] arr) {
		for (String cat : arr) {
			if (cat.equals(item)) {
				return true;
			}
		}
		return false;
	}

	public void onNoCategories() {
		// Toast.makeText(mContext, "No categories found",
		// Toast.LENGTH_SHORT).show();
		mApp.showErrorToast(mContext, "Preferences API");
	}

	private void saveSettings() {
		// Save selected categories into device memory.
		String str = adapter.getSelectedItems().toString();
		str = str.substring(1, str.length() - 1);
		mApp.savePreferences(AppConstants.SELECTED_CATEGORIES, str);

		Toast.makeText(
				mContext,
				"Thank you for downloading RighTips. You will start receiving your Tips.",
				Toast.LENGTH_LONG).show();

		// Tell parent activity to kill this fragment
		mFragmentInterface.onFragmentSuicide(getTag());
	}

	public class OrderGridViewAdapter extends BaseAdapter {
		private LayoutInflater mInflater;
		private ArrayList<CategoryItem> mItems;

		public OrderGridViewAdapter(ArrayList<CategoryItem> cats) {
			mItems = cats;
			mInflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return mItems.size();
		}

		@Override
		public Object getItem(int position) {
			return mItems.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return Long.parseLong(mItems.get(position).getId());
		}

		public ArrayList<String> getSelectedItems() {
			ArrayList<String> selItems = new ArrayList<String>();
			for (Integer i = 0; i < mItems.size(); i++) {
				if (mItems.get(i).getSelected() == 1) {
					selItems.add(mItems.get(i).getId());
				}
			}
			return selItems;
		}

		@Override
		public View getView(int position, View covertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (covertView == null) {
				covertView = mInflater.inflate(R.layout.categoryitem, null);
				holder = new ViewHolder();
				holder.item = covertView.findViewById(R.id.cat_item);
				// holder.txt = (TextView) covertView.findViewById(R.id.txt);
				covertView.setTag(holder);
			} else {
				holder = (ViewHolder) covertView.getTag();
			}

			CircularImageView iv = (CircularImageView) holder.item
					.findViewById(R.id.img);
			if (!mItems.get(position).getUrl().equals("")) {
				// Picasso.with(mContext).load(mItems.get(position).getUrl()).placeholder(R.drawable.cat_no_image).error(R.drawable.cat_no_image).into(iv);
				Picasso.with(mContext).load(categoryIcons.get(position))
						.placeholder(R.drawable.cat_no_image)
						.error(R.drawable.cat_no_image).into(iv);
			} else {
				iv.setImageResource(R.drawable.cat_no_image);
			}
			iv.setBorderColor(Color.WHITE);
			iv.setBorderWidth(3);
			holder.txt.setText(mItems.get(position).getName());

			if (mItems.get(position).getSelected() != 1) {
				iv.setBorderColor(Color.WHITE);
				iv.setBorderWidth(3);
				holder.item.findViewById(R.id.item_over).setVisibility(
						View.GONE);
			} else {
				iv.setBorderColor(Color.GREEN);
				iv.setBorderWidth(3);
				holder.item.findViewById(R.id.item_over).setVisibility(
						View.VISIBLE);
			}
			return covertView;
		}

		public void onItemSelect(AdapterView<?> parent, View v, int pos, long id) {
			CircularImageView iv = (CircularImageView) v.findViewById(R.id.img);
			FrameLayout f = (FrameLayout) v.findViewById(R.id.item_over);

			if (mItems.get(pos).getSelected() == 1) {
				mItems.get(pos).setSelected(0);
				iv.setBorderColor(Color.WHITE);
				iv.setBorderWidth(3);
				f.setVisibility(View.GONE);
			} else {
				mItems.get(pos).setSelected(1);
				iv.setBorderColor(Color.GREEN);
				iv.setBorderWidth(3);
				f.setVisibility(View.VISIBLE);
			}
		}
	}

	public static class ViewHolder {
		public View item;
		public TextView txt;

	}
}
