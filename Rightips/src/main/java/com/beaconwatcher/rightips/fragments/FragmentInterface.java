package com.beaconwatcher.rightips.fragments;

import java.util.ArrayList;

import com.bw.libraryproject.entities.LocationData;
import com.bw.libraryproject.entities.NotificationData;

public interface FragmentInterface {
	public void onHistoryItemClicked(NotificationData nd);

	public void onFragmentSuicide(String tag);

	public void onInviteFriendsClicked();

	public void onMapIconClicked(NotificationData nd);

	public void onRegionIconClicked(LocationData ld);

	public void onSliderItemClicked(ArrayList<NotificationData> arr);

}
