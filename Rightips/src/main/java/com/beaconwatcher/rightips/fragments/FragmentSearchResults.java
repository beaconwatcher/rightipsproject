package com.beaconwatcher.rightips.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.customadapters.ListItemAdapter;
import com.beaconwatcher.rightips.customadapters.ListItemAdapter.ListItemNotifier;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.ListItemData;
import com.beaconwatcher.rightips.entities.SearchItemData;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.google.gson.reflect.TypeToken;

import de.greenrobot.event.EventBus;

public class FragmentSearchResults extends AbstractFragment {

	// protected static final String TAG="FragmentReviews";
	private LayoutInflater mLayoutInflater;
	// Views
	private View mRootView;
	private ListView mListView;
	private TextView mTotalResults;

	// varibales

	private ArrayList<SearchItemData> mSearchArray;

	private ListItemAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		TAG = "FragmentSearchResults";
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mContext = getActivity();
		mApp = (RighTipsApplication) getActivity()
				.getApplicationContext();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mLayoutInflater = inflater;
		mRootView = inflater.inflate(R.layout.fragment_search_results,
				container, false);
		mLoader = mRootView.findViewById(R.id.loader);
		mListView = (ListView) mRootView.findViewById(R.id.list_view);
		mTotalResults = (TextView) mRootView
				.findViewById(R.id.txt_total_results);
		checkResults(getArguments());
		return mRootView;
	}

	private void checkResults(Bundle b) {
		if (b != null) {
			String str = b.getString(AppConstants.SEARCH_RESULTS);
			ArrayList<SearchItemData> arr = (ArrayList<SearchItemData>) mApp
					.getClassObjectFromJsonString(str,
							new TypeToken<ArrayList<SearchItemData>>() {
							}.getType());
			if (arr != null && arr.size() > 0) {
				updateResults(arr);
			}
		}

	}

	public void updateResults(ArrayList<SearchItemData> arr) {
		// Convert SearchItemData into ListItemData
		if (arr == null || arr.size() == 0)
			return;
		mTotalResults.setText(arr.size() + " "+mContext.getResources().getString(R.string.lbl_results_found));

		ArrayList<ListItemData> arr2 = new ArrayList<ListItemData>();
		for (int i = 0; i < arr.size(); i++) {
			SearchItemData item = arr.get(i);
			ListItemData nItem = new ListItemData();
			nItem.id = item.nt_id;
			nItem.title = item.site_name;
			nItem.site_title = item.site_address;
			if(item.images.size() > 0){
				nItem.img = item.images.get(0).url;
			}
			nItem.distance = item.distance;

			if(item.tags.size() > 0){
				nItem.tags = new String[item.tags.size()];
				for (int t = 0; t < item.tags.size(); t++) {
					nItem.tags[t] = item.tags.get(t).name;
				}
			}
			arr2.add(nItem);
		}

		mAdapter = new ListItemAdapter(AppConstants.LIST_ITEM_SEARCH_RESULTS,
				arr2, mListView, notifier);
		mListView.setAdapter(mAdapter);
	}

	ListItemNotifier notifier = new ListItemNotifier() {
		@Override
		public void onItemClicked(Object item) {
			// TODO Auto-generated method stub
			String id = Integer.toString(((ListItemData) item).id);
			EventBus.getDefault().post(
					new ApplicationEvent(
							AppConstants.SEARCH_RESULT_ITEM_CLICKED, id, null));
		}
	};

}
