package com.beaconwatcher.rightips.fragments;

import java.util.ArrayList;

import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.activities.GoogleMapActivity;
import com.beaconwatcher.rightips.customslides.NotificationMiniSliderView;
import com.beaconwatcher.rightips.customslides.NotificationSliderView;
import com.beaconwatcher.rightips.customslides.SeeAllMiniSliderView;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.beaconwatcher.rightips.services.GetTimeZoneByLocationService;
import com.beaconwatcher.rightips.time.JodaDigitalClock;
import com.bw.libraryproject.entities.CategoryItem;
import com.bw.libraryproject.entities.LocationData;
import com.bw.libraryproject.entities.NotificationData;
import com.bw.libraryproject.services.LocationBeaconsService;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.BaseSliderView.OnSliderClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.reflect.TypeToken;

import de.greenrobot.event.EventBus;

public class FragmentHome extends NotificationReceiverFragment implements
		BaseSliderView.OnSliderClickListener {

	private static final String TAG = "RighTipsHomeFragment";

	public static String EXTRAS_ACTIVITY_DATA = "extraActivityData";
	public static String EXTRAS_ACTIVITY_RESULT = "extraActivityResult";

	private RighTipsApplication mApp;
	private Context mContext;

	private FragmentManager fm;

	private String appUUID = "5144A35F387A7AC0B254DFB1381C9DFF";

	private View rootView;
	private SliderLayout mSlider;
	private View mSliderContainer;
	private SliderLayout mSliderSearch;
	private View mSliderSearchContainer;
	
	
	private LinearLayout mTimeContainer;
	private RelativeLayout mLoader;
	private RelativeLayout mMsgNoSlides;
	private TextView mTxtNoSlides;

	private LayoutInflater mInflater;
	private ViewGroup mContainer;
	private View mMainContainer;
	private JodaDigitalClock mDigitalClock;

	private LocationData mLocationData;
	private String cityName;

	private boolean slidesAvailable = false;
	private ArrayList<NotificationData> mAllNotifications;


	private boolean hasLocationNotifications = false;
	private boolean hasSearchNotifications = false;

	
	//Fragments
	private FragmentWeather mFragmentWeather;
	private View mWeatherContainer;
	


	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		LocalBroadcastManager.getInstance(mContext).unregisterReceiver(
				timeZoneBroadCastReceiver);
		LocalBroadcastManager.getInstance(mContext).unregisterReceiver(
				locationNotificationReceiver);
		super.onPause();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		checkLastSliderData();
		LocalBroadcastManager.getInstance(mContext).registerReceiver(
				timeZoneBroadCastReceiver, timeZoneIntentFilter);
		LocalBroadcastManager.getInstance(mContext).registerReceiver(
				locationNotificationReceiver, locationNotificationIntentFilter);
		
		checkLastLocation();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mApp = (RighTipsApplication) getActivity().getApplicationContext();
		mContext = getActivity();
		mApp.getTracker().trackScreenView("/", "Home screen");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mInflater = inflater;
		rootView = inflater.inflate(R.layout.fragement_home, container, false);
		mContainer = container;
		mMainContainer = rootView.findViewById(R.id.container);

	//	mMsgNoSlides = (RelativeLayout) rootView.findViewById(R.id.msg_no_slides);
		//mTxtNoSlides = (TextView) rootView.findViewById(R.id.txt_no_slides);

		mSlider = (SliderLayout) rootView.findViewById(R.id.slider);
		mSliderContainer = rootView.findViewById(R.id.slider_container);
		mSlider.setPresetTransformer(SliderLayout.Transformer.Default);
		mSlider.setCustomIndicator((PagerIndicator) rootView
				.findViewById(R.id.custom_indicator_big_slider));
		mSlider.startAutoCycle(10000,  5000, true);
		
		
		mSliderSearch = (SliderLayout) rootView.findViewById(R.id.slider_search);
		mSliderSearchContainer = rootView.findViewById(R.id.slider_search_container);
		mSliderSearch.setPresetTransformer(SliderLayout.Transformer.Default);
		mSliderSearch.setCustomIndicator((PagerIndicator) rootView
				.findViewById(R.id.custom_indicator_slider_search));
		mSliderSearch.setDuration(4000);
		addSearchSlides();
		
		

		mDigitalClock = (JodaDigitalClock) rootView
				.findViewById(R.id.digitalClock);
		mTimeContainer = (LinearLayout) rootView
				.findViewById(R.id.time_container);
		mLoader = (RelativeLayout) rootView.findViewById(R.id.loader);
		mWeatherContainer = rootView.findViewById(R.id.weather_container);
		addWeatherFragment();
		
		

		// Add ClickListeners
		rootView.findViewById(R.id.btnMaps).setOnClickListener(mClickListener);
		rootView.findViewById(R.id.btnRegion).setOnClickListener(mClickListener);
		rootView.findViewById(R.id.btnThings).setOnClickListener(mClickListener);
		rootView.findViewById(R.id.btnFoods).setOnClickListener(mClickListener);
		rootView.findViewById(R.id.btnShopping).setOnClickListener(mClickListener);
		rootView.findViewById(R.id.btnEvents).setOnClickListener(mClickListener);
		return rootView;
	}
	
	//Very Import to implement this function for inner fragments
	//Otherwise app will crash on resume.
	/*@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		Fragment f=this.getChildFragmentManager().findFragmentByTag(FragmentWeather.class.getName());
		if(f!=null){
			getChildFragmentManager().beginTransaction().remove(f).commit();
		}
		super.onDestroyView();
	}*/
	
	private void addWeatherFragment(){
		mFragmentWeather=new FragmentWeather();
		getChildFragmentManager().beginTransaction().add(R.id.weather_container, mFragmentWeather, FragmentWeather.class.getName()).commit();
	}
	

	IntentFilter timeZoneIntentFilter = new IntentFilter(
			AppConstants.ACTION_TIME_ZONE_FOUND);
	BroadcastReceiver timeZoneBroadCastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String TimeZoneId = intent.getExtras().getString(
					AppConstants.TIME_ZONE_ID);
			if (mDigitalClock != null) {
				mDigitalClock.setVisibility(View.VISIBLE);
				mDigitalClock.setTimeZone(TimeZoneId);
			}
			// Toast.makeText(mContext, TimeZoneId, Toast.LENGTH_LONG).show();
		}
	};

	/*
	 * When location notification fetched, the service which fetch location
	 * notifications broadcast event, we need to catch it here so that open
	 * slider activity.
	 */

	private IntentFilter locationNotificationIntentFilter = new IntentFilter(
			LocationBeaconsService.ACTION_LOCATION_NOTIFICATIONS_FOUND);
	private BroadcastReceiver locationNotificationReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			showLoader(false);
			ArrayList<NotificationData> arr = intent.getParcelableArrayListExtra(LocationBeaconsService.NOTIFICATIONS_ARRAY);
			showSlides(arr, "search");
		}
	};
	
	
	
	//Whenever location selected.
	public void onEvent(ApplicationEvent event) {
		if (event.getType().equals(AppConstants.LOCATION_CHANGED)) {
			mLocationData = (LocationData) event.getData();
			if (mLocationData != null) {
				setLocation(mLocationData);
			}
		}
	}

	private void putIntoDB(ArrayList<NotificationData> arr) {
		int total = arr.size();
		for (int i = 0; i < total; i++) {
			insertNotificationIntoDB(arr.get(i));
		}
		getAllNotifications();
	}
	
	
	
	private void checkLastLocation(){
		//Check if there is any last location saved.
		LocationData ld=(LocationData) mApp.getGSONPreferences(AppConstants.LAST_LOCATION_DATA, null, LocationData.class);
		if(ld!=null){
			setLocation(ld);
		}
	}

	private void showSlides(ArrayList<NotificationData> arr, String which) {
		// Location notification found.
		if (arr != null && arr.size() > 0) {
			if (which.equals("location")) {
				hasLocationNotifications = true;
			} else if (which.equals("search")) {
				hasSearchNotifications = true;
			}

			//mMsgNoSlides.setVisibility(View.INVISIBLE);
			// Save all notificaiton into device preferenes.
			mApp.saveGSONPreferences(AppConstants.LAST_LOCATION_NOTIFICATIONS,
					arr);
			// Add slides on minislider at home screen.
			addMiniSlides(arr);
		}

		// No notification found
		else {
			//mMsgNoSlides.setVisibility(View.VISIBLE);
			//mTxtNoSlides.setText(getResources().getString(R.string.txt_not_notificatons));
			mApp.saveGSONPreferences(AppConstants.LAST_LOCATION_NOTIFICATIONS,null);
			emptyMiniSlider();
		}

	}




	private void showLoader(boolean b) {
		if (mLoader != null)
			mLoader.setVisibility((b == true) ? View.VISIBLE : View.INVISIBLE);
	}


	OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			// TODO Auto-generated method stub
			switch (v.getId()) {
			
			case R.id.btnThings:
				searchTagsClicked("Entertainment, Tourism, Service");
				break;
				
			case R.id.btnFoods:
				searchTagsClicked("Food & Drinks");
				break;
				
			case R.id.btnShopping:
				searchTagsClicked("Retailers & Shops");
				break;

			case R.id.btnEvents:
				searchTagsClicked("Events");
				break;


			case R.id.btnMaps:
				if (mLocationData != null) {
					Intent mapIntent = new Intent(mContext,
							GoogleMapActivity.class);
					LatLng l = new LatLng(mLocationData.getLatitude(),
							mLocationData.getLongitude());
					mapIntent.putExtra("position", l);
					startActivity(mapIntent);
				}

				break;

			case R.id.btnRegion:
				if (mLocationData != null) {
					mFragmentInterface.onRegionIconClicked(mLocationData);
				}
				break;

			}
		}
	};

	@Override
	public void onSliderClick(BaseSliderView slider) {
		Toast.makeText(mContext, slider.getBundle().get("extra") + "",
				Toast.LENGTH_SHORT).show();
	}

	/*******************************************************************
	 * FUNCTION TO BE ACCESSED FROM OUTSIDE OF THIS FRAGMENT
	 *******************************************************************/
	// Accessible from outside (activity).
	public void setLocation(LocationData ld) {
		if (ld == null) {
			return;
		}
		
		mLocationData = ld;
		mTimeContainer.setVisibility(View.VISIBLE);

		cityName = ld.getCityName();

		if (cityName == null) {
			return;
		}

		// In case of current location from GPS
		if (ld.getSource().equals(AppConstants.LOCATION_SOURCE_GPS)) {
			rootView.findViewById(R.id.location_heading).setVisibility(
					View.VISIBLE);
		}

		// Location searched with search tool.
		else {
			// Call Google TimeZoneAPI through custom service and get timezone
			// by location.
			getTimeZoneByLocation(ld);
			rootView.findViewById(R.id.location_heading).setVisibility(
					View.INVISIBLE);
		}

		TextView tv = (TextView) rootView.findViewById(R.id.txt_location);
		tv.setText(cityName.toUpperCase());
		
		if(mFragmentWeather!=null){
			mFragmentWeather.setLocation(ld);
		}
		fetchLocationNotifications(mLocationData);
	}

	public void emptyMiniSlider() {
		mSlider.removeAllSliders();
		mSliderContainer.setVisibility(View.INVISIBLE);
		mSliderSearchContainer.setVisibility(View.VISIBLE);
	}

	public void addMiniSlides(ArrayList<NotificationData> notifications) {
		mAllNotifications = notifications;
		if (notifications.size() > 0) {
			slidesAvailable = true;
		}

		mSliderContainer.setVisibility(View.VISIBLE);
		mSliderSearchContainer.setVisibility(View.INVISIBLE);
		
		mSlider.removeAllSliders();

		int totalSlides = notifications.size();
		if (notifications.size() > 5) {
			totalSlides = 6;
		}

		for (int i = 0; i < totalSlides; i++) {
			// If more than 5 slides, we show a "See Full List" button
			if (i == 5) {
				SeeAllMiniSliderView seeAllSlide = new SeeAllMiniSliderView(
						mContext);
				seeAllSlide.setOnSliderClickListener(slideClickListener);
				seeAllSlide.getBundle().putString("type", "seeAll");
				mSlider.addSlider(seeAllSlide);
			} else {
				NotificationMiniSliderView notificationSlide = new NotificationMiniSliderView(
						mContext);

				NotificationData nd = notifications.get(i);

				// TextSliderView textSliderView = new TextSliderView(mContext);
				notificationSlide
						.title(nd.getTitle())
						.siteName(nd.getSiteName())
						.description(nd.getDetail())
						.setScaleType(
								NotificationSliderView.ScaleType.CenterCrop)
						.setOnSliderClickListener(slideClickListener);

				if (!nd.getImages().equals("")) {
					notificationSlide.image(nd.getImages().split(",")[0]);
				}

				ArrayList<CategoryItem> cats = nd.getCategories();
				if (cats != null && cats.size() > 0) {
					notificationSlide.categories(cats);
				}

				// initialize a SliderLayout
				// add your extra information
				notificationSlide.getBundle().putString("type", "normal");
				notificationSlide.getBundle().putParcelable("notification", nd);
				// .putString("extra", notifications.get(i).getID().toString());
				mSlider.addSlider(notificationSlide);
			}
		}
	}
	
	
	public void addSearchSlides() {
		ArrayList<NotificationData> notifications=new ArrayList<NotificationData>();
		NotificationData snd=new NotificationData();
		snd.setSiteName("Food & Drinks");
		snd.setDetail("Food & Drinks");
		snd.setTitle("");
		snd.setID(R.drawable.notif_img_1);
		notifications.add(snd);
		
		
		
		NotificationData snd2=new NotificationData();
		snd2.setSiteName("Latest Events");
		snd2.setTitle("");
		snd.setDetail("Events");
		snd2.setID(R.drawable.notif_event_1);
		notifications.add(snd2);
		

		int totalSlides = notifications.size();

		for (int i = 0; i < totalSlides; i++) {
			NotificationMiniSliderView searchSlide = new NotificationMiniSliderView(mContext);
			NotificationData nd = notifications.get(i);
				// TextSliderView textSliderView = new TextSliderView(mContext);
			searchSlide
						.title(nd.getTitle())
						.siteName(nd.getSiteName())
						.description(nd.getDetail())
						.setScaleType(
								NotificationSliderView.ScaleType.CenterCrop)
						.setOnSliderClickListener(slideClickListener);

			if (nd.getID()!=0) {
				searchSlide.image(nd.getID());
			}
			// initialize a SliderLayout
			// add your extra information
			searchSlide.getBundle().putString("type", "fixed");
			searchSlide.getBundle().putParcelable("notification", nd);
			// .putString("extra", notifications.get(i).getID().toString());
			mSliderSearch.addSlider(searchSlide);
		}
	}
	
	
	private void checkLastSliderData(){
		LocationData ld = (LocationData) mApp.getGSONPreferences(AppConstants.LAST_LOCATION_DATA, "", LocationData.class);
		if (ld != null) {
			ArrayList<NotificationData> arr = (ArrayList<NotificationData>) mApp.getGSONTypeTokenPreferences(AppConstants.LAST_LOCATION_NOTIFICATIONS, "", new TypeToken<ArrayList<NotificationData>>() {});
			showSlides(arr, "location");
		}
	}
	
	
	
	
	
	
	
	

	private OnSliderClickListener slideClickListener = new OnSliderClickListener() {
		@Override
		public void onSliderClick(BaseSliderView slide) {
			// TODO Auto-generated method stub
			// In case see all slide clicked, we send all searched notifications
			// notification
			String slideType=slide.getBundle().getString("type");
			if(slideType.equals("seeAll")){
				searchTagsClicked("");
				//mFragmentInterface.onSliderItemClicked(mAllNotifications);
			}
			
			else if(slideType.equals("fixed")){
				NotificationData nd = (NotificationData) slide.getBundle()
						.getParcelable("notification");
				String tags=nd.getDetail();
				searchTagsClicked(tags);
			}
			else {
				ArrayList<NotificationData> arr = new ArrayList<NotificationData>();
				NotificationData nd = (NotificationData) slide.getBundle()
						.getParcelable("notification");
				arr.add(nd);

				// INSERT SELECTED NOTIFICATION INTO LOCAL DATABSE FOR KEEPING
				// HISTORY LIST UPDATED
				// putIntoDB(arr);
				mFragmentInterface.onSliderItemClicked(arr);
			}

			/*
			 * Intent intent=new Intent(SliderFragment.this,
			 * NotificationFragment.class); NotificationData
			 * nd=(NotificationData
			 * )slider.getBundle().getParcelable("notification");
			 * intent.putExtra(NotificationFragment.NOTIFICATION_TO_SHOW, nd);
			 * startActivity(intent);
			 */
		}
	};

	
	
	//Post event which will be caught in search fragment and initiate search with predefined tags.
	private void searchTagsClicked(String tags){
		ApplicationEvent evt=new ApplicationEvent(AppConstants.SEARCH_TAGS_CLICKED, tags, null);
		EventBus.getDefault().post(evt);
	}
	
	
	


	public void fetchLocationNotifications(LocationData ld) {
		// TODO Auto-generated method stub
		showLoader(true);
		Intent intent = new Intent(getActivity(), LocationBeaconsService.class);
		intent.putExtra("latitude", Double.toString(ld.getLatitude()));
		intent.putExtra("longitude", Double.toString(ld.getLongitude()));
		mContext.startService(intent);
	}


	/*
	 * Called when all notifications from local database are fetched and
	 * available to be manipulated anywhere in the view.
	 * 
	 * @see com.beaconwatcher.rightips.fragments.NotificationReceiverFragment#
	 * OnAllNotificationFetched()
	 */

	@Override
	public void OnAllNotificationFetched() {
		// TODO Auto-generated method stub
		super.OnAllNotificationFetched();
		if (hasLocationNotifications == false
				&& hasSearchNotifications == false) {
			if (notifications.size() > 0) {
				addMiniSlides(notifications);
			}
		}
	}

	/***********************************************************
	 * TIME WIDGET
	 **********************************************************/
	private void getTimeZoneByLocation(LocationData ld) {
		Log.d("HomeFragment", "sending request to load time zone by latitude: "
				+ ld.getLatitude() + ", longitude: " + ld.getLongitude());
		Intent intent = new Intent(mContext, GetTimeZoneByLocationService.class);
		intent.putExtra("latitude", Double.toString(ld.getLatitude()));
		intent.putExtra("longitude", Double.toString(ld.getLongitude()));
		mContext.startService(intent);
	}
	
	
	
	
}
