package com.beaconwatcher.rightips.fragments;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.brickred.socialauth.android.DialogListener;
import org.brickred.socialauth.android.SocialAuthAdapter;
import org.brickred.socialauth.android.SocialAuthAdapter.Provider;
import org.brickred.socialauth.android.SocialAuthError;
import org.brickred.socialauth.android.SocialAuthListener;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.activities.LoginActivity;
import com.beaconwatcher.rightips.activities.NotificationWallCommentActivity;
import com.beaconwatcher.rightips.activities.YoutubePlayerActivity;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.entities.piwik.PiwikConstants;
import com.beaconwatcher.rightips.entities.piwik.PiwikUtilities;
import com.beaconwatcher.rightips.events.AddNewReviewScreenEvent;
import com.beaconwatcher.rightips.events.ReviewSavedEvent;
import com.beaconwatcher.rightips.services.GetSpecificNotificationService;
import com.beaconwatcher.rightips.services.wall.WallLikeService;
import com.beaconwatcher.rightips.services.wall.WallShareService;
import com.beaconwatcher.rightips.usables.image.URLToBitmapAsyncTask;
import com.beaconwatcher.rightips.usables.image.URLToBitmapAsyncTask.AsyncBitmapResponse;
import com.bw.libraryproject.entities.NotificationData;
import com.bw.libraryproject.entities.SocialMediaLink;
import com.bw.libraryproject.entities.UserDataItem;
import com.bw.libraryproject.utils.TinyUrlTask;
import com.bw.libraryproject.utils.TinyUrlTask.TinyUrlResponseHandler;
import com.bw.libraryproject.utils.YouTubeUtils;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.squareup.picasso.Picasso;


public class FragmentNotification extends NotificationReceiverFragment {
	public static final String NOTIFICATION_TO_SHOW = "NotificationTOShow";


	protected static final String TAG = "NotificationFragment";
	private Context mContext;
	private RighTipsApplication mApp;

	// UI
	private View mSliderContainer;
	private View mYoutubeContainer;
	private ImageView mYoutubeThumbnail;
	private View mBtnPlay;
	private SliderLayout mTopSlider;
	private View rootView;
	private LayoutInflater mLayoutInflater;

	TextView title;
	TextView noteDate;
	TextView detail;
	TextView siteName;
	TextView siteAddress;
	TextView sitePhone;
	TextView discount;
	TextView totalLikes;
	FrameLayout btnMap;
	FrameLayout btnComment;
	FrameLayout btnPhone;
	FrameLayout btnWebsite;

	LinearLayout smContainer;

	View mDetailContainer, mReviewContainer;
	Fragment mFragmentReviews;
	
	String videoUrl="";
	

	Typeface tf;
	Typeface tf2;

	// Broadcast Receivers
	// BroadcastReceiver likeBroadCastReceiver;
	// BroadcastReceiver shareBroadCastReceiver;
	// BroadcastReceiver noteBroadCastReceiver;

	NotificationData selectedNotification;

	RelativeLayout btnShare;
	RelativeLayout btnLike;

	// Social Auth instance
	SocialAuthAdapter adapter;

	/*
	 * When like button is pressed, an IntentService is invoked to send like
	 * request to server, after successful response, we need to tell app that
	 * like operation is completed. That IntentServices broadcasts this event.
	 */
	IntentFilter likeIntentFilter = new IntentFilter(
			WallLikeService.WALL_LIKE_ACTION);
	BroadcastReceiver likeBroadCastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			showLoader(false);

			Boolean success = intent.getBooleanExtra("success", false);
			if (success == true) {
				Toast.makeText(mContext, "Your Like request was successful",
						Toast.LENGTH_LONG).show();
				Log.d("NotificationFragment: selected notification id:",
						selectedNotification.getNoteID());
				if (selectedNotification != null) {
					mApp.getTracker()
							.trackEvent(
									PiwikConstants.CATEGORY_NOTIFICATION,
									PiwikConstants.ACTION_LIKE,
									PiwikUtilities
											.getPiwikNotificationString(selectedNotification));
					getNotificationDetails(selectedNotification.getNoteID());
				}
			} else {
				String msg = intent.getStringExtra("message");
				// Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
				mApp.showErrorToast(mContext, "Notification Details");
			}
		}
	};

	/*
	 * When share button is pressed, an IntentService is invoked to send share
	 * request to server, after successful response, we need to tell app that
	 * share operation is completed. That IntentServices broadcasts this event.
	 */
	IntentFilter shareIntentFilter = new IntentFilter(
			WallShareService.WALL_SHARE_ACTION);
	BroadcastReceiver shareBroadCastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			Toast.makeText(mContext, "Your Share request was successful",
					Toast.LENGTH_LONG).show();
			// shareNotification();
		}
	};

	/*
	 * When notification updated details is loaded via
	 * GetSpecificNotificationService we need to update contents on screen.
	 */
	IntentFilter noteIntentFilter = new IntentFilter(
			GetSpecificNotificationService.GET_SPECIFIC_NOTIFICATION_SERVICE);
	BroadcastReceiver noteBroadCastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			NotificationData nd = intent.getParcelableExtra("notification");
			if (nd != null) {
				updateNotification(nd);
			}

			// Toast.makeText(mContext, "notification updated data loaded",
			// Toast.LENGTH_LONG).show();
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mApp = (RighTipsApplication) getActivity()
				.getApplicationContext();
		mContext = getActivity();

		// Add it to Library
		adapter = new SocialAuthAdapter(new ResponseListener());

		// Add providers
		adapter.addProvider(Provider.FACEBOOK, R.drawable.facebook);
		/*
		 * adapter.addProvider(Provider.TWITTER, R.drawable.twitter);
		 * adapter.addProvider(Provider.LINKEDIN, R.drawable.linkedin);
		 * adapter.addProvider(Provider.YAHOO, R.drawable.yahoo);
		 * adapter.addProvider(Provider.EMAIL, R.drawable.email);
		 * adapter.addProvider(Provider.MMS, R.drawable.mms);
		 */
		// Providers require setting user call Back url
	}

	// This method will be called when a MessageEvent is posted
	public void onEvent(AddNewReviewScreenEvent event) {
		showAddReview(event.getRating());
	}

	public void onEvent(ReviewSavedEvent event) {
		removeAddReview();
	}

	private void showAddReview(float rating) {
		FragmentAddReview fragment = new FragmentAddReview();
		Bundle b = new Bundle();
		b.putFloat(AppConstants.REVIEW_RATING, rating);
		b.putString(AppConstants.USER_ID, mApp.getUID());
		b.putString(AppConstants.SITE_ID, selectedNotification.getSiteID());
		b.putString(AppConstants.SITE_NAME, selectedNotification.getSiteName());
		b.putString(AppConstants.USER_NAME, mApp.getUserName());
		fragment.setArguments(b);
		getChildFragmentManager()
				.beginTransaction()
				.add(R.id.fragment_container, fragment,
						FragmentAddReview.class.getName()).commit();
	}

	private void removeAddReview() {
		FragmentAddReview fragment = (FragmentAddReview) getChildFragmentManager()
				.findFragmentByTag(FragmentAddReview.class.getName());
		if (fragment != null)
			getChildFragmentManager().beginTransaction().remove(fragment)
					.commit();
	}

	@Override
	public void onResume() {
		super.onResume();
		/*
		 * If user clicked a notificaiton in Slider Activity, then we need to
		 * show that notification.
		 */

		LocalBroadcastManager.getInstance(mContext).registerReceiver(
				likeBroadCastReceiver, likeIntentFilter);
		LocalBroadcastManager.getInstance(mContext).registerReceiver(
				shareBroadCastReceiver, shareIntentFilter);
		LocalBroadcastManager.getInstance(mContext).registerReceiver(
				noteBroadCastReceiver, noteIntentFilter);

		Bundle b = getArguments();
		if (!(b == null) && !b.isEmpty()) {
			if (b.containsKey("notification")) {
				NotificationData nd = b.getParcelable("notification");
				if (nd != null) {
					selectedNotification = nd;
					showNotification(nd);
				}
			} else if (b.containsKey("id")) {
				String id = b.getString("id");
				getNotificationDetails(id);
			}

		}

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		LocalBroadcastManager.getInstance(mContext).unregisterReceiver(
				likeBroadCastReceiver);
		LocalBroadcastManager.getInstance(mContext).unregisterReceiver(
				shareBroadCastReceiver);
		LocalBroadcastManager.getInstance(mContext).unregisterReceiver(
				noteBroadCastReceiver);

		super.onPause();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mLayoutInflater = inflater;
		tf = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/Aller.ttf");
		tf2 = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/Aller_Rg.ttf");

		rootView = inflater.inflate(R.layout.fragment_notification, container,
				false);
		
		
		mSliderContainer = rootView.findViewById(R.id.slider_container);
		mYoutubeContainer = rootView.findViewById(R.id.youtube_container);
		mYoutubeThumbnail = (ImageView) rootView.findViewById(R.id.youtube_thumbnail);
		
		mBtnPlay = rootView.findViewById(R.id.btn_play);
		mBtnPlay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!videoUrl.equals("")){
					Intent intent=new Intent(getActivity(), YoutubePlayerActivity.class);
					intent.putExtra("video", videoUrl);
					getActivity().startActivity(intent);
				}
			}
		});
		

		mDetailContainer = rootView.findViewById(R.id.detail_container);
		title = (TextView) rootView.findViewById(R.id.txt_title);
		title.setTypeface(tf);

		noteDate = (TextView) rootView.findViewById(R.id.txt_date);
		noteDate.setTypeface(tf2);

		detail = (TextView) rootView.findViewById(R.id.txt_detail);
		detail.setTypeface(tf2);
		
		detail.setMovementMethod(LinkMovementMethod.getInstance());
		

		siteName = (TextView) rootView.findViewById(R.id.txt_site_name);
		siteAddress = (TextView) rootView.findViewById(R.id.txt_site_address);
		// sitePhone = (TextView) rootView.findViewById(R.id.txt_site_phone);
		discount = (TextView) rootView.findViewById(R.id.txt_discount);
		totalLikes = (TextView) rootView.findViewById(R.id.txt_total_likes);
		mLoader = (RelativeLayout) rootView.findViewById(R.id.loader);

		smContainer = (LinearLayout) rootView
				.findViewById(R.id.social_media_container);
		smContainer.setVisibility(View.INVISIBLE);

		btnMap = (FrameLayout) rootView.findViewById(R.id.btn_map);
		btnComment = (FrameLayout) rootView.findViewById(R.id.btn_comment);
		btnPhone = (FrameLayout) ((LinearLayout) (smContainer.getChildAt(0)))
				.getChildAt(0);
		
		btnWebsite = (FrameLayout) ((LinearLayout) (smContainer.getChildAt(smContainer.getChildCount()-1)))
				.getChildAt(0);
		

		// Reviews
		mReviewContainer = rootView.findViewById(R.id.review_container);

		RadioGroup radioGroup = (RadioGroup) rootView
				.findViewById(R.id.radioGroup);

		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub
				if (checkedId == R.id.radio_infos) {
					mDetailContainer.setVisibility(View.VISIBLE);
					mReviewContainer.setVisibility(View.GONE);
				} else if (checkedId == R.id.radio_review) {
					mDetailContainer.setVisibility(View.GONE);
					mReviewContainer.setVisibility(View.VISIBLE);
					showReviews(selectedNotification.getSiteID());
				}
			}
		});

		// Disable all social media links
		for (int i = 0; i < smContainer.getChildCount(); i++) {
			LinearLayout btnCont = (LinearLayout) smContainer.getChildAt(i);
			FrameLayout btn = (FrameLayout) btnCont.getChildAt(0);
			btn.setEnabled(false);
		}

		// Set max records to unlimited by assigning 0
		maxNotifications = 0;
		mTopSlider = (SliderLayout) rootView.findViewById(R.id.slider);
		mTopSlider.setPresetTransformer(SliderLayout.Transformer.Default);
		mTopSlider.setPresetIndicator(SliderLayout.PresetIndicators.Right_Top);
		mTopSlider.setCustomAnimation(new DescriptionAnimation());
		//mTopSlider.setDuration(4000);
		//mTopSlider.startAutoCycle(10000,  5000, false);
		

		btnMap.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String lat = selectedNotification.getSiteLat();
				String lng = selectedNotification.getSiteLong();

				if (!lat.equals("0") && !lng.equals("0") && !lat.equals("")
						&& !lng.equals("")) {
					mFragmentInterface.onMapIconClicked(selectedNotification);
				} else {
					mApp.showErrorToast(mContext,
							"Notification coordinates data");
					// Toast.makeText(mContext, "No coordinates found",
					// Toast.LENGTH_LONG).show();
				}
			}
		});

		btnComment.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mApp.isLoggedIn()) {
					commentNotification();
				} else {
					// If not logged, show login activity.
					Intent loginIntent = new Intent(FragmentNotification.this
							.getActivity(), LoginActivity.class);
					// We don't want to instruct login to decide for Facebook or
					// Internal.
					loginIntent.putExtra(LoginActivity.EXTRAS_LOGIN_TYPE, 3);
					startActivityForResult(loginIntent,
							AppConstants.REQUEST_CODE_LOGIN_TO_COMMENT);
				}
			}
		});

		btnPhone.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!selectedNotification.getSitePhone().equals("")) {
					Intent intent = new Intent(Intent.ACTION_CALL);
					intent.setData(Uri.parse("tel:"
							+ selectedNotification.getSitePhone()));
					startActivity(intent);
				} else {
					mApp.showErrorToast(mContext, "No Phone number");
					// Toast.makeText(mContext, "No Phone number found",
					// Toast.LENGTH_LONG).show();
				}
			}
		});
		
		btnWebsite.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!selectedNotification.getNoteUrl().equals("")) {
					String url = selectedNotification.getNoteUrl();
					if (!url.startsWith("http://")
							&& !url.startsWith("https://"))
						url = "http://" + url;
					Intent browserIntent = new Intent(
							Intent.ACTION_VIEW, Uri.parse(url));
					mContext.startActivity(browserIntent);
				} else {
					mApp.showErrorToast(mContext, "No website found in this venue.");
				}
			}
		});
		
		

		btnLike = (RelativeLayout) rootView.findViewById(R.id.btnLike);
		btnLike.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mApp.isLoggedIn()) {
					likeNotification();
				} else {
					// If not logged, show login activity.
					Intent loginIntent = new Intent(FragmentNotification.this
							.getActivity(), LoginActivity.class);
					// We don't want to instruct login to decide for Facebook or
					// Internal.
					loginIntent.putExtra(LoginActivity.EXTRAS_LOGIN_TYPE, 3);
					startActivityForResult(loginIntent,
							AppConstants.REQUEST_CODE_LOGIN_TO_LIKE);
				}

			}
		});

		btnShare = (RelativeLayout) rootView.findViewById(R.id.btnShare);
		// Enable Provider
		adapter.enable(btnShare);

		return rootView;
	}
	
	

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		// If this is resumed after login, most probably it was because
		// Share, Like button or Comment was pressed.
		// We need to call appropriate feature
		if (requestCode == AppConstants.REQUEST_CODE_LOGIN_TO_LIKE) {
			likeNotification();
		}
		if (requestCode == AppConstants.REQUEST_CODE_LOGIN_TO_SHARE) {
			shareClicked();
		}
		if (requestCode == AppConstants.REQUEST_CODE_LOGIN_TO_COMMENT) {
			commentNotification();
		}
	}

	private void shareClicked() {
		// TODO Auto-generated method stub
		if (mApp.isLoggedIn()) {
			shareNotification();

		} else {
			// If not logged, show login activity.
			Intent loginIntent = new Intent(
					FragmentNotification.this.getActivity(),
					LoginActivity.class);
			// We don't want to instruct login to decide for Facebook or
			// Internal.
			loginIntent.putExtra(LoginActivity.EXTRAS_LOGIN_TYPE, 3);
			startActivityForResult(loginIntent,
					AppConstants.REQUEST_CODE_LOGIN_TO_SHARE);
		}

	}

	private void commentNotification() {
		String uid = mApp.getUID();
		if (uid != null) {
			Intent intent = new Intent(mContext,
					NotificationWallCommentActivity.class);
			intent.putExtra("uid", uid);
			intent.putExtra("msg_id", selectedNotification.getMsgID());
			intent.putExtra("title", selectedNotification.getTitle());
			getActivity().startActivity(intent);
		}
	}

	private void getNotificationDetails(String noteID) {
		showLoader(true);
		Log.d("NotificationFragment", "getNotificationDetails id: " + noteID);
		Intent intent = new Intent(mContext,
				GetSpecificNotificationService.class);
		intent.putExtra("note_id", noteID);
		intent.putExtra("singleNote", true);
		mContext.startService(intent);
	}

	private void showReviews(String siteID) {
		if (mFragmentReviews == null) {
			mFragmentReviews = new FragmentReviews();
			Bundle b = new Bundle();
			b.putString(AppConstants.REVIEWS_MODE,
					AppConstants.REVIEWS_MODE_SITE);
			b.putString(AppConstants.SITE_ID, siteID);
			b.putString(AppConstants.USER_NAME, mApp.getUserName());
			mFragmentReviews.setArguments(b);
			getChildFragmentManager()
					.beginTransaction()
					.add(R.id.review_container, mFragmentReviews,
							FragmentReviews.class.getName()).commit();
		} else {
			((FragmentReviews) mFragmentReviews).loadReviews();
		}
	}

	private void likeNotification() {
		String uid = mApp.getUID();
		if (uid != null) {
			showLoader(true);
			Intent intent = new Intent(getActivity(), WallLikeService.class);
			intent.putExtra("uid", uid);
			intent.putExtra("msg_id", selectedNotification.getMsgID());
			getActivity().startService(intent);
		}
	}

	private void shareNotification() {
		String uid = mApp.getUID();
		if (uid != null) {
			Intent intent = new Intent(getActivity(), WallShareService.class);
			intent.putExtra("user_id", uid);
			intent.putExtra("msg_id", selectedNotification.getMsgID());
			getActivity().startService(intent);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		View rv = rootView.findViewById(R.id.root_view);
		if (rv != null) {
			UnbindDrawables(rv);
		}
		System.gc();
	}
	
	

	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) {
	 * getMenuInflater().inflate(R.menu.refresh_menu, menu); //MenuItem
	 * refreshItem = menu.findItem(R.id.refresh);
	 * //refreshItem.setActionView(R.layout.actionbar_indeterminate_progress);
	 * return true; }
	 * 
	 * @Override public boolean onOptionsItemSelected(MenuItem item) {
	 * 
	 * if(item.getItemId() == R.id.refresh){ Intent intent=new Intent(mContext,
	 * RighTipsMonitorService.class); intent.putExtra("reloadNotifications",
	 * true); mContext.startService(intent); }
	 * 
	 * return super.onOptionsItemSelected(item); }
	 */

	public void showNotification(NotificationData nd) {
		nd.setStatus("opened");
		insertNotificationIntoDB(nd);

		selectedNotification = nd;
		showLoader(true);
		getNotificationDetails(nd.getNoteID());
	}

	/*
	 * @Override public void onNotificationBroadcastReceived(NotificationData
	 * nd) { // TODO Auto-generated method stub showNotification(nd); }
	 */

	public void updateNotification(NotificationData nd) {
		smContainer.setVisibility(View.VISIBLE);
		mApp.getTracker().trackEvent(PiwikConstants.CATEGORY_NOTIFICATION,
				PiwikConstants.ACTION_VIEW,
				PiwikUtilities.getPiwikNotificationString(nd));

		showLoader(false);
		selectedNotification = nd;
		
		
		//Check if video url is provided, then only show youtube video player
		if(!nd.getVideoUrl().equals("") && !nd.getVideoUrl().equals("null")){
			mTopSlider.stopAutoCycle();
			mSliderContainer.setVisibility(View.GONE);
			mYoutubeContainer.setVisibility(View.VISIBLE);
			videoUrl=YouTubeUtils.getVideoId(nd.getVideoUrl());
			Picasso.with(mContext).load("http://img.youtube.com/vi/"+videoUrl+"/0.jpg").fit().into(mYoutubeThumbnail);
		}
		
		
		else{
			mTopSlider.startAutoCycle(10000,  5000, false);
			mSliderContainer.setVisibility(View.VISIBLE);
			mYoutubeContainer.setVisibility(View.GONE);
			
			mTopSlider.removeAllSliders();
			// Add analytic event.
			// AnalyticsEvent evt=mApp.getAnalyticsEvent(mContext,
			// "Notification Opened");
			/*
			 * MUST UNCOMMENT evt.addAttribute("Beacon Zone", nd.zone);
			 * evt.addAttribute("Notification ID", nd.id);
			 * evt.addAttribute("Notification Title", nd.title);
			 * evt.addAttribute("Notification Content", nd.template);
			 * mApp.logAnalyticsEvent(evt);
			 */

			if (nd.getImages().length() > 0) {
				String[] images = nd.getImages().split(", ");
				///mApp.showToast(mContext, nd.getImages(), Toast.LENGTH_LONG);
				for (int i = 0; i < images.length; i++) {
					DefaultSliderView imageSlide = new DefaultSliderView(
							getActivity());
					// initialize a SliderLayout
					imageSlide.image(images[i]).setScaleType(
							BaseSliderView.ScaleType.CenterCrop);
					// .setOnSliderClickListener(this);

					// add your extra information
					imageSlide.getBundle().putParcelable("notification", nd);
					mTopSlider.addSlider(imageSlide);
				}
			} else {
				DefaultSliderView imageSlide = new DefaultSliderView(getActivity());
				imageSlide.image(R.drawable.cat_no_image).setScaleType(
						BaseSliderView.ScaleType.Fit);
				// .setOnSliderClickListener(this);

				imageSlide.getBundle().putParcelable("notification", nd);
				mTopSlider.addSlider(imageSlide);
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		

		title.setText(nd.getTitle());
		if (!nd.getStartDate().equals("") && !nd.getEndDate().equals("")) {
			noteDate.setText(nd.getStartDate() + " - " + nd.getEndDate());
		} else {
			noteDate.setVisibility(View.GONE);
		}

		detail.setText(nd.getDetail());
		
		siteName.setText(nd.getSiteName());
		siteAddress.setText(nd.getSiteAddress());
		// sitePhone.setText(nd.getSitePhone());
		if (!nd.getDiscount().equals("0") && !nd.getDiscount().equals("")) {
			discount.setVisibility(View.VISIBLE);
			discount.setText(nd.getDiscount());
		}

		totalLikes.setText(nd.getLikes());

		String uid = mApp.getUID();
		if (uid != null) {
			ArrayList<UserDataItem> arr = nd.getUsersLiked();

			for (int i = 0; i < arr.size(); i++) {
				if (arr.get(i).getUserID().equals(uid)) {
					btnLike.setEnabled(false);
				}
			}
		}

		if (nd.getSitePhone().equals("")) {
			btnPhone.setEnabled(false);
		} else {
			btnPhone.setEnabled(true);
		}
		
		if(nd.getNoteUrl().equals("")){
			btnWebsite.setEnabled(false);
		}
		else{
			btnWebsite.setEnabled(true);
		}
		
		

		/*************************************************************
		 * SOCIAL-MEDIA LINKS
		 *************************************************************/
		ArrayList<SocialMediaLink> links = nd.getSocialMediaLinks();

		for (int i = 0; i < links.size(); i++) {
			final SocialMediaLink sLink = links.get(i);
			LinearLayout btnCont = (LinearLayout) smContainer
					.getChildAt(Integer.parseInt(sLink.getID()));

			FrameLayout btn = null;
			if (btnCont != null) {
				btn = (FrameLayout) btnCont.getChildAt(0);
			}

			if (btn != null) {
				if (sLink.getUrl().equals("")) {
					btn.setEnabled(false);
				} else {
					btn.setEnabled(true);
				}

				btn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// If It is Email link button.
						if (sLink.getID().equals("4")) {
							startEmailClient(sLink.getUrl());
						} else {
							String url = sLink.getUrl();
							if (!url.startsWith("http://")
									&& !url.startsWith("https://"))
								url = "http://" + url;
							Intent browserIntent = new Intent(
									Intent.ACTION_VIEW, Uri.parse(url));
							mContext.startActivity(browserIntent);
						}
					}
				});
			}

		}
	}

	private void startEmailClient(String email) {
		final Intent emailIntent = new Intent(
				Intent.ACTION_SEND);

		emailIntent.setType("text/html");
		emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { email });
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
		emailIntent.putExtra(Intent.EXTRA_TEXT, Html
				.fromHtml("<small>" + "" + "</small>"));
		startActivity(Intent.createChooser(emailIntent, "Email:"));

		/*
		 * Intent emailIntent = new Intent(Intent.ACTION_VIEW,
		 * Uri.parse("mailto:")); PackageManager pm =
		 * mContext.getPackageManager();
		 * 
		 * List<ResolveInfo> resInfo = pm.queryIntentActivities(emailIntent, 0);
		 * if (resInfo.size() > 0) { ResolveInfo ri = resInfo.get(0); // First
		 * create an intent with only the package name of the first registered
		 * email app // and build a picked based on it Intent intentChooser =
		 * pm.getLaunchIntentForPackage(ri.activityInfo.packageName); Intent
		 * openInChooser = Intent.createChooser(intentChooser,
		 * "Choose Email Client");
		 * 
		 * // Then create a list of LabeledIntent for the rest of the registered
		 * email apps List<LabeledIntent> intentList = new
		 * ArrayList<LabeledIntent>(); for (int i = 1; i < resInfo.size(); i++)
		 * { // Extract the label and repackage it in a LabeledIntent ri =
		 * resInfo.get(i); String packageName = ri.activityInfo.packageName;
		 * Intent intent = pm.getLaunchIntentForPackage(packageName);
		 * intentList.add(new LabeledIntent(intent, packageName,
		 * ri.loadLabel(pm), ri.icon)); }
		 * 
		 * LabeledIntent[] extraIntents = intentList.toArray(new
		 * LabeledIntent[intentList.size()]); // Add the rest of the email apps
		 * to the picker selection
		 * openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
		 * startActivity(openInChooser); }
		 */
	}

	/*************************************************************
	 * SOCIAL-AUTH SHARE FEATURE
	 *************************************************************/
	/**
	 * Listens Response from Library
	 * 
	 */

	private final class ResponseListener implements DialogListener {
		@Override
		public void onComplete(Bundle values) {
			showLoader(true);

			// Get name of provider after authentication
			final String providerName = values
					.getString(SocialAuthAdapter.PROVIDER);
			// Toast.makeText(NotificationFragment.this.getActivity(),
			// providerName + " connected", Toast.LENGTH_LONG).show();
			Log.d(TAG, "Social-Auth connected with = " + providerName);

			String noteURL = selectedNotification.getNoteUrl();// "http://rightips.com/notifications.php?tpl_id=3&nt_id="+selectedNotification.getID().toString();
			shortenUrl(noteURL, providerName);
			// To shorten url

		}

		@Override
		public void onError(SocialAuthError error) {
			Log.e(TAG, "Authentication Error: " + error.getMessage());
		}

		@Override
		public void onCancel() {
			Log.e(TAG, "Authentication Cancelled");
		}

		@Override
		public void onBack() {
			Log.d("ShareButton", "Dialog Closed by pressing Back Key");
		}
	}

	private void shortenUrl(final String longUrl, final String providerName) {
		TinyUrlTask urlTask = new TinyUrlTask(new TinyUrlResponseHandler() {
			@Override
			public void onUrlShortened(String url) {
				// TODO Auto-generated method stub
				sharePost((url.equals("") ? longUrl : url), providerName);
			}
		});

		urlTask.execute(longUrl);
	}

	private void sharePost(String noteURL, final String providerName) {
		final String caption = "Check out\n"
				+ selectedNotification.getSiteName() + "\non " + noteURL;
		String url = selectedNotification.getImages().split(",")[0];
		url = url.replace("http://www.rightips.com",
				"http://s3-eu-west-1.amazonaws.com/rtimage");

		URLToBitmapAsyncTask task = new URLToBitmapAsyncTask(
				new AsyncBitmapResponse() {

					@Override
					public void onBitmapDownloaded(Bitmap bmp) {
						// TODO Auto-generated method stub
						if (bmp != null) {
							Log.d(TAG, "Image downloaded successfuly");
							// If WhatsApp, we need to send image to WhatsApp.
							if (providerName.equals("WhatsApp")) {
								shareImageWhatsApp(caption, bmp);
							}

							else {
								try {
									Log.d(TAG,
											"Trying to upload image on Facebook");
									adapter.uploadImageAsync(caption,
											"icon.png", bmp, 0,
											new UploadImageListener());
								} catch (Exception e) {
									Log.d(TAG,
											"Could not upload image on Facebook: "
													+ e.getMessage());
									showLoader(false);
									// Toast.makeText(mContext,
									// "Exception Uploading Image",
									// Toast.LENGTH_SHORT).show();
								}
							}
						} else {
							Log.e(TAG, "Not a valid Bitmap to share");
							mApp.showErrorToast(mContext,
									"Invalid Bitmap in share feature");
							// Toast.makeText(mContext,
							// "Not a valid Bitmap to share",
							// Toast.LENGTH_SHORT).show();
						}
					}
				});

		task.execute(url);
	}

	// To get status of image upload after authentication
	private final class UploadImageListener implements
			SocialAuthListener<Integer> {
		@Override
		public void onExecute(String provider, Integer t) {
			showLoader(false);

			Integer status = t;
			if (status.intValue() == 200 || status.intValue() == 201
					|| status.intValue() == 204) {
				Log.d(TAG, "Image uploaded and post shared on Facebook");
				Toast.makeText(mContext, "Your post was successful",
						Toast.LENGTH_SHORT).show();
				mApp.getTracker()
						.trackEvent(
								PiwikConstants.CATEGORY_SHARE,
								PiwikConstants.ACTION_FACEBOOK,
								PiwikUtilities
										.getPiwikNotificationString(selectedNotification));
			}

			else {
				Log.d("ShareButton", "Error Posting SocialAuth");
				// Toast.makeText(NotificationFragment.this.getActivity(),
				// "Message not posted on " + provider,
				// Toast.LENGTH_LONG).show();
				mApp.showErrorToast(mContext, "Posting message");
			}

			// shareClicked();
		}

		@Override
		public void onError(SocialAuthError e) {
			// Toast.makeText(NotificationFragment.this.getActivity(),
			// "Your status could not be posted", Toast.LENGTH_LONG).show();
			mApp.showErrorToast(mContext, "Posting message");
		}
	}

	// To get status of message after authentication
	private final class MessageListener implements SocialAuthListener<Integer> {
		@Override
		public void onExecute(String provider, Integer t) {
			Integer status = t;
			if (status.intValue() == 200 || status.intValue() == 201
					|| status.intValue() == 204)
				Toast.makeText(FragmentNotification.this.getActivity(),
						"Message posted on " + provider, Toast.LENGTH_LONG)
						.show();
			else
				Toast.makeText(FragmentNotification.this.getActivity(),
						"Message not posted on " + provider, Toast.LENGTH_LONG)
						.show();

			shareClicked();
		}

		@Override
		public void onError(SocialAuthError e) {
			Log.d("ShareButton", "Error Posting SocialAuth");
		}
	}

	private Bitmap getNotificationBmp() {
		String url = selectedNotification.getImages().split(",")[0];
		url = url.replace("http://www.rightips.com",
				"http://s3-eu-west-1.amazonaws.com/rtimage");
		Bitmap bmp = null;

		// Bitmap bmp=URLToBitmap.DownloadImage(url);
		return bmp;

	}

	/*************************************************************
	 * SHARE WHATS APP
	 *************************************************************/
	public void shareImageWhatsApp(String caption, Bitmap bmp) {
		Intent share = new Intent(Intent.ACTION_SEND);
		share.setType("image/jpeg");
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		File f = new File(Environment.getExternalStorageDirectory()
				+ File.separator + "temporary_file.jpg");
		try {
			f.createNewFile();
			new FileOutputStream(f).write(bytes.toByteArray());
		} catch (IOException e) {
			e.printStackTrace();
		}

		share.putExtra(Intent.EXTRA_TEXT, caption);
		share.putExtra(
				Intent.EXTRA_STREAM,
				Uri.parse(Environment.getExternalStorageDirectory()
						+ File.separator + "temporary_file.jpg"));
		if (isPackageInstalled("com.whatsapp", mContext)) {
			mApp.getTracker().trackEvent(
					PiwikConstants.CATEGORY_SHARE,
					PiwikConstants.ACTION_WHATSAPP,
					PiwikUtilities
							.getPiwikNotificationString(selectedNotification));

			share.setPackage("com.whatsapp");
			startActivity(Intent.createChooser(share, "Share Image"));

		} else {
			Toast.makeText(mContext, "Please Install Whatsapp",
					Toast.LENGTH_LONG).show();
		}

	}

	private boolean isPackageInstalled(String packagename, Context context) {
		PackageManager pm = context.getPackageManager();
		try {
			pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
			return true;
		} catch (NameNotFoundException e) {
			return false;
		}
	}

	private String getNotificationString(NotificationData nd) {
		return PiwikUtilities.getPiwikNotificationString(
				selectedNotification.getNoteID(),
				selectedNotification.getTitle(),
				selectedNotification.getSiteID(),
				selectedNotification.getSiteName(), "0");
	}

}
