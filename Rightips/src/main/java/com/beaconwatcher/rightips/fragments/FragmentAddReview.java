package com.beaconwatcher.rightips.fragments;

import java.io.FileNotFoundException;
import java.io.IOException;

import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.beaconwatcher.rightips.services.ReviewsService;
import com.bw.libraryproject.utils.BitmapUtilities;

public class FragmentAddReview extends AbstractFragment {

	protected static final String TAG = "FragmentAddReview";
	protected static final int SELECT_IMAGE_CODE = 10009;

	private FragmentAddReview mInstance;
	private LayoutInflater mLayoutInflater;

	// Views
	private View mRootView;
	private EditText mTxtReview;
	private RatingBar mRatingBar;
	private ImageView mImageView;
	private View mBtnPost;

	// Fragments
	FragmentSelectImage mFragmentSelectImage;

	// Variables
	TypedArray mRatingValues;
	Uri mSelectedImageUri;

	// Variables from bundle
	String mSiteID;
	String mUserID;
	String mUserName;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mInstance = this;
		mContext = getActivity();
		mApp = (RighTipsApplication) getActivity()
				.getApplicationContext();
		mRatingValues = this.getResources().obtainTypedArray(
				R.array.rating_values);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mLayoutInflater = inflater;
		mRootView = inflater.inflate(R.layout.fragment_add_review, container,
				false);
		mLoader = mRootView.findViewById(R.id.loader);
		mRatingBar = (RatingBar) mRootView.findViewById(R.id.rating_bar);

		Bundle b = getArguments();
		if (!(b == null) && !b.isEmpty()) {
			mUserID = b.getString(AppConstants.USER_ID);
			mSiteID = b.getString(AppConstants.SITE_ID);
			mRatingBar.setRating(b.getFloat(AppConstants.REVIEW_RATING));
			((TextView) mRootView.findViewById(R.id.lbl_what_you_think))
					.setText(b.getString(AppConstants.USER_NAME)
							+ ", "
							+ mContext.getResources().getString(
									R.string.lbl_what_you_think));
			((TextView) mRootView.findViewById(R.id.lbl_place_name)).setText(b
					.getString(AppConstants.SITE_NAME) + "?");
			((TextView) mRootView.findViewById(R.id.lbl_rating_value))
					.setText(mRatingValues.getString((int) b
							.getFloat(AppConstants.REVIEW_RATING) - 1));
		}

		mTxtReview = (EditText) mRootView.findViewById(R.id.et_reviews);

		// mImageView = (ImageView) mRootView.findViewById(R.id.selected_img);
		mBtnPost = mRootView.findViewById(R.id.btn_post);

		mBtnPost.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				postReview();
			}
		});

		mRatingBar
				.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
					@Override
					public void onRatingChanged(RatingBar ratingBar,
							float rating, boolean fromUser) {
						((TextView) mRootView
								.findViewById(R.id.lbl_rating_value))
								.setText(mRatingValues
										.getString((int) rating - 1));
						// TODO Auto-generated method stub

					}
				});

		mRootView.findViewById(R.id.btn_camera).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						addImageSelection();
					}
				});

		return mRootView;
	}

	private void addImageSelection() {
		mFragmentSelectImage = new FragmentSelectImage();
		getChildFragmentManager()
				.beginTransaction()
				.add(R.id.fragment_container, mFragmentSelectImage,
						FragmentSelectImage.class.getName())
				.addToBackStack(FragmentSelectImage.class.getName()).commit();
	}

	public void onEvent(ApplicationEvent event) {
		if (event.getType().equals(AppConstants.IMAGE_SELECTED_FOR_UPLOAD)) {
			mSelectedImageUri = (Uri) ((ApplicationEvent) event).getData();
			try {
				Bitmap bmp = BitmapUtilities.getThumbnail(mSelectedImageUri,
						400, mContext);
				((ImageView) mRootView.findViewById(R.id.btn_camera))
						.setImageBitmap(bmp);// .setImageURI(mSelectedImageUri);
			} catch (IOException e) {
			}

			getChildFragmentManager().beginTransaction()
					.remove(mFragmentSelectImage).commit();
		} else if (event.getType()
				.equals(AppConstants.IMAGE_SELECTION_CANCELED)) {
			getChildFragmentManager().beginTransaction()
					.remove(mFragmentSelectImage).commit();
		}
	}

	public void postReview() {
		if (mTxtReview.getText().toString().equals("")) {
			return;
		}

		showLoader(true);
		Log.d(TAG, "posting new review");

		Intent intent = new Intent(mContext, ReviewsService.class);
		intent.putExtra(AppConstants.INTENT_SERVICE_TAG, TAG);
		intent.putExtra(AppConstants.INTENT_SERVICE_ACTION_TYPE,
				AppConstants.ACTION_POST_REVIEW);
		intent.putExtra(AppConstants.SITE_ID, mSiteID);
		intent.putExtra(AppConstants.USER_ID, mUserID);
		intent.putExtra(AppConstants.REVIEW_RATING, mRatingBar.getRating());
		intent.putExtra(AppConstants.REVIEW_TEXT, mTxtReview.getText()
				.toString());
		if (mSelectedImageUri != null) {
			intent.putExtra(AppConstants.IMAGE_URI, mSelectedImageUri);
		}
		mContext.startService(intent);
	}

}
