package com.beaconwatcher.rightips.fragments;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.beaconwatcher.rightips.R;
import com.bw.libraryproject.entities.NotificationData;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

public class FragmentHistory extends NotificationReceiverFragment {
	private LayoutInflater inflater;
	private ListView historyList;
	private View rootView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// mContext = getActivity().getApplicationContext();
		maxNotifications = 0;
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater infl, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		rootView = infl.inflate(R.layout.fragement_history, container, false);
		inflater = infl;
		historyList = (ListView) rootView.findViewById(R.id.history_list);
		return rootView;
	}

	@Override
	public void OnAllNotificationFetched() {
		// TODO Auto-generated method stub
		super.OnAllNotificationFetched();
		if (notifications.size() > 0) {
			NotificationAdapter adapter = new NotificationAdapter(
					notifications, this.getActivity().getApplicationContext());
			historyList.setAdapter(adapter);
		}
	}

	@Override
	public void onDestroy() {
		View rv = rootView.findViewById(R.id.root_view);
		if (rv != null) {
			UnbindDrawables(rv);
		}
		System.gc();
		super.onDestroy();
	}

	public class NotificationAdapter extends BaseAdapter {
		private Context mContext;
		private ArrayList<NotificationData> _notes;

		public NotificationAdapter(ArrayList<NotificationData> nd, Context c) {
			mContext = c;
			_notes = nd;
		}

		@Override
		public int getCount() {
			return _notes.size();// images.length;
		}

		@Override
		public long getItemId(int i) {
			return i;
		}

		@Override
		public NotificationData getItem(int position) {
			// TODO Auto-generated method stub
			return _notes.get(position);
		}

		@Override
		public View getView(int i, View reuseableView, ViewGroup viewGroup) {
			final View item;// = null;

			if (reuseableView != null) {
				item = (View) reuseableView;
			}

			else {
				item = inflater.inflate(R.layout.history_list_item, viewGroup,
						false);
				item.setLayoutParams(new AbsListView.LayoutParams(
						AbsListView.LayoutParams.MATCH_PARENT, getResources()
								.getDimensionPixelSize(
										R.dimen.history_item_height)));
			}

			NotificationData nd = getItem(i);

			item.setTag(nd);

			TextView tv = (TextView) item.findViewById(R.id.txt_title);
			tv.setText(nd.getTitle());

			TextView tv2 = (TextView) item.findViewById(R.id.txt_site_address);
			tv2.setText(nd.getSiteAddress());

			TextView tv3 = (TextView) item.findViewById(R.id.txt_creation_date);

			DateFormat dateF = DateFormat.getDateTimeInstance();
			String data = dateF.format(new Date(nd.getCreationDate()));

			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"EEE MMM dd, yyyy  hh:mm:ss", Locale.US);
			String dateStr = dateFormat.format(nd.getCreationDate()); // formatted
																		// date
																		// in
																		// string

			tv3.setText(data);

			if (nd.getStatus().equals("posted")) {
				item.setBackgroundColor(Color.parseColor("#ffffff"));
				Typeface boldTypeface = Typeface
						.defaultFromStyle(Typeface.BOLD);
				tv.setTypeface(boldTypeface);
				tv.setTextColor(Color.BLACK);
				tv2.setTextColor(Color.BLACK);
				tv3.setTextColor(Color.BLACK);
			}

			Transformation transformation = new Transformation() {
				@Override
				public Bitmap transform(Bitmap source) {
					int targetWidth = item
							.findViewById(R.id.notification_image).getWidth();

					double aspectRatio = (double) source.getHeight()
							/ (double) source.getWidth();
					int targetHeight = (int) (targetWidth * aspectRatio);
					Bitmap result = Bitmap.createScaledBitmap(source,
							targetWidth, targetHeight, false);
					if (result != source) {
						// Same bitmap is returned if sizes are the same
						source.recycle();
					}
					return result;
				}

				@Override
				public String key() {
					return "transformation" + " desiredWidth";
				}
			};

			Transformation transformation2 = new Transformation() {
				@Override
				public Bitmap transform(Bitmap source) {
					int targetHeight = item.findViewById(
							R.id.notification_image).getHeight();
					int targetWidth = item
							.findViewById(R.id.notification_image).getWidth();

					// double aspectRatio = (double) source.getHeight() /
					// (double) source.getWidth();
					// int targetWidth = (int) (targetHeight * aspectRatio);
					Bitmap result = Bitmap.createScaledBitmap(source,
							targetWidth, targetHeight, false);
					if (result != source) {
						// Same bitmap is returned if sizes are the same
						source.recycle();
					}
					return result;
				}

				@Override
				public String key() {
					return "transformation" + " desiredWidth";
				}
			};

			
			ImageView icon = (ImageView) item.findViewById(R.id.icon_source);

			if (nd.getSource().equals("beacon")) {
				icon.setImageResource(R.drawable.icon_beacon);
			} else {
				icon.setImageResource(R.drawable.icon_beacon_source_location);
			}

			final ImageView img;

			if (nd.getImages().length() > 0) {
				String url = nd.getImages().split(",")[0];
				img = (ImageView) item.findViewById(R.id.notification_image);

				// Picasso.with(mContext).load(url).placeholder(R.drawable.cat_no_image).error(R.drawable.cat_no_image).into(img);
				
				Picasso.with(mContext).load(url).error(android.R.drawable.stat_notify_error).fit().centerCrop().into(img);
/*				Picasso.with(mContext).load(url)
						.error(android.R.drawable.stat_notify_error)
						.transform(transformation2).into(img, new Callback() {
							@Override
							public void onSuccess() {
								// holder.progressBar_picture.setVisibility(View.GONE);
							}

							@Override
							public void onError() {
								// Log.e(LOGTAG, "error");
								// holder.progressBar_picture.setVisibility(View.GONE);
							}
						});
*/			}

			item.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					NotificationData nd = (NotificationData) v.getTag();
					if (nd.getStatus().equals("posted")) {
						nd.setStatus("opened");
						insertNotificationIntoDB(nd);
					}
					mFragmentInterface
							.onHistoryItemClicked((NotificationData) v.getTag());
				}
			});
			return item;
		}
	}
}
