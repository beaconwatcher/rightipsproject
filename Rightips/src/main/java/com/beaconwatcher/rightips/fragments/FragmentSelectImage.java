package com.beaconwatcher.rightips.fragments;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beaconwatcher.rightips.RighTipsApplication;
import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.entities.AppConstants;
import com.beaconwatcher.rightips.events.ApplicationEvent;
import com.bw.libraryproject.utils.BitmapUtilities;

import de.greenrobot.event.EventBus;

public class FragmentSelectImage extends AbstractFragment {
	private static final String ERROR_TAG = "FragmentSelectImage";
	private RighTipsApplication mApp;
	private Context mContext;
	private View mBtnContainer;
	private RelativeLayout mPreviewContainer;
	private ImageView mPreviewImage;
	private TextView mBtnUseImage;
	private TextView mBtnPhotoLibrary;
	private TextView mBtnTakePhoto;
	private TextView mBtnCancel;
	private View mBtnTick;

	private static final int FILECHOOSER_REQUESTCODE = 2888;
	private static final int TAKE_PHOTO_REQUESTCODE = 2889;
	private Uri mSelectedImageURI = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mApp = (RighTipsApplication) getActivity()
				.getApplicationContext();
		mContext = getActivity();

	}

	public View onCreateView(android.view.LayoutInflater inflater,
			android.view.ViewGroup container, Bundle savedInstanceState) {

		View mRootView = inflater.inflate(R.layout.fragement_select_image,
				container, false);
		mBtnContainer = mRootView.findViewById(R.id.btn_container);
		mPreviewContainer = (RelativeLayout) mRootView
				.findViewById(R.id.preview_container);
		mPreviewImage = (ImageView) mRootView.findViewById(R.id.preview_img);

		mBtnUseImage = (TextView) mRootView.findViewById(R.id.btn_use_image);
		mBtnPhotoLibrary = (TextView) mRootView
				.findViewById(R.id.btn_photo_library);
		mBtnTakePhoto = (TextView) mRootView.findViewById(R.id.btn_take_photo);
		mBtnCancel = (TextView) mRootView.findViewById(R.id.btn_cancel);
		mBtnTick = mRootView.findViewById(R.id.btn_tick);

		mBtnCancel.setOnClickListener(mClickListener);
		mBtnPhotoLibrary.setOnClickListener(mClickListener);
		mBtnTakePhoto.setOnClickListener(mClickListener);
		mBtnUseImage.setOnClickListener(mClickListener);
		mPreviewImage.setOnClickListener(mClickListener);
		mBtnTick.setOnClickListener(mClickListener);

		return mRootView;

	};

	OnClickListener mClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {

			case R.id.btn_photo_library:
				openFileChooser();
				break;

			case R.id.btn_take_photo:
				openTakePhoto();
				break;

			case R.id.btn_cancel:
				String senderAction = "";
				if (getArguments() != null
						&& getArguments().containsKey(AppConstants.ACTION_TYPE)) {
					senderAction = getArguments().getString(
							AppConstants.ACTION_TYPE);
				}
				EventBus.getDefault().post(
						new ApplicationEvent(
								AppConstants.IMAGE_SELECTION_CANCELED, null,
								senderAction));
				break;

			// Upload image
			case R.id.btn_use_image:
				if (mSelectedImageURI != null) {
					ImageSelected();
				}
				break;

			case R.id.preview_img:
			case R.id.btn_tick:
				if (mBtnTick.getVisibility() == View.VISIBLE) {
					mBtnTick.setVisibility(View.GONE);
					mPreviewImage.setAlpha(Float.parseFloat(".5"));
					mBtnUseImage.setVisibility(View.GONE);
					mBtnContainer.setVisibility(View.VISIBLE);
				} else {
					mBtnTick.setVisibility(View.VISIBLE);
					mPreviewImage.setAlpha(Float.parseFloat("1"));
					mBtnUseImage.setVisibility(View.VISIBLE);
					mBtnContainer.setVisibility(View.GONE);
				}
				break;

			}
		}
	};

	private void ImageSelected() {
		if (mSelectedImageURI == null) {
			return;
		}

		String senderAction = "";
		if (getArguments() != null
				&& getArguments().containsKey(AppConstants.ACTION_TYPE)) {
			senderAction = getArguments().getString(AppConstants.ACTION_TYPE);
		}
		EventBus.getDefault().post(
				new ApplicationEvent(AppConstants.IMAGE_SELECTED_FOR_UPLOAD,
						mSelectedImageURI, senderAction));
	}

	public void openFileChooser() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		intent.setType("image/*");
		startActivityForResult(intent, FILECHOOSER_REQUESTCODE);
	}

	public void openTakePhoto() {
		try {
			// Create AndroidExampleFolder at sdcard
			File imageStorageDir = new File(
					Environment
							.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
					"RighTipsFolder");

			if (!imageStorageDir.exists()) {
				// Create AndroidExampleFolder at sdcard
				imageStorageDir.mkdirs();
			}

			// Create camera captured image file path and name
			File file = new File(imageStorageDir + File.separator + "IMG_"
					+ String.valueOf(System.currentTimeMillis()) + ".jpg");

			mSelectedImageURI = Uri.fromFile(file);

			// Camera capture image intent
			Intent captureIntent = new Intent(
					MediaStore.ACTION_IMAGE_CAPTURE);
			captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mSelectedImageURI);
			startActivityForResult(captureIntent, TAKE_PHOTO_REQUESTCODE);

		} catch (Exception e) {
			// Toast.makeText(getBaseContext(), "Exception:" +
			// e,Toast.LENGTH_LONG).show();
			mApp.showErrorToast(mContext, ERROR_TAG + " " + e);
		}
	}

	// openFileChooser for Android 3.0+
	public void openFileChooserOld() {

		// Update message
		// mUploadMessage = "Uploading....";

		try {
			// Create AndroidExampleFolder at sdcard
			File imageStorageDir = new File(
					Environment
							.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
					"RighTipsFolder");

			if (!imageStorageDir.exists()) {
				// Create AndroidExampleFolder at sdcard
				imageStorageDir.mkdirs();
			}

			// Create camera captured image file path and name
			File file = new File(imageStorageDir + File.separator + "IMG_"
					+ String.valueOf(System.currentTimeMillis()) + ".jpg");

			mSelectedImageURI = Uri.fromFile(file);

			// Camera capture image intent
			final Intent captureIntent = new Intent(
					MediaStore.ACTION_IMAGE_CAPTURE);

			captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mSelectedImageURI);

			Intent i = new Intent(Intent.ACTION_GET_CONTENT);
			i.addCategory(Intent.CATEGORY_OPENABLE);
			i.setType("image/*");

			// Create file chooser intent
			Intent chooserIntent = Intent.createChooser(i, "Image Chooser");

			// Set camera intent to file chooser
			chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
					new Parcelable[] { captureIntent });

			// On select image call onActivityResult method of activity
			// startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);

			Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
			intent.addCategory(Intent.CATEGORY_OPENABLE);
			intent.setType("image/*");
			startActivityForResult(intent, 2005);

		} catch (Exception e) {
			// Toast.makeText(getBaseContext(), "Exception:" +
			// e,Toast.LENGTH_LONG).show();
			mApp.showErrorToast(mContext, ERROR_TAG + " " + e);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, intent);

		if (resultCode != Activity.RESULT_OK) {
			return;
		}

		if (requestCode == FILECHOOSER_REQUESTCODE) {
			Uri result = null;
			try {
				result = intent.getData();
				mSelectedImageURI = result;
				mPreviewImage.setImageURI(mSelectedImageURI);
				mPreviewImage.setAlpha(Float.parseFloat("1"));
				updateUI();
			} catch (Exception e) {
				// Toast.makeText(getApplicationContext(), "activity :" +
				// e,Toast.LENGTH_LONG).show();
				mApp.showErrorToast(mContext, ERROR_TAG + " " + e);
			}
		}

		else if (requestCode == TAKE_PHOTO_REQUESTCODE) {
			if (mSelectedImageURI == null) {
				return;
			}
			try {
				Bitmap bmp = BitmapUtilities.getThumbnail(mSelectedImageURI,
						400, mContext);
				mPreviewImage.setImageBitmap(bmp);
				mPreviewImage.setAlpha(Float.parseFloat("1"));
				updateUI();
			} catch (IOException e) {
			}
		}
	}

	private void updateUI() {
		// mBtnContainer.setVisibility(View.INVISIBLE);
		mBtnUseImage.setVisibility(View.VISIBLE);
		mPreviewContainer.setVisibility(View.VISIBLE);
		mBtnContainer.setVisibility(View.GONE);
		mBtnTick.setVisibility(View.VISIBLE);
		// Picasso.with(mContext).load(mSelectedImageURI).fit().centerInside().into(mPreviewImage);
	}
}
