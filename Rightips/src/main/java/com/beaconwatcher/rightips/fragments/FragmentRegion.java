package com.beaconwatcher.rightips.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.beaconwatcher.rightips.R;
import com.beaconwatcher.rightips.customslides.SlideRegionHistory;
import com.beaconwatcher.rightips.customslides.SlideRegionPhotos;
import com.bw.libraryproject.entities.LocationData;
import com.bw.libraryproject.interfaces.SuicidalFragmentListener;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.Tricks.ViewPagerEx.OnPageChangeListener;

public class FragmentRegion extends Fragment {
	private static String TAG = "RegionFragment";

	private Context mContext;
	private LayoutInflater mInflater;
	private View mRootView;
	private SliderLayout mSlider;

	private SuicidalFragmentListener suicideListener;
	private LocationData mLocation;

	private SlideRegionPhotos mSlidePhotos;

	@Override
	public View onCreateView(LayoutInflater infl, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mContext = getActivity();
		mRootView = infl.inflate(R.layout.fragment_region, container, false);
		mInflater = infl;
		mSlider = (SliderLayout) mRootView.findViewById(R.id.slider);
		// mSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Top);
		mSlider.setCustomIndicator((PagerIndicator) mRootView
				.findViewById(R.id.custom_indicator_big_slider));

		mSlider.addOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				// TODO Auto-generated method stub
				if (position == 1) {
					// calculateScreenSize();
					SlideRegionPhotos slide = (SlideRegionPhotos) mSlider
							.getCurrentSlider();
					slide.loadPanoramioPhotos();
				}
			}

			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int state) {
				// TODO Auto-generated method stub

			}
		});

		Bundle bundle;
		if (this.getArguments() != null) {
			bundle = getArguments();

			if (bundle.getParcelable("location") != null) {
				mLocation = (LocationData) bundle.getParcelable("location");
			}
		}

		mRootView.getViewTreeObserver().addOnGlobalLayoutListener(
				new ViewTreeObserver.OnGlobalLayoutListener() {
					@Override
					public void onGlobalLayout() {
						getView().getViewTreeObserver()
								.removeGlobalOnLayoutListener(this);
						calculateScreenSize();
					}
				});

		if (mLocation != null) {
			addSlides();
		}

		return mRootView;
	}

	private void calculateScreenSize() {
		int fragmentWidth = getView().getWidth();
		int fragmentHeight = getView().getHeight();
		if (fragmentWidth > 0) {
			// Width greater than 0, layout should be done.
			if (mSlidePhotos != null) {
				if (fragmentWidth > fragmentHeight
						|| fragmentWidth == fragmentHeight) {
					// mGridColumns=4;
					mSlidePhotos.setNumColumns(4);
					mSlidePhotos.setCellWidth((fragmentWidth / 4) - 40);
					mSlidePhotos.setCellHeight((fragmentHeight / 2) - 100);
					mSlidePhotos.setMaxSlides(8);
				} else {
					// mGridColumns=2;
					mSlidePhotos.setNumColumns(2);
					mSlidePhotos.setCellWidth((fragmentWidth / 2) - 40);
					mSlidePhotos.setCellHeight((fragmentHeight / 3) - 75);
					mSlidePhotos.setMaxSlides(6);
				}
			}
		}
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		mSlider.stopAutoCycle();
		super.onStop();
	}

	private void addSlides() {
		if (mSlider.getChildCount() > 0) {
			mSlider.removeAllSliders();
		}

		// History Slide
		SlideRegionHistory slideHistory = new SlideRegionHistory(mContext,
				mLocation);
		mSlider.addSlider(slideHistory);

		// Photos Slide
		mSlidePhotos = new SlideRegionPhotos(mContext, mLocation);
		mSlider.addSlider(mSlidePhotos);
		mRootView.invalidate();
	}
}
