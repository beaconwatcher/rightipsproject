package com.beaconwatcher.rightips.customslides;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.bw.libraryproject.entities.CategoryItem;
import com.daimajia.slider.library.R;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;

/**
 * This is a slider with a description TextView.
 */
public class SeeAllMiniSliderView extends BaseSliderView {

	private String mTitle;
	private String mDiscount;
	private String mSiteName;
	private String mSiteAddress;
	private Context mContext;

	private ArrayList<CategoryItem> mCategories;

	/**
	 * the title of a slider image.
	 * 
	 * @param title
	 * @return
	 */
	public SeeAllMiniSliderView title(String title) {
		mTitle = title;
		return this;
	}

	/**
	 * the site name of a slider image.
	 * 
	 * @param site
	 *            name
	 * @return
	 */
	public SeeAllMiniSliderView siteName(String sn) {
		mSiteName = sn;
		return this;
	}

	/**
	 * the discount on slider image.
	 * 
	 * @param dis
	 * @return
	 */
	public SeeAllMiniSliderView discount(String dis) {
		mDiscount = dis;
		return this;
	}

	/**
	 * the site address of a slider image.
	 * 
	 * @param site
	 *            name
	 * @return
	 */
	public SeeAllMiniSliderView siteAddress(String sa) {
		mSiteAddress = sa;
		return this;
	}

	/**
	 * the site address of a slider image.
	 * 
	 * @param site
	 *            name
	 * @return
	 */
	public SeeAllMiniSliderView categories(ArrayList<CategoryItem> cats) {
		mCategories = cats;
		return this;
	}

	public String getTitle() {
		return mTitle;
	}

	public String getSiteName() {
		return mSiteName;
	}

	public String getDiscount() {
		return mDiscount;
	}

	public String getSiteAddress() {
		return mSiteAddress;
	}

	public ArrayList<CategoryItem> getCategories() {
		return mCategories;
	}

	public SeeAllMiniSliderView(Context context) {
		super(context);
		mContext = context;

	}

	@Override
	public View getView() {
		View v = LayoutInflater.from(mContext).inflate(
				R.layout.render_type_mini_notification_see_all, null);
		bindEventAndShow(v, null);
		return v;
	}

}
