package com.bw.libraryproject.app;
/*
Copyright 2014 BeaconWatcher

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */


import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.text.TextUtils;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.annotation.ReportsCrashes;

import java.lang.reflect.Type;
import java.util.Timer;
import java.util.TimerTask;


	@ReportsCrashes(
        formKey = "", // This is required for backward compatibility but not used
        mailTo = "email4shahbaz@gmail.com",
        customReportContent = { ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME, ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL, ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.LOGCAT }                
    )



/**
 * Main application controller class to keep reference for
 * global variables across multiple Activities.
 */
public class ApplicationController extends Application {
	
	public String EXTRA_STRING_DATA = "extraStringData";
	
	
	//BW Demo App key="M19fQlcgU2ltdWxhdGlvbg==";
	//BW Simulation App kye="MF9fQlcgU2ltdWxhdGlvbg==";
	public String appKey="MF9fQlcgU2ltdWxhdGlvbg==";
	
	
	//Login info
	public Boolean LoggedIn = false;
	public String LoginID = "";
	public String LoginName = "";
	
	private SharedPreferences settings=null;
	private ProgressDialog mPd;
	private AsyncHttpClient httpClient;
	private Gson gson=new Gson();
	
	
	@Override
		public void onCreate() {
			// TODO Auto-generated method stub
			super.onCreate();
			 ACRA.init(this);
		}
	
	
	
	
	public String getJSONStringFromClassObject(Object obj){
		return gson.toJson(obj);
	}
	
	public Object getClassObjectFromJsonString(String str, Class c) {
		return gson.fromJson(str,  c);
	}
	
	
	public Object getClassObjectFromJsonString(String str, Type type) {
		return gson.fromJson(str, type);
	}
	
	
	
	
	//Save new key-value pair in shared-preferences
	public void savePreferences(String key, String val){
		if(settings==null){
			settings = getSharedPreferences("RighTipsApp", 0);
		}
		SharedPreferences.Editor editor = settings.edit();
    	editor.putString(key, val);
    	editor.commit();
	}
	
	public void saveGSONPreferences(String key, Object obj){
		String str=gson.toJson(obj);
		if(str!=null){savePreferences(key,  str);}
	}
	
	
	
	
	//Save new key-value pair in shared-preferences
	public String getPreferences(String key, String defaultVal){
		if(settings==null){
			settings = getSharedPreferences("RighTipsApp", 0);
		}
		
		if(defaultVal==null){
			defaultVal="";
		}
		String val = settings.getString(key, defaultVal);
		return val;
	}		

	
	
	public Object getGSONPreferences(String key, String defaultVal, Class clazz){
		String str=getPreferences(key, defaultVal);
		if(str!=defaultVal){
			Object obj=gson.fromJson(str, clazz);
			return obj;
		}
		return null;
	}		


	public Object getGSONTypeTokenPreferences(String key, String defaultVal, TypeToken token){
		String str=getPreferences(key, defaultVal);
		if(str!=defaultVal){
			Object obj=gson.fromJson(str, token.getType());
			return obj;
		}
		return null;
	}		
	
	
	
	//Shoes a progress dialog.
	public void showDialog(Context c, String title, String msg){
		mPd=ProgressDialog.show(c, title, msg);
	}
	
	public void hideDialog(){
		if (mPd!=null){
			mPd.dismiss();
			mPd=null;
		}
	}
	
	//Shows a toast for given message and context.
	public static void showToast(Context context, String message, int duration) {
		try {
			if (context != null) {
				Toast toast = Toast.makeText(context, message, duration);
				toast.show();
				toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	
	
	//Abstract method
    public void showErrorToast(Context context, String type){}

	
	
	
	public static void showDialogMessage(Context context, String message) {
		try {
			if (context != null) {
				AlertDialog alert = new AlertDialog.Builder(context)
						.setMessage(message).setPositiveButton("OK", null)
						.create();
				alert.show();

			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	
	
	
	
	
	public static String verifyEditTextData(Context context,
		EditText editTextView, boolean needTrimSpaces, boolean showMessage) {
		String textOfEditext = null;
		if (editTextView != null) {
			textOfEditext = editTextView.getText().toString();
			if (textOfEditext != null) {
				if (needTrimSpaces) {
					textOfEditext = textOfEditext.trim();
				}
				if (textOfEditext.length() <= 0) {
					textOfEditext = null;

				}
				if (textOfEditext == null && showMessage) {
					String message = "";//Nom d'utilisateur ou mot de passe est manquant";
					if (needTrimSpaces){
						message = "Empty Text Field";
					}
					Toast.makeText(context, "Empty Textfield", Toast.LENGTH_SHORT).show();
				}
			}
		}
		return textOfEditext;
	}
	
	public final static boolean isValidEmail(Context context, CharSequence target) {
        if (target == null) { return false;}
        else if(android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()){
        	return true;
        }
        else{
        	ApplicationController.showDialogMessage(context, "Wrong Email format");
        }
        
        return false;
    }   
	
	public void APICall(String url, RequestParams params, AsyncHttpResponseHandler handler){
		if(httpClient==null){
			httpClient=new AsyncHttpClient();
    		httpClient.setTimeout(36000);
    		httpClient.setMaxConnections(100);
		}
		try{
			params.put("key", appKey);
			httpClient.post(url,  params, handler);
		}
		
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	
	public void APICall(String url, RequestParams params, AsyncHttpResponseHandler handler, Boolean addKey){
		if(httpClient==null){
			httpClient=new AsyncHttpClient();
		}
		if(addKey==true){
			params.put("key", appKey);
		}
		
		try{
			httpClient.get(url, handler);
		}
		
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//For determining if the application was in foreground or background
	private Timer mActivityTransitionTimer;
    private TimerTask mActivityTransitionTimerTask;
    public boolean wasInBackground;
    private final long MAX_ACTIVITY_TRANSITION_TIME_MS = 2000;
	
	
    public void startActivityTransitionTimer() {
        this.mActivityTransitionTimer = new Timer();
        this.mActivityTransitionTimerTask = new TimerTask() {
            public void run() {
                ApplicationController.this.wasInBackground = true;
            }
        };

        this.mActivityTransitionTimer.schedule(mActivityTransitionTimerTask,
                                               MAX_ACTIVITY_TRANSITION_TIME_MS);
    }

    public void stopActivityTransitionTimer() {
        if (this.mActivityTransitionTimerTask != null) {
            this.mActivityTransitionTimerTask.cancel();
        }

        if (this.mActivityTransitionTimer != null) {
            this.mActivityTransitionTimer.cancel();
        }

        this.wasInBackground = false;
    }
    
    
    
    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (SettingNotFoundException e) {
                e.printStackTrace();
            	return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    } 

}
