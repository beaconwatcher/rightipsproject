package com.bw.libraryproject.entities;

import android.os.Parcel;
import android.os.Parcelable;
 
/**
 * @author Shahbaz Ali
 *
 * A basic object that can be parcelled to
 * transfer between objects
 *
 */
public class LocationData implements Parcelable {
	private Double _latitude=0.0;
	private Double _longitude=0.0;
	private String _cityName="";
	private String _address="";
	private String _source="";
	
	/**
	 * Standard basic constructor for non-parcel
	 * object creation
	 */
	public LocationData() { ; };
 
	/**
	 *
	 * Constructor to use when re-constructing object
	 * from a parcel
	 *
	 * @param in a parcel from which to read this object
	 */
	public LocationData(Parcel in) {
		readFromParcel(in);
	}
 
	/**
	 * standard getter functions
	 */
	
	public Double getLatitude() {return _latitude;}
	public Double getLongitude() {return _longitude;}
	public String getCityName(){
		if(_cityName==null){
			_cityName="";
		}
		return _cityName;
	}
	public String getAddress(){return _address;}
	public String getSource(){return _source;}

	
	/**
	 * Standard setter functions
	 */
	public void setLatitude(Double lat){_latitude=lat;}
	public void setLongitude(Double lng){_longitude=lng;}
	public void setCityName(String city){if(city!=null){_cityName=city;}}
	public void setAddress(String address){_address=address;}
	public void setSource(String src){_source=src;}
	
	
	
	@Override
	public int describeContents() {
		return 0;
	}
 
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// We just need to write each field into the
		// parcel. When we read from parcel, they
		// will come back in the same order
		dest.writeDouble(_latitude);
		dest.writeDouble(_longitude);
		dest.writeString(_cityName);
		dest.writeString(_address);
		dest.writeString(_source);
	}
 
	/**
	 *
	 * Called from the constructor to create this
	 * object from a parcel.
	 *
	 * @param in parcel from which to re-create object
	 */
	private void readFromParcel(Parcel in) {
 
		// We just need to read back each
		// field in the order that it was
		// written to the parcel
		_latitude=in.readDouble();
		_longitude=in.readDouble();
		_cityName=in.readString();
		_address=in.readString();
		_source=in.readString();
	}
 
    /**
     *
     * This field is needed for Android to be able to
     * create new objects, individually or as arrays.
     *
     * This also means that you can use use the default
     * constructor to create the object and use another
     * method to hyrdate it as necessary.
     *
     * I just find it easier to use the constructor.
     * It makes sense for the way my brain thinks ;-)
     *
     */
    public static final Creator CREATOR =
    	new Creator() {
            public LocationData createFromParcel(Parcel in) {
                return new LocationData(in);
            }
 
            public LocationData[] newArray(int size) {
                return new LocationData[size];
            }
    };
    
}

