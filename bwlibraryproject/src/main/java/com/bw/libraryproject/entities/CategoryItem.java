package com.bw.libraryproject.entities;

import android.os.Parcel;
import android.os.Parcelable;
 
/**
 * @author Shahbaz Ali
 *
 * A basic object that can be parcelled to
 * transfer between objects
 *
 */
public class CategoryItem implements Parcelable {
	private String id="";
	private String name="";
	private String url="";
	private Boolean selected=false;
	
	/**
	 * Standard basic constructor for non-parcel
	 * object creation
	 */
	public CategoryItem() { ; };
 
	/**
	 *
	 * Constructor to use when re-constructing object
	 * from a parcel
	 *
	 * @param in a parcel from which to read this object
	 */
	public CategoryItem(Parcel in) {
		readFromParcel(in);
	}
 
	/**
	 * standard getter functions
	 */
	public String getId(){return id;}
	public String getName(){return name;}
	public String getUrl(){return url;}
	public Boolean getSelected(){return selected;}
	
	
	
	
	/**
	 * Standard setter functions
	 */
	public void setId(String i){id=i;}
	public void setName(String n){name=n;}
	public void setUrl(String u){url=u;}
	public void setSelected(Boolean b){selected=b;}
	
	
	
	@Override
	public int describeContents() {
		return 0;
	}
 
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// We just need to write each field into the
		// parcel. When we read from parcel, they
		// will come back in the same order
		dest.writeString(id);
		dest.writeString(name);
		dest.writeString(url);
		dest.writeValue(selected);
	}
 
	/**
	 *
	 * Called from the constructor to create this
	 * object from a parcel.
	 *
	 * @param in parcel from which to re-create object
	 */
	private void readFromParcel(Parcel in) {
 
		// We just need to read back each
		// field in the order that it was
		// written to the parcel
		id=in.readString();
		name=in.readString();
		url=in.readString();
		selected=(Boolean) in.readValue(Boolean.class.getClassLoader());
	}
 
    /**
     *
     * This field is needed for Android to be able to
     * create new objects, individually or as arrays.
     *
     * This also means that you can use use the default
     * constructor to create the object and use another
     * method to hyrdate it as necessary.
     *
     * I just find it easier to use the constructor.
     * It makes sense for the way my brain thinks ;-)
     *
     */
    public static final Creator CREATOR =
    	new Creator() {
            public CategoryItem createFromParcel(Parcel in) {
                return new CategoryItem(in);
            }
 
            public CategoryItem[] newArray(int size) {
                return new CategoryItem[size];
            }
    };
}


