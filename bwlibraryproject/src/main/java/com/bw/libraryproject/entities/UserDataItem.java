package com.bw.libraryproject.entities;

import android.os.Parcel;
import android.os.Parcelable;
 
/**
 * @author Shahbaz Ali
 *
 * A basic object that can be parcelled to
 * transfer between objects
 *
 */
public class UserDataItem implements Parcelable {
	private String uid="";
	private String username="";
	private String first_name="";
	private String last_name="";
	private String profile_pic="";
	
	/**
	 * Standard basic constructor for non-parcel
	 * object creation
	 */
	public UserDataItem() { ; };
 
	/**
	 *
	 * Constructor to use when re-constructing object
	 * from a parcel
	 *
	 * @param in a parcel from which to read this object
	 */
	public UserDataItem(Parcel in) {
		readFromParcel(in);
	}
 
	/**
	 * standard getter functions
	 */
	public String getUserID(){return uid;}
	public String getUserName(){return username;}
	public String getFirstName(){return first_name;}
	public String getLastName(){return last_name;}
	public String getProfilePic(){return profile_pic;}
	
	
	
	
	/**
	 * Standard setter functions
	 */
	public void setUserID(String i){uid=i;}
	public void setUserName(String n){username=n;}
	public void setFirstName(String fn){first_name=fn;}
	public void setLastName(String ln){last_name=ln;}
	public void setProfilePic(String pic){profile_pic=pic;}
	
	
	
	@Override
	public int describeContents() {
		return 0;
	}
 
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// We just need to write each field into the
		// parcel. When we read from parcel, they
		// will come back in the same order
		dest.writeString(uid);
		dest.writeString(username);
		dest.writeString(first_name);
		dest.writeString(last_name);
		dest.writeString(profile_pic);
	}
 
	/**
	 *
	 * Called from the constructor to create this
	 * object from a parcel.
	 *
	 * @param in parcel from which to re-create object
	 */
	private void readFromParcel(Parcel in) {
 
		// We just need to read back each
		// field in the order that it was
		// written to the parcel
		uid=in.readString();
		username=in.readString();
		first_name=in.readString();
		last_name=in.readString();
		profile_pic=in.readString();
	}
 
    /**
     *
     * This field is needed for Android to be able to
     * create new objects, individually or as arrays.
     *
     * This also means that you can use use the default
     * constructor to create the object and use another
     * method to hyrdate it as necessary.
     *
     * I just find it easier to use the constructor.
     * It makes sense for the way my brain thinks ;-)
     *
     */
    public static final Creator CREATOR =
    	new Creator() {
            public UserDataItem createFromParcel(Parcel in) {
                return new UserDataItem(in);
            }
 
            public UserDataItem[] newArray(int size) {
                return new UserDataItem[size];
            }
    };
}


