package com.bw.libraryproject.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;
 
/**
 * @author Shahbaz Ali
 *
 * A base class for RighTips Notifications that can be parcelled to
 * transfer between objects
 *
 */
public class NotificationData implements Parcelable {
	private Integer id=0;
	private String note_id="0";
	private String title="";
	private String detail="";
	private String siteID="0";
	private String siteName="";
	private String siteAddress="";
	private String sitePhone="";
	private String siteLat="0";
	private String siteLong="0";
	private String startDate="";
	private String endDate="";
	private String template="";
	private String zone="1";
	private String status="";
	private String msg_id="0";
	private String likes="";
	private String catGroupID="0";
	private String catGroupName="";
	private String catGroupMarker="";
	private String catID="0";
	private Integer siteTotalNotes=0;
	private String nt_url="";
	private String source="beacon";
	private String video_url="";
	//private String video_url="https://youtu.be/M8egp_3ewfs";

	
	private ArrayList<String> images=new ArrayList<String>();
	
	
	//private String[] images=new String[5];
	private long creationDate=0;
	private String discount="0";
	private ArrayList<CategoryItem> cats=new ArrayList<CategoryItem>();
	private ArrayList<UserDataItem> users_liked=new ArrayList<UserDataItem>();
	
	private ArrayList<SocialMediaLink> social_media_links=new ArrayList<SocialMediaLink>();
	
	
	/**
	 * Standard basic constructor for non-parcel
	 * object creation
	 */
	public NotificationData() { ; };
 
	/**
	 *
	 * Constructor to use when re-constructing object
	 * from a parcel
	 *
	 * @param in a parcel from which to read this object
	 */
	public NotificationData(Parcel in) {
		readFromParcel(in);
	}
 
	/**
	 * standard getter functions
	 */
	
	public Integer getID() {return id;}
	public String getNoteID() {return note_id;}
	public String getTitle() {return title;}
	public String getDetail() {return detail;}
	public String getSiteID() {return siteID;}
	public String getSiteName() {return siteName;}
	public String getSiteAddress() {return siteAddress;}
	public String getSitePhone() {return sitePhone;}
	public String getSiteLat() {return siteLat;}
	public String getSiteLong() {return siteLong;}
	public String getStartDate() {return startDate;}
	public String getEndDate() {return endDate;}
	public String getTemplate() {return template;}
	public String getZone() {return zone;}
	public String getStatus(){return status;}
	public String getMsgID(){return msg_id;}
	public String getLikes(){return likes;}
	public String getCatGroupID(){return catGroupID;}
	public String getCatGroupName(){return catGroupName;}
	public String getCatGroupMarker(){return catGroupMarker;}
	public String getCatID(){return catID;}
	public Integer getSiteTotalNotifications(){return siteTotalNotes;}
	public String getNoteUrl(){return nt_url;}
	public String getSource(){return source;}
	public String getVideoUrl(){return video_url;}
	
	
	public String getImages() {
		if(images.size() > 0){
			String[] imagesArr = new String[images.size()];
			imagesArr = images.toArray(imagesArr);
			return Arrays.toString(imagesArr).replace("[", "").replace("]", "");
		}
		return "";
	}
	
	
	public long getCreationDate(){return creationDate;}
	public String getDiscount(){return discount;}
	public ArrayList<CategoryItem> getCategories() {return cats;}
	public ArrayList<UserDataItem> getUsersLiked() {return users_liked;}
	public ArrayList<SocialMediaLink> getSocialMediaLinks() {return social_media_links;}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Standard setter functions
	 */
	public void setID(int i) {id=i;}
	public void setNoteID(String _id) {note_id=_id;}
	public void setTitle(String t) {title=t;}
	public void setDetail(String d) {detail=d;}
	public void setSiteID(String id) {siteID=id;}
	public void setSiteName(String sn) {siteName=sn;}
	public void setSiteAddress(String sa) {siteAddress=sa;}
	public void setSitePhone(String sp) {sitePhone=sp;}
	public void setSiteLat(String lt) {siteLat=lt;}
	public void setSiteLong(String lng) {siteLong=lng;}
	public void setStartDate(String sd) {startDate=parseDate(sd);}
	public void setEndDate(String ed) {endDate=parseDate(ed);}
	public void setTemplate(String tmp) {template=tmp;}
	public void setZone(String z) {zone=z;}
	public void setStatus(String st){status=st;}
	public void setMsgID(String mid){msg_id=mid;}
	public void setLikes(String l) {likes=l;}
	
	public void setCatGroupID(String cgid){catGroupID=cgid;}
	public void setCatGroupName(String cgn){catGroupName=cgn;}
	public void setCatGroupMarker(String cgm){catGroupMarker=cgm;}
	public void setCatID(String cid){catID=cid;}
	public void setSiteTotalNotifications(Integer t){siteTotalNotes=t;}
	public void setNoteUrl(String url){nt_url=url;}
	public void setSource(String src){source=src;}
	public void setVideoUrl(String url){video_url=url;}
	
	
	
	public void setImages(ArrayList<String> imgs) {images=imgs;}

	
	
	public void setCreationDate(long date){creationDate=date;}
	public void setDiscount(String dis){discount=dis;}
	public void setCategories(ArrayList<CategoryItem> c){this.cats=c;}
	public void setUsersLiked(ArrayList<UserDataItem> ul){
		users_liked=ul;
	}
	
	public void setSocialMediaLinks(ArrayList<SocialMediaLink> links){
		social_media_links=links;
	}
	
	
	
	private String parseDate(String str){
		if(str.equals("") || str.equals("0000-00-00") || str.equals("0000-00-00 00:00:00"))return "";
		
		String[] arr=str.split(" ");
		String newStr="";
		if(arr[0]!=null){
			String d=arr[0];
			String[]darr=d.split("-");
			if(darr[0]!=null){newStr="-"+darr[0];}
			if(darr[1]!=null){newStr="-"+darr[1]+newStr;}
			if(darr[2]!=null){newStr=darr[2]+newStr;}
		}
		return newStr;
	}
	
	
	
	
	@Override
	public int describeContents() {
		return 0;
	}
 
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// We just need to write each field into the
		// parcel. When we read from parcel, they
		// will come back in the same order
		dest.writeInt(id);
		dest.writeString(note_id);
		dest.writeString(title);
		dest.writeString(detail);
		dest.writeString(siteID);
		dest.writeString(siteName);
		dest.writeString(siteAddress);
		dest.writeString(sitePhone);
		dest.writeString(siteLat);
		dest.writeString(siteLong);
		dest.writeString(startDate);
		dest.writeString(endDate);
		dest.writeString(template);
		dest.writeString(zone);
		dest.writeString(status);
		dest.writeString(msg_id);
		dest.writeString(likes);
		dest.writeString(catGroupID);
		dest.writeString(catGroupName);
		dest.writeString(catGroupMarker);
		dest.writeString(catID);
		dest.writeInt(siteTotalNotes);
		dest.writeString(nt_url);
		dest.writeString(source);
		dest.writeString(video_url);
		
		
		
		dest.writeList(images);
		dest.writeLong(creationDate);
		dest.writeString(discount);
		dest.writeTypedList(cats);
		dest.writeTypedList(users_liked);
		dest.writeTypedList(social_media_links);
	}
 
	/**
	 *
	 * Called from the constructor to create this
	 * object from a parcel.
	 *
	 * @param in parcel from which to re-create object
	 */
	private void readFromParcel(Parcel in) {
 
		// We just need to read back each
		// field in the order that it was
		// written to the parcel
		id=in.readInt();
		note_id=in.readString();
		title=in.readString();
		detail=in.readString();
		siteID=in.readString();
		siteName=in.readString();
		siteAddress=in.readString();
		sitePhone=in.readString();
		siteLat=in.readString();
		siteLong=in.readString();
		startDate=in.readString();
		endDate=in.readString();
		template=in.readString();
		zone=in.readString();
		status=in.readString();
		msg_id=in.readString();
		likes=in.readString();
		catGroupID=in.readString();
		catGroupName=in.readString();
		catGroupMarker=in.readString();
		catID=in.readString();
		siteTotalNotes=in.readInt();
		nt_url=in.readString();
		source=in.readString();
		video_url=in.readString();
		
		
		
		images=in.readArrayList(String.class.getClassLoader());
		
		creationDate=in.readLong();
		discount=in.readString();
		//cats=in.readArrayList(CategoryItem.class.getClassLoader());
		in.readTypedList(cats, CategoryItem.CREATOR);
		in.readTypedList(users_liked, UserDataItem.CREATOR);//.class.getClassLoader());
		in.readTypedList(social_media_links, SocialMediaLink.CREATOR);//.class.getClassLoader());
	}
 
    /**
     *
     * This field is needed for Android to be able to
     * create new objects, individually or as arrays.
     *
     * This also means that you can use use the default
     * constructor to create the object and use another
     * method to hyrdate it as necessary.
     *
     * I just find it easier to use the constructor.
     * It makes sense for the way my brain thinks ;-)
     *
     */
    public static final Creator CREATOR =
    	new Creator() {
            public NotificationData createFromParcel(Parcel in) {
                return new NotificationData(in);
            }
 
            public NotificationData[] newArray(int size) {
                return new NotificationData[size];
            }
    };
    
}

