package com.bw.libraryproject.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Shahbaz Ali
 *
 * A basic object that can be parcelled to
 * transfer between objects
 *
 */

public class UserAppData implements Parcelable {
	private String mAppID;
	private String mAppName;
	private String mAppUUID;
	private String mAppKey;
	
	/**
	 * Standard basic constructor for non-parcel
	 * object creation
	 */
	public UserAppData() { ; };
	
	
	public UserAppData(String id, String name, String uuid, String key){
		this.mAppID=id;
		this.mAppName=name;
		this.mAppUUID=uuid;
		this.mAppKey=key;
	}
 
	/**
	 *
	 * Constructor to use when re-constructing object
	 * from a parcel
	 *
	 * @param in a parcel from which to read this object
	 */
	public UserAppData(Parcel in) {
		readFromParcel(in);
	}
	
	/**
	 * standard getter functions
	 */
	public String getID(){return this.mAppID;}
	public String getName(){return this.mAppName;}
	public String getUUID(){return this.mAppUUID;}
	public String getKey(){return this.mAppKey;}
	

	/**
	 * standard setter functions
	 */
	public void setID(String id){this.mAppID=id;}
	public void setName(String n){this.mAppName=n;}
	public void setUUID(String uuid){this.mAppUUID=uuid;}
	public void setKey(String key){this.mAppKey=key;}
	
	
	@Override
	public int describeContents() {
		return 0;
	}
 
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// We just need to write each field into the
		// parcel. When we read from parcel, they
		// will come back in the same order
		dest.writeString(mAppID);
		dest.writeString(mAppName);
		dest.writeString(mAppUUID);
		dest.writeString(mAppKey);
	}
 
	/**
	 *
	 * Called from the constructor to create this
	 * object from a parcel.
	 *
	 * @param in parcel from which to re-create object
	 */
	private void readFromParcel(Parcel in) {
 
		// We just need to read back each
		// field in the order that it was
		// written to the parcel
		mAppID=in.readString();
		mAppName=in.readString();
		mAppUUID=in.readString();
		mAppKey=in.readString();
	}
 
    /**
     *
     * This field is needed for Android to be able to
     * create new objects, individually or as arrays.
     *
     * This also means that you can use use the default
     * constructor to create the object and use another
     * method to hyrdate it as necessary.
     *
     * I just find it easier to use the constructor.
     * It makes sense for the way my brain thinks ;-)
     *
     */
    public static final Creator CREATOR =
    	new Creator() {
            public UserAppData createFromParcel(Parcel in) {
                return new UserAppData(in);
            }
 
            public UserAppData[] newArray(int size) {
                return new UserAppData[size];
            }
    };
	
}


