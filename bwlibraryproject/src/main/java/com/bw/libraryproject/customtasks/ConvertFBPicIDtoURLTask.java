package com.bw.libraryproject.customtasks;

import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;


public class ConvertFBPicIDtoURLTask extends AsyncTask<Void, Void, String> {
    private String url;
    private FBProfilePicUrlHandler mHandler;

    public interface FBProfilePicUrlHandler{
		public void onFoundUrl(String resultUrl);
		public void onError();
	}

    
    

    public ConvertFBPicIDtoURLTask(String url, FBProfilePicUrlHandler h) {
    	this.mHandler=h;
        this.url = url+"?type=large&width=200&height=200";
    }

    @Override
    protected String doInBackground(Void... params) {
    	AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
        final HttpGet request = new HttpGet(url);
        
    	try {
            HttpResponse response = client.execute(request);
            final int statusCode = 
                response.getStatusLine().getStatusCode();

            if (statusCode != HttpStatus.SC_OK) {
                Header[] headers = response.getHeaders("Location");
                if (headers != null && headers.length != 0) {
                    String newUrl =headers[headers.length - 1].getValue();
                    return newUrl;
                    // call again with new URL
                    
                }
            }
        }
        catch (Exception e) {
            request.abort();
        }
    	return null;
    }

    @Override
    protected void onPostExecute(String result) {
        if(this.mHandler!=null){
        	mHandler.onFoundUrl(result);
        }
        super.onPostExecute(result);
    }
}
