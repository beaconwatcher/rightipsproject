/*
Copyright 2014 BeaconWatcher

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package com.bw.libraryproject.activities;


import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.Toast;

import com.bw.libraryproject.R;
import com.bw.libraryproject.app.ApplicationController;
import com.bw.libraryproject.customadapers.LeDeviceListAdapter;
import com.radiusnetworks.ibeacon.IBeacon;
import com.radiusnetworks.ibeacon.IBeaconConsumer;
import com.radiusnetworks.ibeacon.IBeaconManager;
import com.radiusnetworks.ibeacon.RangeNotifier;
import com.radiusnetworks.ibeacon.Region;
import com.radiusnetworks.ibeacon.service.IBeaconData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Displays list of found beacons sorted by RSSI.
 * Starts new activity with selected beacon if activity was provided.
 *
 * @author Shahbaz Ali (BeaconWatcher)
 */
public class ListBeaconsActivity extends Activity implements IBeaconConsumer, RangeNotifier{
  private static final String TAG = ListBeaconsActivity.class.getSimpleName();
  public static final String EXTRAS_TARGET_ACTIVITY = "extraTargetActivity";
  public static final String EXTRAS_BEACON = "extrasBeacon";
  
  protected static final int SPLASH_CODE = 1;
  private static final int REQUEST_ENABLE_BT = 3;
  
  
  
  private Context mContext;
  private ApplicationController mApp;
  private Boolean mBtEnabled = false;
  

  private IBeaconManager iBeaconManager;
  private Map<String,TableRow> mRowMap = new HashMap<String,TableRow>();
  private LeDeviceListAdapter mAdapter;
  

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_list_beacons);
    
    mContext=ListBeaconsActivity.this;
    mApp = (ApplicationController) getApplicationContext();
    
    // Configure device list.
    mAdapter = new LeDeviceListAdapter(this);
    ListView list = (ListView) findViewById(R.id.device_list);
    list.setAdapter(mAdapter);
    list.setOnItemClickListener(createOnItemClickListener());
    
    // Configure BeaconManager.
    iBeaconManager = IBeaconManager.getInstanceForApplication(this.getApplicationContext());
    iBeaconManager.LOG_DEBUG = true;
    
  }
  
  @Override
  protected void onResume() {
    super.onResume();
    iBeaconManager.setForegroundBetweenScanPeriod(1100);
	iBeaconManager.bind(this);

    
    // Check if device supports Bluetooth Low Energy.
    try{
    	//Check if Bluetooth LE is supported by this Android device,
    	//and if so, make sure it is enabled.
    	//Throws a RuntimeException if Bluetooth LE is not supported.
    	//(Note: The Android emulator will do this)
    	
    	//BLE is supported and Bluetooth is enabled.    	
        if (iBeaconManager.checkAvailability()) {
      	  	mBtEnabled = true;
         }
        
    	//BLE supported but Bluetooth is not enabled, show dialog to enable it.
        else{
        	Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        	this.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }
        
  //BLE is not supported
    catch(RuntimeException e){
    	Toast.makeText(mContext, "Device does not have Bluetooth Low Energy", Toast.LENGTH_LONG).show();
    	finish();
    }
  }
 
  //After enabling Bluetooth, we need to set listeners.
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == REQUEST_ENABLE_BT) {
      if (resultCode == Activity.RESULT_OK) {
    	  mBtEnabled = true;
      } else {
        Toast.makeText(this, "Bluetooth not enabled", Toast.LENGTH_LONG).show();
        //getActionBar().setSubtitle("Bluetooth not enabled");
      }
    }
    
    super.onActivityResult(requestCode, resultCode, data);
  }
  
  
  
  
  
  @Override
  public void onIBeaconServiceConnect() {
	  Region region = new Region("MainActivityRanging", null, null, null);
      try {
          iBeaconManager.startRangingBeaconsInRegion(region);
          iBeaconManager.setRangeNotifier(this);
      } catch (RemoteException e) {
          e.printStackTrace();
      }
  }

  @Override
  public void didRangeBeaconsInRegion(final Collection<IBeacon> iBeacons, final Region region) {
	// Note that results are not delivered on UI thread.
      runOnUiThread(new Runnable() {
        @Override
        public void run() {
          // Note that beacons reported here are already sorted by estimated
          // distance between device and beacon.
         // getActionBar().setSubtitle("Found beacons: " + iBeacons.size());
          mAdapter.updateItems(iBeacons);
        }
     });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
/*
    getMenuInflater().inflate(R.menu.scan_menu, menu);
    MenuItem refreshItem = menu.findItem(R.id.refresh);
    refreshItem.setActionView(R.layout.actionbar_indeterminate_progress);
*/
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      finish();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onStart() {
    super.onStart();
    iBeaconManager.setForegroundBetweenScanPeriod(3300);
    iBeaconManager.bind(this);
  }

  @Override
  protected void onStop() {
	  super.onStop();
	  iBeaconManager.setForegroundBetweenScanPeriod(0);
	  iBeaconManager.unBind(this);
  }
  
  @Override
	protected void onPause() {
		super.onPause();
		iBeaconManager.setForegroundBetweenScanPeriod(0);
		iBeaconManager.unBind(this);
	}
  

  @Override
  public void onDestroy() {
      super.onDestroy();
      iBeaconManager.setForegroundBetweenScanPeriod(0);
      iBeaconManager.unBind(this);
  }
  

  private AdapterView.OnItemClickListener createOnItemClickListener() {
    return new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    	//  mApp.setSelectedBeacon(mAdapter.getItem(position));
    	  ArrayList<IBeacon> beacons=new ArrayList<IBeacon>();
    	  beacons.add(mAdapter.getItem(position));
    	  IBeaconData beaconData= (IBeaconData) IBeaconData.fromIBeacons(beacons).toArray()[0];
    	  
    	  if (getIntent().getStringExtra(EXTRAS_TARGET_ACTIVITY) != null) {
              try {
            	  Class<?> clazz = Class.forName(getIntent().getStringExtra(EXTRAS_TARGET_ACTIVITY));
            	  Intent intent = new Intent(ListBeaconsActivity.this, clazz);
            	  intent.putExtra(EXTRAS_BEACON, beaconData);
            	  startActivity(intent);
              } catch (ClassNotFoundException e) {
            	  Log.e(TAG, "Finding class by name failed", e);
              }
    	  }
    	  
    	  else{
    		  Intent intent = new Intent();
    		  intent.putExtra("result", beaconData);
    		  setResult(RESULT_OK, intent);
    		  finish();
    	  }
      }
    };
  }
}
