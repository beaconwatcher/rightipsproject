package com.bw.libraryproject.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.bw.libraryproject.R;
import com.bw.libraryproject.app.ApplicationController;
import com.bw.libraryproject.customadapers.AppAdapter;
import com.bw.libraryproject.entities.UserAppData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SelectAppActivity extends Activity{
	String TAG = "SelectSiteActivity";
	public static String EXTRA_APPLICATIONS = "extraApplications";
	
	private ApplicationController mApp;
	private Context mContext;

	
	private String mSelAppID;
	private String mUserID;
	
	private ArrayList<UserAppData> userApps;
	private AppAdapter mAppsAdapter;
	
	private ListView mAppsList;
	private LinearLayout mNoAppView;
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set View to register.xml
		setContentView(R.layout.activity_select_app);
		mContext = SelectAppActivity.this;
		mApp=(ApplicationController) getApplicationContext();
		
		mNoAppView = (LinearLayout) findViewById(R.id.no_site_box);
		mAppsList = (ListView) findViewById(R.id.sites_list);
		
		userApps = new ArrayList<UserAppData>();
		
		String apps = getIntent().getStringExtra(EXTRA_APPLICATIONS);
		if(!apps.equals("")){
			parseApps(apps, userApps);
		}
		
		mAppsAdapter = new AppAdapter(mContext, userApps);
		mAppsList.setAdapter(mAppsAdapter);
		
		mAppsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				UserAppData app = (UserAppData) mAppsList.getAdapter().getItem(arg2);
				mSelAppID=app.getID();
				
				//Set global app key value.
				mApp.appKey = app.getKey();
				
				Intent intent = new Intent();
//				intent.putExtra("result", response);
				setResult(RESULT_OK, intent);
				finish();
			}
		});
	}
	
	
	private void parseApps(String response, ArrayList<UserAppData> arr){
		JSONObject jo=null;
		try{
			jo=new JSONObject(response);
			
			JSONObject d = jo.getJSONObject("data");
			JSONArray jArr = d.getJSONArray("apps");
			
			for(int i=0; i<jArr.length(); i++){
				JSONObject app=(JSONObject) jArr.get(i);
				UserAppData appData=new UserAppData(app.getString("apk_id"), app.getString("apk_title"), app.getString("apk_uuid"), app.getString("apk_key"));
				arr.add(appData);
			}
		}
		catch(JSONException e){
            Log.e("log_tag", "Error parsing data "+e.toString());
		}
	}

}