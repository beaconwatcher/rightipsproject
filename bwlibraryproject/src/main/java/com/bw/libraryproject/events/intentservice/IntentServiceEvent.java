package com.bw.libraryproject.events.intentservice;

public class IntentServiceEvent {
	public String tag;
    public String message;
    public String type;
    public Object data;
    public String id; 

    public IntentServiceEvent(String tag, String type, String message, Object data, String id) {
    	this.tag = tag;
    	this.type = type;
        this.message = message;
        this.data = data;
        this.id=id;
    }
}