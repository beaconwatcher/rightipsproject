package com.bw.libraryproject.utils;

import android.util.Log;

import com.bw.libraryproject.entities.CategoryItem;
import com.bw.libraryproject.entities.NotificationData;
import com.bw.libraryproject.entities.SocialMediaLink;
import com.bw.libraryproject.entities.UserDataItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NotificationUtilities {
    
	
    public static ArrayList<NotificationData> JsonToNotifications(JSONArray arr){
    	ArrayList<NotificationData> notes=new ArrayList<NotificationData>();
    	for(int i=0; i<arr.length(); i++){
    		try{
        		JSONObject jNote=arr.getJSONObject(i);
        		notes.add(getNotificationFromJson(jNote));
    		}
    		catch(JSONException e){
                Log.e("Notification_Data_JsonToNotifications", "Error parsing data "+e.toString());
			}
    	}
    	return notes;
    }
    
    
    public static NotificationData getNotificationFromJson(JSONObject jNote){
		NotificationData nd=new NotificationData();
		try{
			if(jNote.has("nt_id")){nd.setNoteID(jNote.getString("nt_id"));}
			if(jNote.has("nt_title")){nd.setTitle(jNote.getString("nt_title"));}
			if(jNote.has("nt_details")){nd.setDetail(jNote.getString("nt_details"));}
			if(jNote.has("site_id")){nd.setSiteID(jNote.getString("site_id"));}
			if(jNote.has("site_name")){nd.setSiteName(jNote.getString("site_name"));}
			if(jNote.has("site_title")){nd.setSiteName(jNote.getString("site_title"));}
			if(jNote.has("site_address")){nd.setSiteAddress(jNote.getString("site_address"));}
			if(jNote.has("site_phone")){nd.setSitePhone(jNote.getString("site_phone"));	}
			if(jNote.has("site_lat")){nd.setSiteLat(jNote.getString("site_lat"));	}
			if(jNote.has("site_long")){nd.setSiteLong(jNote.getString("site_long"));	}
			if(jNote.has("nt_deal_start")){nd.setStartDate(jNote.getString("nt_deal_start"));	}
			if(jNote.has("nt_deal_end")){nd.setEndDate(jNote.getString("nt_deal_end"));	}
			if(jNote.has("template")){nd.setTemplate(jNote.getString("template"));	}
			if(jNote.has("zone")){nd.setZone(jNote.getString("zone"));	}
			if(jNote.has("nt_discount")){nd.setDiscount(jNote.getString("nt_discount"));}
			if(jNote.has("msg_id")){nd.setMsgID(jNote.getString("msg_id"));}
			if(jNote.has("total_likes")){nd.setLikes(jNote.getString("total_likes"));}
			if(jNote.has("catGroup_id")){nd.setCatGroupID(jNote.getString("catGroup_id"));}
			if(jNote.has("catGroup_name")){nd.setCatGroupName(jNote.getString("catGroup_name"));}
			if(jNote.has("catGroup_marker")){nd.setCatGroupMarker(jNote.getString("catGroup_marker"));}
			if(jNote.has("cat_id")){nd.setCatID(jNote.getString("cat_id"));}
			if(jNote.has("site_total_notifications")){nd.setSiteTotalNotifications(jNote.getInt("site_total_notifications"));}
			if(jNote.has("nt_url")){nd.setNoteUrl(jNote.getString("nt_url"));}
			if(jNote.has("source")){nd.setSource(jNote.getString("source"));}
			if(jNote.has("video_url")){nd.setVideoUrl(jNote.getString("video_url"));}
			
			if(jNote.has("images")){
				JSONArray imgs=jNote.getJSONArray("images");
				
				ArrayList<String> images=new ArrayList<String>();
				for(int im=0; im<imgs.length(); im++){
					JSONObject imgObj=imgs.getJSONObject(im);
					images.add(imgObj.getString("url").toString());
				}
				
				nd.setImages(images);
			}
			
			
			
			
			
			if(jNote.has("category")){
				ArrayList<CategoryItem> cats=new ArrayList<CategoryItem>();
				JSONArray jCats=jNote.getJSONArray("category");
				for(int c=0; c<jCats.length(); c++){
					CategoryItem cat=new CategoryItem();
					JSONObject jCat=jCats.getJSONObject(c);
					cat.setId(jCat.getString("cat_id"));
					cat.setUrl(jCat.getString("cat_image"));
					cat.setName(jCat.getString("cat_name"));
					cats.add(cat);
				}
				nd.setCategories(cats);
			}
			
			if(jNote.has("likes")){
				ArrayList<UserDataItem> usersLiked=new ArrayList<UserDataItem>();
				try{
					JSONArray users=jNote.getJSONArray("likes");
					for(int u=0; u<users.length(); u++){
						UserDataItem user=new UserDataItem();
						JSONObject uObject=users.getJSONObject(u);
						user.setUserID(uObject.getString("uid"));
						user.setUserName(uObject.getString("username"));
						user.setFirstName(uObject.getString("first_name"));
						user.setLastName(uObject.getString("last_name"));
						user.setProfilePic(uObject.getString("profile_pic"));
						usersLiked.add(user);
					}
					nd.setUsersLiked(usersLiked);
				}
				catch(JSONException e){Log.e("Notification_Data_JsonToNotification", "Error parsing user likes"+e.toString());}
			}
			
			if(jNote.has("social_links")){
				ArrayList<SocialMediaLink> smLinks=new ArrayList<SocialMediaLink>();
				JSONArray jLinks=jNote.getJSONArray("social_links");
				for(int l=0; l<jLinks.length(); l++){
					JSONObject jLink=jLinks.getJSONObject(l);
					SocialMediaLink sm=new SocialMediaLink();
					sm.setID(jLink.getString("id"));
					sm.setName(jLink.getString("name"));
					sm.setUrl(jLink.getString("url"));
					smLinks.add(sm);
				}
				
				if(smLinks.size() > 0){
					nd.setSocialMediaLinks(smLinks);
				}
			}
		}
		catch (JSONException e){
			Log.e("Notification_Data_JsonToNotification", "Error parsing data "+e.toString());
		}
		
		return nd;

    }

}
