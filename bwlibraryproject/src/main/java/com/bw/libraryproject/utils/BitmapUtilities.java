package com.bw.libraryproject.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.BlurMaskFilter.Blur;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Matrix.ScaleToFit;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class BitmapUtilities {
	 public static Bitmap getScaledBitmap(Bitmap b, int reqHeight)  {
	        int bWidth = b.getWidth();
	        int bHeight = b.getHeight();

	        int nHeight = reqHeight;
	        float percent=(((float)nHeight)/(float)bHeight)*(float)100;
	        int nWidth=(bWidth/100)*((int)percent);
	        
	        return Bitmap.createScaledBitmap(b, nWidth, nHeight, false);
	    }
	 
	 public static Bitmap resizeBitmap(Bitmap b, int reqWidth)  {
		 	float aspectRatio = b.getWidth() / (float) b.getHeight();
		    int width = reqWidth;
		    int height = Math.round(width / aspectRatio);

		    return Bitmap.createScaledBitmap(b, width, height, false);
	 }
	 
	 
	 
	 public static Drawable scaleDrawable(Drawable image, float scaleFactor, Resources r) {
		    if ((image == null) || !(image instanceof BitmapDrawable)) {
		        return image;
		    }
		    Bitmap b = ((BitmapDrawable)image).getBitmap();

		    int sizeX = Math.round(image.getIntrinsicWidth() * scaleFactor);
		    int sizeY = Math.round(image.getIntrinsicHeight() * scaleFactor);
		    Bitmap bitmapResized = Bitmap.createScaledBitmap(b, sizeX, sizeY, false);
		    image = new BitmapDrawable(r, bitmapResized);
		    return image;
		}
	 
	 
	 
	 public static Bitmap addShadow(final Bitmap bm, final int dstWidth, final int dstHeight, int color, int size, int dx, int dy) {
		    final Bitmap mask = Bitmap.createBitmap(dstWidth+dx, dstHeight+dy, Config.ALPHA_8);

		    final Matrix scaleToFit = new Matrix();
		    final RectF src = new RectF(0, 0, bm.getWidth(), bm.getHeight());
		    final RectF dst = new RectF(0, 0, dstWidth - dx, dstHeight - dy);
		    scaleToFit.setRectToRect(src, dst, ScaleToFit.CENTER);

		    final Matrix dropShadow = new Matrix(scaleToFit);
		    dropShadow.postTranslate(dx, dy);

		    final Canvas maskCanvas = new Canvas(mask);
		    final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		    maskCanvas.drawBitmap(bm, scaleToFit, paint);
		    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_OUT));
		    maskCanvas.drawBitmap(bm, dropShadow, paint);

		    final BlurMaskFilter filter = new BlurMaskFilter(size, Blur.NORMAL);
		    paint.reset();
		    paint.setAntiAlias(true);
		    paint.setColor(color);
		    paint.setMaskFilter(filter);
		    paint.setFilterBitmap(true);

		    final Bitmap ret = Bitmap.createBitmap(dstWidth+size, dstHeight+size, Config.ARGB_8888);
		    final Canvas retCanvas = new Canvas(ret);
		    retCanvas.drawBitmap(mask, 0,  0, paint);
		    retCanvas.drawBitmap(bm, scaleToFit, null);
		    mask.recycle();
		    return ret;
		}
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	    public static ByteArrayInputStream getByteArrayStream(Uri uri, int thumbnailSize, Context context, Bitmap bitmap){
	    	try{
		    	if(bitmap==null){
		    		bitmap=getThumbnail(uri, thumbnailSize, context);
		    	}
	    		ByteArrayOutputStream out = new ByteArrayOutputStream();
	    		bitmap.compress(Bitmap.CompressFormat.PNG, 85, out);
	    		byte[] myByteArray = out.toByteArray();
	    		out.close();
	    		return new ByteArrayInputStream(myByteArray);
	    	}
	    	catch(IOException e){return null;}
	    }
	    
	    
	    
	    public static byte[] getByteArray(Uri uri, int thumbnailSize, Context context){
	    	try{
	    		Bitmap bitmap=getThumbnail(uri, thumbnailSize, context);
	    		ByteArrayOutputStream out = new ByteArrayOutputStream();
	    		bitmap.compress(Bitmap.CompressFormat.PNG, 85, out);
	    		byte[] myByteArray = out.toByteArray();
	    		out.close();
	    		return myByteArray;
	    	}
	    	catch(IOException e){return null;}
	    }
	    
	    
		public static  Bitmap getThumbnail(Uri uri, int thumbnailSize, Context context) throws FileNotFoundException, IOException{
		        InputStream input = context.getContentResolver().openInputStream(uri);

		        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
		        onlyBoundsOptions.inJustDecodeBounds = true;
		        onlyBoundsOptions.inDither=true;//optional
		        onlyBoundsOptions.inPreferredConfig= Config.ARGB_8888;//optional
		        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
		        input.close();
		        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
		            return null;

		        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

		        double ratio = (originalSize > thumbnailSize) ? (originalSize / thumbnailSize) : 1.0;

		        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
		        bitmapOptions.inDither=true;//optional
		        bitmapOptions.inPreferredConfig= Config.ARGB_8888;//optional
		        input = context.getContentResolver().openInputStream(uri);
		        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
		        input.close();
		        return bitmap;
		    }

		    private static int getPowerOfTwoForSampleRatio(double ratio){
		        int k = Integer.highestOneBit((int)Math.floor(ratio));
		        if(k==0) return 1;
		        else return k;
		    }
		


	    

}
